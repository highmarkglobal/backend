namespace org.axius.chaincode


/**
 * Transactions and Assets on Axius
 */ 
transaction OnboardAsset {  
  --> AxiusAsset asset
}

transaction buyAsset {
  --> AxiusTransactionBuyAsset purchaseAsset
}

transaction OnboardBounty {
  --> AxiusBounty bounty
}

transaction JoinBounty {
  --> AxiusTransactionJoinBounty joinBounty
}

transaction UploadTestResult {
  --> AxiusTestResult testResult  
}

transaction ValidateTestResult {
  --> AxiusValidateResult validateResult
}

 /**
 * Resources
 */
asset AxiusAsset identified by assetId {
  o String assetId
  o String assetName  
  o String assetVersion    
  o String license
  o String licensingTerms
  o String price
  o String pricingUnit
  o String featureType
  o String featureName
  o String featureDescription  

  --> Producer producer 
}

asset AxiusContractAsset identified by contractId {
  o String contractId
  o String contractDate

  --> AxiusAsset asset  
  --> Consumer consumer
}

asset AxiusTransactionBuyAsset identified by purchaseId  {
  o String purchaseId
  o String purchaseDate
  o String purchasePrice
  o String purchasePricingUnit
  o String[] features   
  o String license
  o String licensingTerms
  
  --> AxiusContractAsset contractAsset
}

asset AxiusBounty identified by bountyId {
  o String bountyId    
  o String bountyName    
  o String assetId
  o String startDate
  o String endDate
  o String reward
  o String claimFeatuerType
  o String bountyInstructionDocument
  --> Producer producer
}

asset AxiusContractBounty identified by contractId {
  o String contractId    
  o String joinDate
   
  --> AxiusBounty bounty
  --> Asset asset
  --> Producer producer
  --> Tester tester
}

asset AxiusTransactionJoinBounty identified by joinBountyId  {
  o String joinBountyId
  o String joinDate
  
  --> AxiusContractBounty bounty
}

asset AxiusTestResult identified by testResultId {

  o String testResultId    
  
  --> Tester tester  
  o String bountyName  
  o String assetName
  o String[] featureType
  o String[] featureName
  o String platformProvider
  o String platformOS
  o String toolsUsed
  o String testDate
  o String recommendation
  o String bugReportFile
  o String url
  o String testScriptFile
  o String testDataFile
  o String scriptLogFile  
}

asset AxiusValidateResult identified by validateResultId {

  o String validateResultId    
  
  
}


/**
 *  Participants on Axius
 */
participant Producer identified by Id {
  o String Id
  o String Name  
}
participant Consumer identified by Id {
  o String Id
  o String Name  
}
participant Tester identified by Id {
  o String Id
  o String Name  
}
participant Validator identified by Id {
  o String Id
  o String Name  
}
participant Initiator identified by Id {
  o String Id
  o String Name  
}
