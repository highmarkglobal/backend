/**
 * onboard Asset
 * @param {org.axius.chaincode.OnboardAsset} onboardAsset
 * @transaction
 */
 async function onboardAsset(onboardAsset) {   
   // Business Logic here
   let assetRegistry = await getAssetRegistry('org.axius.chaincode.AxiusAsset');
   await assetRegistry.update(onboardAsset.asset);
 }


 /**
 * buy Asset
 * @param {org.axius.chaincode.buyAsset} buyAsset
 * @transaction
 */
async function buyAsset(buyAsset) {
  let assetRegistry = await getAssetRegistry('org.axius.chaincode.AxiusTransactionBuyAsset');
  await assetRegistry.update(buyAsset.purchaseAsset);
}


/**
 * onboard Bounty
 * @param {org.axius.chaincode.OnboardBounty} onboardBounty
 * @transaction
 */
async function onboardBounty(onboardBounty){
  let assetRegistry = await getAssetRegistry('org.axius.chaincode.AxiusTransactionOnboardBounty');
  await assetRegistry.update(onboardBounty.bounty);
}


/**
 * join Bounty
 * @param {org.axius.chaincode.JoinBounty} joinBounty
 * @transaction
 */
async function joinBounty(joinBounty){
  let assetRegistry = await getAssetRegistry('org.axius.chaincode.AxiusTransactionJoinBounty');
  await assetRegistry.update(joinBounty.joinBounty);
}


/**
 * upload Test Result
 * @param {org.axius.chaincode.UploadTestResult} testResult
 * @transaction
 */
async function updateTestResult(testResult){
  let assetRegistry = await getAssetRegistry('org.axius.chaincode.AxiusTestResult');
  await assetRegistry.update(testResult.testResult);
}
