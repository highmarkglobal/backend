/**
 * onboard Asset
 * @param {org.axius.chaincode.OnboardAsset} onboardAsset
 * @transaction
 */
 async function onboardAsset(onboardAsset) {   
   // Business Logic here
   let assetRegistry = await getAssetRegistry('org.axius.chaincode.AxiusAsset');
   await assetRegistry.update(onboardAsset.asset);
 }


 /**
 * buy Asset
 * @param {org.axius.chaincode.buyAsset} buyAsset
 * @transaction
 */
async function buyAsset(buyAsset) {
  const factory = getFactory();
  const NS = 'org.axius.chaincode';

  const transactionAsset = factory.newResource(NS, 'AxiusTransactionBuyAsset', buyAsset.contractAsset.contractId);  
  transactionAsset.purchaseId = buyAsset.contractAsset.contractId;
  transactionAsset.purchaseDate = buyAsset.contractAsset.contractDate;
  transactionAsset.purchasePrice = buyAsset.contractAsset.asset.price;
  transactionAsset.purchasePricingUnit = buyAsset.contractAsset.asset.pricingUnit;
  transactionAsset.features = buyAsset.contractAsset.asset.featureName;
  transactionAsset.license = buyAsset.contractAsset.asset.license;
  transactionAsset.licensingTerms = buyAsset.contractAsset.asset.licensingTerms;
  transactionAsset.contractAsset = buyAsset.contractAsset;

  let assetRegistry = await getAssetRegistry(NS + '.AxiusTransactionBuyAsset');
  await assetRegistry.addAll([transactionAsset]);
}


/**
 * onboard Bounty
 * @param {org.axius.chaincode.OnboardBounty} onboardBounty
 * @transaction
 */
async function onboardBounty(onboardBounty){
  let assetRegistry = await getAssetRegistry('org.axius.chaincode.AxiusTransactionOnboardBounty');
  await assetRegistry.update(onboardBounty.bounty);
}


/**
 * join Bounty
 * @param {org.axius.chaincode.JoinBounty} joinBounty
 * @transaction
 */
async function joinBounty(joinBounty){
  const factory = getFactory();
  const NS = 'org.axius.chaincode';

  const transactionBounty = factory.newResource(NS, 'AxiusTransactionJoinBounty', joinBounty.contractBounty.contractId);    
  transactionBounty.contractId = joinBounty.contractBounty.contractId;
  transactionBounty.bountyId = joinBounty.contractBounty.bounty.bountyId;
  transactionBounty.claimFeatureType = joinBounty.contractBounty.bounty.claimFeatureType;
  transactionBounty.claimFeatureName = joinBounty.contractBounty.bounty.claimFeatureName;
  transactionBounty.reward = joinBounty.contractBounty.bounty.reward;
  transactionBounty.assetId = joinBounty.contractBounty.bounty.assetId;  
  transactionBounty.assetVersion = joinBounty.contractBounty.bounty.assetVersion;
  transactionBounty.initiator = joinBounty.contractBounty.bounty.initiator;
  transactionBounty.joinDate = joinBounty.contractBounty.contractDate;
  transactionBounty.contractBounty = joinBounty.contractBounty;

  let assetRegistry = await getAssetRegistry('org.axius.chaincode.AxiusTransactionJoinBounty');
  await assetRegistry.addAll([transactionBounty]);

}

/**
 * upload Test Result
 * @param {org.axius.chaincode.UploadTestResult} testResult
 * @transaction-
 */
async function updateTestResult(testResult){
  let assetRegistry = await getAssetRegistry('org.axius.chaincode.AxiusTestResult');
  await assetRegistry.update(testResult.testResult);
}

