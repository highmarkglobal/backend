package tech.highmarkglobal.blockchain.config;

public class BlockchainConfig {
	private static final String BASE_URL = "http://localhost:3000/api/org.axius.chaincode.";
	private static final String BASE_CLASS = "org.axius.chaincode.";
	
	public static final String PRODUCER = "Producer";
	public static final String CONSUMER = "Consumer";
	public static final String TESTER = "Tester";
	public static final String VALIDATOR = "Validator";
	public static final String AXIUSASSET = "AxiusAsset";
	public static final String CONTRACT_ASSET = "AxiusContractAsset";
	public static final String BUY_ASSET = "buyAsset";
	public static final String BOUNTY = "AxiusBounty";
	public static final String CONTRACT_BOUNTY = "AxiusContractBounty";
	public static final String JOIN_BOUNTY = "JoinBounty";	
	public static final String TEST_RESULT = "AxiusTestResult";
	public static final String VALIDATION_RESULT = "AxiusValidationResult";
	
	public static final String URI_PRODUCER = BASE_URL + PRODUCER;
	public static final String URI_CONSUMER = BASE_URL + CONSUMER;	                                            
	public static final String URI_TESTER = BASE_URL + TESTER;	
	public static final String URI_VALIDATOR = BASE_URL + VALIDATOR;
	public static final String URI_ASSET = BASE_URL + AXIUSASSET;
	public static final String URI_CONTRACT_ASSET = BASE_URL + CONTRACT_ASSET;	
	public static final String URI_BUY_ASSET = BASE_URL + BUY_ASSET;
	public static final String URI_BOUNTY = BASE_URL + BOUNTY;
	public static final String URI_CONTRACT_BOUNTY = BASE_URL + CONTRACT_BOUNTY;
	public static final String URI_JOIN_BOUNTY = BASE_URL + JOIN_BOUNTY;
	public static final String URI_TEST_RESULT = BASE_URL + TEST_RESULT;
	public static final String URI_VALIDATION_RESULT = BASE_URL + VALIDATION_RESULT;	
	
	public static final String CLASS = "$class";
	public static final String CLASS_PRODUCER = BASE_CLASS + PRODUCER;
	public static final String CLASS_CONSUMER = BASE_CLASS + CONSUMER;
	public static final String CLASS_TESTER = BASE_CLASS + TESTER;
	public static final String CLASS_VALIDATOR = BASE_CLASS + VALIDATOR;
	public static final String CLASS_ASSET = BASE_CLASS + AXIUSASSET;
	public static final String CLASS_CONTRACT_ASSET = BASE_CLASS + CONTRACT_ASSET;
	public static final String CLASS_BOUNTY = BASE_CLASS + BOUNTY;
	public static final String CLASS_CONTRACT_BOUNTY = BASE_CLASS + CONTRACT_BOUNTY;
	public static final String CLASS_TEST_RESULT = BASE_CLASS + TEST_RESULT;
	public static final String CLASS_VALIDATION_RESULT = BASE_CLASS + VALIDATION_RESULT;	
}
