package tech.highmarkglobal.blockchain;

import java.util.HashMap;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import tech.highmarkglobal.bcm.web.dto.AssetDTO;
import tech.highmarkglobal.blockchain.config.BlockchainConfig;

public class Asset {
	private HttpStatus callChaincodeAPI(String uri, HashMap<String, String> requestPayload) {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("Content-Type",  "application/json");
		
		headers.setAll(map);
		
		HttpEntity<?> request = new HttpEntity<>(requestPayload, headers);
		ResponseEntity<?> response = new RestTemplate().postForEntity(uri, request, String.class);
		
		return response.getStatusCode();		
	}
	
//	public HttpStatus addProducer(HashMap<String, String> requestPayload) {
//		return callChaincodeAPI(BlockchainConfig.URI_ASSET, requestPayload);		
//	}
	
	public void addAsset(AssetDTO asset) {
		HashMap<String, String> requestPayload = new HashMap<String, String>();
		//requestPayload.put("assetId", )
	}

}
