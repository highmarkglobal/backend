package tech.highmarkglobal.bcm.web.dto;


import java.util.Date;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import io.swagger.annotations.ApiModel;
@ApiModel
public class JoinBountyDTO {
	
	
	private String bountyID;
	
	private String bountyName;
	
	private String assetID;
	
	private String assetName;
	
	private String producerID;
	
	private String producerName;
	
	private String testerID;
	
	private String testerName;
	
	@DateTimeFormat(iso = ISO.DATE)
	private Date joinDate;
	

	public String getBountyID() {
		return bountyID;
	}

	public void setBountyID(String bountyID) {
		this.bountyID = bountyID;
	}

	public String getBountyName() {
		return bountyName;
	}

	public void setBountyName(String bountyName) {
		this.bountyName = bountyName;
	}

	public String getAssetID() {
		return assetID;
	}

	public void setAssetID(String assetID) {
		this.assetID = assetID;
	}

	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	public String getProducerID() {
		return producerID;
	}

	public void setProducerID(String producerID) {
		this.producerID = producerID;
	}

	public String getProducerName() {
		return producerName;
	}

	public void setProducerName(String producerName) {
		this.producerName = producerName;
	}

	public String getTesterID() {
		return testerID;
	}

	public void setTesterID(String testerID) {
		this.testerID = testerID;
	}

	public String getTesterName() {
		return testerName;
	}

	public void setTesterName(String testerName) {
		this.testerName = testerName;
	}

	public Date getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}



@Override
	public String toString() {
	
	return "JoinBountyDTO [bountyID= "+bountyID+ ",bountyName= "+bountyName+ ",assetID= "+assetID+ ",assetName= "+assetName+ ",producerID= "+producerID+ ",producerName= "+producerName+ ",testerID= "+testerID+ ",testerName= "+testerName+ ",joinDate= "+joinDate+ "]";
}
	
}
