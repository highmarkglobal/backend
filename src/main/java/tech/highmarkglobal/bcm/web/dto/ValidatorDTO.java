package tech.highmarkglobal.bcm.web.dto;



import java.util.Date;

//import org.bson.org.bson.Document;
//import org.bson.Document;
import org.json.simple.JSONObject;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import tech.highmarkglobal.bcm.domain.FileInfo;

public class ValidatorDTO {
	

	
	private String bountyName;

	private String assetName;
	
	private String rewards;

/*	private String claimFeatureType;

	private String claimFeatureName;*/
	@DateTimeFormat(iso = ISO.DATE)
	private Date validatorDate;
	
	private JSONObject features;

	private String platform;

	private String toolsUsed;

	private String recommendation;

	private FileInfo testReport;

	private FileInfo testScript;

	private FileInfo scriptLog;
	
	private String systemRecommendation;
	
	private String systemConfidence;
	
	private String validationRecommendation;
	
	private String validationMethod;
	
	private FileInfo validationReport;
	
	private FileInfo validatorBugsReport;
	
	private FileInfo validatorTestScripts;
	
	private FileInfo validatorScriptLog;

	private String testerID;
	
	private String producerId;
	
	//added tester report filenames
    private String bugsReportFilaname;
	
	private String testScriptFilename;
	
	
	private String scriptLogFilename;
	
	//validator reports
	private String validationReportFilename;
    
	private String validatorBugsReportFilename;
	        
	private String validatorTestScriptsFilename;
	        
	private String validatorScriptLogFilename;
	
	
	
	
	

	public String getValidationReportFilename() {
		return validationReportFilename;
	}

	public void setValidationReportFilename(String validationReportFilename) {
		this.validationReportFilename = validationReportFilename;
	}

	public String getValidatorBugsReportFilename() {
		return validatorBugsReportFilename;
	}

	public void setValidatorBugsReportFilename(String validatorBugsReportFilename) {
		this.validatorBugsReportFilename = validatorBugsReportFilename;
	}

	public String getValidatorTestScriptsFilename() {
		return validatorTestScriptsFilename;
	}

	public void setValidatorTestScriptsFilename(String validatorTestScriptsFilename) {
		this.validatorTestScriptsFilename = validatorTestScriptsFilename;
	}

	public String getValidatorScriptLogFilename() {
		return validatorScriptLogFilename;
	}

	public void setValidatorScriptLogFilename(String validatorScriptLogFilename) {
		this.validatorScriptLogFilename = validatorScriptLogFilename;
	}

	public String getBugsReportFilaname() {
		return bugsReportFilaname;
	}

	public void setBugsReportFilaname(String bugsReportFilaname) {
		this.bugsReportFilaname = bugsReportFilaname;
	}

	public String getTestScriptFilename() {
		return testScriptFilename;
	}

	public void setTestScriptFilename(String testScriptFilename) {
		this.testScriptFilename = testScriptFilename;
	}



	public String getScriptLogFilename() {
		return scriptLogFilename;
	}

	public void setScriptLogFilename(String scriptLogFilename) {
		this.scriptLogFilename = scriptLogFilename;
	}
	
	
	
	
	

	public String getProducerId() {
		return producerId;
	}

	public void setProducerId(String producerId) {
		this.producerId = producerId;
	}

	public String getTesterID() {
		return testerID;
	}

	public void setTesterID(String testerID) {
		this.testerID = testerID;
	}

	public String getBountyName() {
		return bountyName;
	}

	public void setBountyName(String bountyName) {
		this.bountyName = bountyName;
	}

	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	
	

	public String getRewards() {
		return rewards;
	}

	public void setRewards(String rewards) {
		this.rewards = rewards;
	}

/*	public String getClaimFeatureType() {
		return claimFeatureType;
	}

	public void setClaimFeatureType(String claimFeatureType) {
		this.claimFeatureType = claimFeatureType;
	}

	public String getClaimFeatureName() {
		return claimFeatureName;
	}

	public void setClaimFeatureName(String claimFeatureName) {
		this.claimFeatureName = claimFeatureName;
	}
*/
	public JSONObject getFeatures() {
		return features;
	}

	public void setFeatures(JSONObject features) {
		this.features = features;
	}

	
	public String getPlatform() {
		return platform;
	}


	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getToolsUsed() {
		return toolsUsed;
	}

	public void setToolsUsed(String toolsUsed) {
		this.toolsUsed = toolsUsed;
	}

	public String getRecommendation() {
		return recommendation;
	}

	public void setRecommendation(String recommendation) {
		this.recommendation = recommendation;
	}

	
	public Date getValidatorDate() {
		return validatorDate;
	}

	public void setValidatorDate(Date validatorDate) {
		this.validatorDate = validatorDate;
	}



	public FileInfo getTestReport() {
		return testReport;
	}

	public void setTestReport(FileInfo testReport) {
		this.testReport = testReport;
	}

	public FileInfo getTestScript() {
		return testScript;
	}

	public void setTestScript(FileInfo testScript) {
		this.testScript = testScript;
	}

	public FileInfo getScriptLog() {
		return scriptLog;
	}

	public void setScriptLog(FileInfo scriptLog) {
		this.scriptLog = scriptLog;
	}

	public String getSystemRecommendation() {
		return systemRecommendation;
	}

	public void setSystemRecommendation(String systemRecommendation) {
		this.systemRecommendation = systemRecommendation;
	}

	public String getSystemConfidence() {
		return systemConfidence;
	}

	public void setSystemConfidence(String systemConfidence) {
		this.systemConfidence = systemConfidence;
	}

	public String getValidationRecommendation() {
		return validationRecommendation;
	}

	public void setValidationRecommendation(String validationRecommendation) {
		this.validationRecommendation = validationRecommendation;
	}

	public String getValidationMethod() {
		return validationMethod;
	}

	public void setValidationMethod(String validationMethod) {
		this.validationMethod = validationMethod;
	}

	public FileInfo getValidationReport() {
		return validationReport;
	}

	public void setValidationReport(FileInfo validationReport) {
		this.validationReport = validationReport;
	}

	public FileInfo getValidatorBugsReport() {
		return validatorBugsReport;
	}

	public void setValidatorBugsReport(FileInfo validatorBugsReport) {
		this.validatorBugsReport = validatorBugsReport;
	}

	public FileInfo getValidatorTestScripts() {
		return validatorTestScripts;
	}

	public void setValidatorTestScripts(FileInfo validatorTestScripts) {
		this.validatorTestScripts = validatorTestScripts;
	}

	public FileInfo getValidatorScriptLog() {
		return validatorScriptLog;
	}

	public void setValidatorScriptLog(FileInfo validatorScriptLog) {
		this.validatorScriptLog = validatorScriptLog;
	}

	
	


}
