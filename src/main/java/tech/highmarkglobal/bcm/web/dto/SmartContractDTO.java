package tech.highmarkglobal.bcm.web.dto;

import org.json.simple.JSONObject;

public class SmartContractDTO {
	
    private String name;
	
	private String secondParty;
	
	private JSONObject jsonObject;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecondParty() {
		return secondParty;
	}

	public void setSecondParty(String secondParty) {
		this.secondParty = secondParty;
	}

	public JSONObject getJsonObject() {
		return jsonObject;
	}

	public void setJsonObject(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}

	@Override
	public String toString() {
		return "SmartContractDTO [name=" + name + ", secondParty=" + secondParty + ", jsonObject=" + jsonObject + "]";
	}
	
	

}
