package tech.highmarkglobal.bcm.web.dto;

import java.util.Collection;

import org.springframework.data.annotation.Id;

import io.swagger.annotations.ApiModel;
import tech.highmarkglobal.bcm.domain.PricingTemplate;
import tech.highmarkglobal.bcm.domain.PricingTemplate.PricingOption;

@ApiModel
public class PricingTemplateDTO {
	
	@Id
	private String id;
	private PricingOption option;
	private String planName;
	private String paymentOption;
	private int users;
	private float tokens;
	private Double price;
	
	public PricingOption getOption() {
		return option;
	}
	public void setOption(PricingOption option) {
		this.option = option;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getPaymentOption() {
		return paymentOption;
	}
	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}
	public int getUsers() {
		return users;
	}
	public void setUsers(int users) {
		this.users = users;
	}
	public float getTokens() {
		return tokens;
	}
	public void setTokens(float tokens) {
		this.tokens = tokens;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		return "PricingTemplateDTO [option=" + option + ", planName=" + planName + ", paymentOption=" + paymentOption
				+ ", users=" + users + ", tokens=" + tokens + ", price=" + price + "]";
	}
	
	
}
