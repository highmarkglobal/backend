package tech.highmarkglobal.bcm.web.dto;
import java.util.Date;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import tech.highmarkglobal.bcm.domain.FileInfo;

//import org.bson.org.bson.Document;
import org.json.simple.JSONObject;
//import org.bson.Document;
public class TestResultDTO {

		
		private String bountyName;

		private String assetName;

/*		private String featureType;

		private String functionalFeature;*/
		
		private JSONObject features;

		private String platformProvider;

		private String platformOS;

		private String testType;

		private String toolsUsed;
		
		@DateTimeFormat(iso = ISO.DATE)
		private Date testDate;

		private String recommendation;

		private FileInfo testReport;

		private String URL;

		//private FileInfo testScript;

		private FileInfo testData;

		private FileInfo scriptLog;
		
		private String testReportFilaname;
		
		private String testScriptFilename;
		
		private String testDataFilename;
		
		private String scriptLogFilename;
		
		private String testerId;

		public String getTesterId() {
			return testerId;
		}

		public void setTesterId(String testerId) {
			this.testerId = testerId;
		}		

		public String getTestReportFilaname() {
			return testReportFilaname;
		}

		public void setTestReportFilaname(String testReportFilaname) {
			this.testReportFilaname = testReportFilaname;
		}

		public String getTestScriptFilename() {
			return testScriptFilename;
		}

		public void setTestScriptFilename(String testScriptFilename) {
			this.testScriptFilename = testScriptFilename;
		}

		public String getTestDataFilename() {
			return testDataFilename;
		}

		public void setTestDataFilename(String testDataFilename) {
			this.testDataFilename = testDataFilename;
		}

		public String getScriptLogFilename() {
			return scriptLogFilename;
		}

		public void setScriptLogFilename(String scriptLogFilename) {
			this.scriptLogFilename = scriptLogFilename;
		}



		public String getBountyName() {
			return bountyName;
		}

		public void setBountyName(String bountyName) {
			this.bountyName = bountyName;
		}

		public String getAssetName() {
			return assetName;
		}

		public void setAssetName(String assetName) {
			this.assetName = assetName;
		}

/*		public String getFeatureType() {
			return featureType;
		}

		public void setFeatureType(String featureType) {
			this.featureType = featureType;
		}

		public String getFunctionalFeature() {
			return functionalFeature;
		}

		public void setFunctionalFeature(String functionalFeature) {
			this.functionalFeature = functionalFeature;
		}
*/
		
		public String getPlatformProvider() {
			return platformProvider;
		}

		public JSONObject getFeatures() {
			return features;
		}

		public void setFeatures(JSONObject features) {
			this.features = features;
		}

		public void setPlatformProvider(String platformProvider) {
			this.platformProvider = platformProvider;
		}

		public String getPlatformOS() {
			return platformOS;
		}

		public void setPlatformOS(String platformOS) {
			this.platformOS = platformOS;
		}

		public String getTestType() {
			return testType;
		}

		public void setTestType(String testType) {
			this.testType = testType;
		}

		public String getToolsUsed() {
			return toolsUsed;
		}

		public void setToolsUsed(String toolsUsed) {
			this.toolsUsed = toolsUsed;
		}

		public Date getTestDate() {
			return testDate;
		}

		public void setTestDate(Date testDate) {
			this.testDate = testDate;
		}

		public String getRecommendation() {
			return recommendation;
		}

		public void setRecommendation(String recommendation) {
			this.recommendation = recommendation;
		}

		public FileInfo getTestReport() {
			return testReport;
		}

		public void setTestReport(FileInfo testReport) {
			this.testReport = testReport;
		}

		public String getURL() {
			return URL;
		}

		public void setURL(String uRL) {
			URL = uRL;
		}

	/*	public FileInfo getTestScript() {
			return testScript;
		}

		public void setTestScript(FileInfo testScript) {
			this.testScript = testScript;
		}*/

		public FileInfo getTestData() {
			return testData;
		}

		public void setTestData(FileInfo testData) {
			this.testData = testData;
		}

		public FileInfo getScriptLog() {
			return scriptLog;
		}

		public void setScriptLog(FileInfo scriptLog) {
			this.scriptLog = scriptLog;
		}
		


	}



