package tech.highmarkglobal.bcm.web.dto;



import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import tech.highmarkglobal.bcm.domain.PricingTemplate;
import tech.highmarkglobal.bcm.domain.ProductDomain;


@ApiModel
public class ProductDTO {
	
	String name;

	String stage;
	
	private String id;
	
	private String assetType;
	
	private Double serviceCharge;
	
	private List<String> domainId;
	
	private float productVersion;
	
	private List<PricingTemplate> pricingTemplateDto;
	
	private String producerId;
	
	public ProductDTO() {
		super();
		// TODO Auto-generated constructor stub
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getStage() {
		return stage;
	}


	public void setStage(String stage) {
		this.stage = stage;
	}


	public String getAssetType() {
		return assetType;
	}


	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}


	public Double getServiceCharge() {
		return serviceCharge;
	}


	public void setServiceCharge(Double serviceCharge) {
		this.serviceCharge = serviceCharge;
	}


	public List<String> getDomainId() {
		return domainId;
	}


	public void setDomainId(List<String> domainId) {
		this.domainId = domainId;
	}


	/*public List<PricingTemplateDTO> getPricingTemplateDtos() {
		return pricingTemplateDtos;
	}


	public void setPricingTemplateDtos(List<PricingTemplateDTO> pricingTemplateDtos) {
		this.pricingTemplateDtos = pricingTemplateDtos;
	}*/


	public float getProductVersion() {
		return productVersion;
	}


	public void setProductVersion(float productVersion) {
		this.productVersion = productVersion;
	}

	public String getProducerId() {
		return producerId;
	}

	public void setProducerId(String producerId) {
		this.producerId = producerId;
	}
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}
	
	public List<PricingTemplate> getPricingTemplateDto() {
		return pricingTemplateDto;
	}


	public void setPricingTemplateDto(List<PricingTemplate> pricingTemplateDto) {
		this.pricingTemplateDto = pricingTemplateDto;
	}


	@Override
	public String toString() {
		return "ProductDTO [name=" + name + ", stage=" + stage + ", assetType=" + assetType + ", serviceCharge="
				+ serviceCharge + ", productVersion=" + productVersion + "]";
	}
	
	

}
