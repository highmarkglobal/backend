package tech.highmarkglobal.bcm.web.dto;


import java.util.ArrayList;
import java.util.Arrays;

import org.json.simple.JSONObject;
import org.springframework.data.annotation.CreatedBy;

import io.swagger.annotations.ApiModel;
@ApiModel
public class AssetDTO {


    private String name;
    
    private String  assetType;
    
    private String  domain;
    
    private String  interfaceType;
    
    private String  deployomentType;
    
    private String  osType;
    
    private String  licensingTerms;
    
    private String  price;
    
    private String  pricingUnit;
    
/*	private String  featureType;
    
    private String  featureTechnology;
    
    private String  featureName;
    
    private String  featureDescription;*/

    private String producerName;
    
    private JSONObject  features;
    
  //  private String duration;
    
   // private String perUser;
    
    
 //   private String perCPU;
    
    
 private JSONObject serviceBundledPrice;
    
    private String serviceDuraiton;
    
    private Integer persistentPrice;
    
    private String persistentPerCPU;
    
    private JSONObject persistentPerFeature;
    
    
    
    
    
    


	public JSONObject getServiceBundledPrice() {
		return serviceBundledPrice;
	}

	public void setServiceBundledPrice(JSONObject serviceBundledPrice) {
		this.serviceBundledPrice = serviceBundledPrice;
	}

	public String getServiceDuraiton() {
		return serviceDuraiton;
	}

	public void setServiceDuraiton(String serviceDuraiton) {
		this.serviceDuraiton = serviceDuraiton;
	}

	public Integer getPersistentPrice() {
		return persistentPrice;
	}

	public void setPersistentPrice(Integer persistentPrice) {
		this.persistentPrice = persistentPrice;
	}

	public String getPersistentPerCPU() {
		return persistentPerCPU;
	}

	public void setPersistentPerCPU(String persistentPerCPU) {
		this.persistentPerCPU = persistentPerCPU;
	}

	public JSONObject getPersistentPerFeature() {
		return persistentPerFeature;
	}

	public void setPersistentPerFeature(JSONObject persistentPerFeature) {
		this.persistentPerFeature = persistentPerFeature;
	}
    
    


/*	public String getPerCPU() {
		return perCPU;
	}

	public void setPerCPU(String perCPU) {
		this.perCPU = perCPU;
	}
    
	public String getPerUser() {
		return perUser;
	}

	public void setPerUser(String perUser) {
		this.perUser = perUser;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}
*/
    

	public JSONObject getFeatures() {
		return features;
	}

	public void setFeatures(JSONObject features) {
		this.features = features;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
    public String getAssetType() {
		return assetType;
	}

	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getInterfaceType() {
		return interfaceType;
	}

	public void setInterfaceType(String interfaceType) {
		this.interfaceType = interfaceType;
	}

	public String getDeployomentType() {
		return deployomentType;
	}

	public void setDeployomentType(String deployomentType) {
		this.deployomentType = deployomentType;
	}

	public String getOsType() {
		return osType;
	}

	public void setOsType(String osType) {
		this.osType = osType;
	}

	public String getLicensingTerms() {
		return licensingTerms;
	}

	public void setLicensingTerms(String licensingTerms) {
		this.licensingTerms = licensingTerms;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getPricingUnit() {
		return pricingUnit;
	}

	public void setPricingUnit(String pricingUnit) {
		this.pricingUnit = pricingUnit;
	}

/*	public String getFeatureType() {
		return featureType;
	}

	public void setFeatureType(String featureType) {
		this.featureType = featureType;
	}

	public String getFeatureTechnology() {
		return featureTechnology;
	}

	public void setFeatureTechnology(String featureTechnology) {
		this.featureTechnology = featureTechnology;
	}

	public String getFeatureName() {
		return featureName;
	}

	public void setFeatureName(String featureName) {
		this.featureName = featureName;
	}

	public String getFeatureDescription() {
		return featureDescription;
	}

	public void setFeatureDescription(String featureDescription) {
		this.featureDescription = featureDescription;
	}*/
	
	
	public String getProducerName() {
		return producerName;
	}

	public void setProducerName(String producerName) {
		this.producerName = producerName;
	}

	@Override
	public String toString() {
		return "AssetDTO [name=" + name + ", assetType=" + assetType + ", domain=" + domain + ",interfaceType=" + interfaceType + ", deployomentType=" + deployomentType + ",osType=" + osType + ", licensingTerms=" + licensingTerms + ",price=" + price + ", pricingUnit=" + pricingUnit + ", producerName=" + producerName + "]";
	}
    
    

}
