package tech.highmarkglobal.bcm.web.dto;

import org.hibernate.validator.constraints.NotBlank;

/**
 * 
 * @author arun
 *
 */
public class LoginDTO {
    @NotBlank
    private String email;

    @NotBlank
    private String password;

    public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
