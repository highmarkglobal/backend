package tech.highmarkglobal.bcm.web.dto;

public class AssetTypeDTO {

	
    private String name;
    
    private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "AssetTypeDTO [name=" + name + ", description=" + description + "]";
	}
    
    
    
}
