package tech.highmarkglobal.bcm.web.dto;

import io.swagger.annotations.ApiModel;
import tech.highmarkglobal.bcm.domain.DomainAttributes;
@ApiModel
public class ProductDomainDTO {
	
	private String domainName;
	
	private String parentId;
	
	/*private DomainAttributes domainAttr;

	public DomainAttributes getDomainAttr() {
		return domainAttr;
	}

	public void setDomainAttr(DomainAttributes domainAttr) {
		this.domainAttr = domainAttr;
	}*/
	
	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	
	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	@Override
	public String toString() {
		return "ProductDomainDTO [domainName=" + domainName + "Parent Id=" + parentId + "]";
	}
	
	

}
