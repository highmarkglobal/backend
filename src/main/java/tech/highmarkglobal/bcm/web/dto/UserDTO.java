package tech.highmarkglobal.bcm.web.dto;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

import tech.highmarkglobal.bcm.domain.UserCategory;

/**
 * 
 * @author arun
 *
 */

public class UserDTO {
    
  /*  @Size(min = 4, max = 40)
    private String name;*/

    @NotBlank
    @Size(max = 40)
    @Email
    private String email;
    
   private String password;
    
   private String userType;
   
   private String UserDetailsId;
   
   private String name;
   
   private String corporationName;
   
   
public String getCorporationName() {
	return corporationName;
}

public void setCorporationName(String corporationName) {
	this.corporationName = corporationName;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

	/*    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
*/
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getUserDetailsId() {
		return UserDetailsId;
	}

	public void setUserDetailsId(String userDetailsId) {
		UserDetailsId = userDetailsId;
	}

	
}
