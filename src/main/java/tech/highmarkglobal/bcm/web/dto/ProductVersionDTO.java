package tech.highmarkglobal.bcm.web.dto;

import io.swagger.annotations.ApiModel;

@ApiModel
public class ProductVersionDTO {
	
    String productId;
	
	String name;
	
	float productVersion;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(float productVersion) {
		this.productVersion = productVersion;
	}

	@Override
	public String toString() {
		return "ProductVersionDTO [productId=" + productId + ", name=" + name + ", productVersion=" + productVersion
				+ "]";
	}
	
	

}
