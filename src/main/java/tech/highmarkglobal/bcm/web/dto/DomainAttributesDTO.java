package tech.highmarkglobal.bcm.web.dto;

public class DomainAttributesDTO {
	
    String productVersion;
	
	String productBundleName;
	
	String feature;

	public String getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(String productVersion) {
		this.productVersion = productVersion;
	}

	public String getProductBundleName() {
		return productBundleName;
	}

	public void setProductBundleName(String productBundleName) {
		this.productBundleName = productBundleName;
	}

	public String getFeature() {
		return feature;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

	@Override
	public String toString() {
		return "DomainAttributesDTO [productVersion=" + productVersion + ", productBundleName=" + productBundleName
				+ ", feature=" + feature + "]";
	}

}
