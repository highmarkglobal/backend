package tech.highmarkglobal.bcm.web.transformer;



import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tech.highmarkglobal.bcm.domain.AddressDetails;
import tech.highmarkglobal.bcm.domain.Asset;
import tech.highmarkglobal.bcm.domain.AssetType;
import tech.highmarkglobal.bcm.domain.BillingDetails;
import tech.highmarkglobal.bcm.domain.BountyProgram;
import tech.highmarkglobal.bcm.domain.DepositDetails;
import tech.highmarkglobal.bcm.domain.JoinBounty;
import tech.highmarkglobal.bcm.domain.PricingTemplate;
import tech.highmarkglobal.bcm.domain.Product;
import tech.highmarkglobal.bcm.domain.ProductDomain;
import tech.highmarkglobal.bcm.domain.Purchase;
import tech.highmarkglobal.bcm.domain.SmartContract;
import tech.highmarkglobal.bcm.domain.TestResult;
import tech.highmarkglobal.bcm.domain.User;
import tech.highmarkglobal.bcm.domain.UserDetails;
import tech.highmarkglobal.bcm.domain.Validator;
import tech.highmarkglobal.bcm.repo.AssetTypesRepository;
import tech.highmarkglobal.bcm.repo.ProductDomainRepository;
import tech.highmarkglobal.bcm.util.HighmarkUtil;
import tech.highmarkglobal.bcm.web.dto.AddressDetailsDTO;
import tech.highmarkglobal.bcm.web.dto.AssetDTO;
import tech.highmarkglobal.bcm.web.dto.AssetTypeDTO;
import tech.highmarkglobal.bcm.web.dto.BillingDetailsDTO;
import tech.highmarkglobal.bcm.web.dto.BountyProgramDTO;
import tech.highmarkglobal.bcm.web.dto.DepositDetailsDTO;
import tech.highmarkglobal.bcm.web.dto.JoinBountyDTO;
import tech.highmarkglobal.bcm.web.dto.PricingTemplateDTO;
import tech.highmarkglobal.bcm.web.dto.ProductDTO;
import tech.highmarkglobal.bcm.web.dto.ProductDomainDTO;
import tech.highmarkglobal.bcm.web.dto.PurchaseDTO;
import tech.highmarkglobal.bcm.web.dto.SmartContractDTO;
import tech.highmarkglobal.bcm.web.dto.TestResultDTO;
import tech.highmarkglobal.bcm.web.dto.UserDTO;
import tech.highmarkglobal.bcm.web.dto.UserDetailsDTO;
import tech.highmarkglobal.bcm.web.dto.ValidatorDTO;

/**
 * This class will have transformers from and to different types we need. A
 * common class is used in this instance due to the simplicity of the
 * application.
 * 
 * @author Ramesh
 *
 */
@Component
public class DTOToDomainTransformer {

	
	static ProductDomainRepository productDomainRepo;
	static AssetTypesRepository assetTypeRepo;
	
	@Autowired
	public void initialize(ProductDomainRepository domainRepository, AssetTypesRepository assetTypesRepository) {
		DTOToDomainTransformer.productDomainRepo = domainRepository;
		DTOToDomainTransformer.assetTypeRepo = assetTypesRepository;
	}
	/*public static Product transform(final ProductDTO productDto) {
		return new Product(productDto.getName(), productDto.getStage());
	}*/

	public static PricingTemplate transform(PricingTemplateDTO dto) {
		PricingTemplate pricingDetail = new PricingTemplate();
		pricingDetail.setOption(dto.getOption());
		pricingDetail.setPaymentOption(dto.getPaymentOption());
		pricingDetail.setPlanName(dto.getPlanName());
		pricingDetail.setTokens(dto.getTokens());
		pricingDetail.setUsers(dto.getUsers());
		pricingDetail.setPrice(dto.getPrice());
		return pricingDetail;
	}

	public static PricingTemplateDTO transform(PricingTemplate pricingDetail) {
		PricingTemplateDTO pricingTemplateDTO = new PricingTemplateDTO();
		pricingTemplateDTO.setOption(pricingDetail.getOption());
		pricingTemplateDTO.setPaymentOption(pricingDetail.getPaymentOption());
		pricingTemplateDTO.setPlanName(pricingDetail.getPlanName());
		pricingTemplateDTO.setTokens(pricingDetail.getTokens());
		pricingTemplateDTO.setUsers(pricingDetail.getUsers());
		pricingTemplateDTO.setPrice(pricingDetail.getPrice());
		return pricingTemplateDTO;
	}

	public static BountyProgram transform(BountyProgramDTO bountyProgramDTO) {
		BountyProgram bountyProgram=new BountyProgram();
		bountyProgram.setBountyName(bountyProgramDTO.getBountyName());
		bountyProgram.setAssetID(bountyProgramDTO.getAssetID());
		bountyProgram.setReward(bountyProgramDTO.getReward());
		bountyProgram.setNoOfParticipants(bountyProgramDTO.getNoOfParticipants());
		bountyProgram.setClaimConditons(bountyProgramDTO.getClaimConditons());
/*		bountyProgram.setClaimFeatureType(bountyProgramDTO.getClaimFeatureType());
		bountyProgram.setClaimFeatureName(bountyProgramDTO.getClaimFeatureName())*/;
		bountyProgram.setFeatures(bountyProgramDTO.getFeatures());
		bountyProgram.setBountyDesc(bountyProgramDTO.getBountyDesc());
		bountyProgram.setStartDate(bountyProgramDTO.getStartDate());
		bountyProgram.setEndDate(bountyProgramDTO.getEndDate());
		bountyProgram.setTokensOffered(bountyProgramDTO.getTokensOffered());
		bountyProgram.setTags(bountyProgramDTO.getTags());
		bountyProgram.setServiceChargs(bountyProgramDTO.getServiceChargs());
		bountyProgram.setOfferedType(bountyProgramDTO.getOfferedType());
		bountyProgram.setAction(bountyProgramDTO.getAction());
		bountyProgram.setTestingCategory(bountyProgramDTO.getTestingCategory());
		bountyProgram.setMilestones(bountyProgramDTO.getMilestones());
		bountyProgram.setSearched(bountyProgramDTO.getSearched());
		bountyProgram.setResponses(bountyProgramDTO.getResponses());
		bountyProgram.setContracts(bountyProgramDTO.getContracts());
		bountyProgram.setProducerId(bountyProgramDTO.getProducerId());
		bountyProgram.setProducerName(bountyProgramDTO.getProducerName());
		bountyProgram.setProductId(bountyProgramDTO.getProductId());
	return bountyProgram;
}
/**
 * 
 * @param bounty
 * @return
 */
public static BountyProgramDTO transform(BountyProgram bountyProgram) {
	BountyProgramDTO bountyProgramDTO=new BountyProgramDTO();
	bountyProgramDTO.setBountyName(bountyProgram.getBountyName());
	bountyProgramDTO.setAssetID(bountyProgram.getAssetID());
	bountyProgramDTO.setReward(bountyProgram.getReward());
	bountyProgramDTO.setNoOfParticipants(bountyProgram.getNoOfParticipants());
	bountyProgramDTO.setClaimConditons(bountyProgram.getClaimConditons());
/*	bountyProgramDTO.setClaimFeatureType(bountyProgram.getClaimFeatureType());
	bountyProgramDTO.setClaimFeatureName(bountyProgram.getClaimFeatureName());*/
	bountyProgramDTO.setFeatures(bountyProgram.getFeatures());
	bountyProgramDTO.setBountyDesc(bountyProgram.getBountyDesc());
	bountyProgramDTO.setStartDate(bountyProgram.getStartDate());
	bountyProgramDTO.setEndDate(bountyProgram.getEndDate());
	bountyProgramDTO.setTokensOffered(bountyProgram.getTokensOffered());
	bountyProgramDTO.setTags(bountyProgram.getTags());
	bountyProgramDTO.setServiceChargs(bountyProgram.getServiceChargs());
	bountyProgramDTO.setOfferedType(bountyProgram.getOfferedType());
	bountyProgramDTO.setAction(bountyProgram.getAction());
	bountyProgramDTO.setTestingCategory(bountyProgram.getTestingCategory());
	bountyProgramDTO.setMilestones(bountyProgram.getMilestones());
	bountyProgramDTO.setSearched(bountyProgram.getSearched());
	bountyProgramDTO.setResponses(bountyProgram.getResponses());
	bountyProgramDTO.setContracts(bountyProgram.getContracts());
	bountyProgramDTO.setProducerId(bountyProgram.getProducerId());
	bountyProgramDTO.setProducerName(bountyProgram.getProducerName());
	bountyProgramDTO.setProductId(bountyProgram.getProductId());
	return bountyProgramDTO;
}

	public static SmartContract transform(SmartContractDTO smartContractDTO) {
		SmartContract contract=new SmartContract();
		contract.setJsonObject(smartContractDTO.getJsonObject());
		contract.setName(smartContractDTO.getName());
		contract.setSecondParty(smartContractDTO.getSecondParty());
		return contract;
		
	}

	public static SmartContractDTO transform(SmartContract createdSmartContract) {
		SmartContractDTO contract=new SmartContractDTO();
		contract.setJsonObject(createdSmartContract.getJsonObject());
		contract.setName(createdSmartContract.getName());
		contract.setSecondParty(createdSmartContract.getSecondParty());
		return contract;
		
	}
	
	public static Product transform(ProductDTO productDTO) {
		Product product = new Product();
		product.setName(productDTO.getName());
		product.setStage(productDTO.getStage());
		product.setAssetType(assetTypeRepo.findById(productDTO.getAssetType()));
		product.setServiceCharge(productDTO.getServiceCharge());
		product.setProducerId(productDTO.getProducerId());
		List<String> prodDomainIds = productDTO.getDomainId();
		List<ProductDomain> pdLst = new ArrayList<ProductDomain>();
		for(String id:prodDomainIds) {
			ProductDomain pd = productDomainRepo.findById(id);
			pdLst.add(pd);
		}
		product.setProductDomain(pdLst);
		
		product.setPricingTemplate(productDTO.getPricingTemplateDto());
		return product;
	}
	
	
	public static ProductDTO transform(Product product) {
		ProductDTO prodDTO = new ProductDTO();
		prodDTO.setId(product.getId());
		prodDTO.setName(product.getName());
		prodDTO.setStage(product.getStage());
		prodDTO.setAssetType(product.getAssetType().getId());
		prodDTO.setServiceCharge(product.getServiceCharge());
		prodDTO.setProducerId(product.getProducerId());
		List<ProductDomain> prodDomains = product.getProductDomain();
		List<String> domainIds = new ArrayList<String>();
		for(ProductDomain pd:prodDomains) {
			domainIds.add(pd.getId());
		}
		prodDTO.setDomainId(domainIds);
		
		prodDTO.setPricingTemplateDto(product.getPricingTemplate());
		
		return prodDTO;
	}
	
	public static ProductDomain transform(ProductDomainDTO productDTO) {
		ProductDomain pd = new ProductDomain();
		pd.setDomainName(productDTO.getDomainName());
		pd.setParentId(productDTO.getParentId());
		return pd;
	}
	
	public static ProductDomainDTO transform(ProductDomain pd) {
		ProductDomainDTO pdDTO = new ProductDomainDTO();
		pdDTO.setDomainName(pd.getDomainName());
		pdDTO.setParentId(pd.getParentId());
		return pdDTO;
	}
	
	public static AssetTypeDTO transform(AssetType asset) {
		AssetTypeDTO assetDTO  = new AssetTypeDTO();
		assetDTO.setName(asset.getName());
		assetDTO.setDescription(asset.getDescription());
		return assetDTO;
	}
	
	public static AssetType transform(AssetTypeDTO assetDTO) {
		AssetType asset = new AssetType();
		asset.setName(assetDTO.getName());
		asset.setDescription(assetDTO.getDescription());
		return asset;
	}

/*	
	public static UserDetails transform(UserDetailsDTO userDetailsDTO) {
		UserDetails userDetails = new UserDetails();
		userDetails.setUserId(userDetailsDTO.getUserId());
		userDetails.setWallet(userDetailsDTO.getWallet());
		userDetails.setAddressDetails(DTOToDomainTransformer.transform(userDetailsDTO.getAddressDetailsDTO()));
		userDetails.setBillingDetails(DTOToDomainTransformer.transform(userDetailsDTO.getBillingDetailsDTO()));
		userDetails.setDepositDetails(DTOToDomainTransformer.transform(userDetailsDTO.getDepositDetailsDTO()));
		return userDetails;
	}
	
	public static UserDetailsDTO transform(UserDetails userDetails) {
		UserDetailsDTO userDetailsDTO = new UserDetailsDTO();
		userDetailsDTO.setUserId(userDetails.getUserId());
		userDetailsDTO.setWallet(userDetails.getWallet());
		userDetailsDTO.setAddressDetailsDTO(DTOToDomainTransformer.transform(userDetails.getAddressDetails()));
		userDetailsDTO.setBillingDetailsDTO(DTOToDomainTransformer.transform(userDetails.getBillingDetails()));
		userDetailsDTO.setDepositDetailsDTO(DTOToDomainTransformer.transform(userDetails.getDepositDetails()));
		return userDetailsDTO;
	}*/
	
	public static AddressDetailsDTO transform(AddressDetails addrDetails) {
		AddressDetailsDTO addrDetailsDTO = new AddressDetailsDTO();
		addrDetailsDTO.setAddress(addrDetails.getAddress());
		addrDetailsDTO.setPhoneNumber(addrDetails.getPhoneNumber());
		addrDetailsDTO.setWebsite(addrDetails.getWebsite());
		return addrDetailsDTO;
	}
	
	public static AddressDetails transform(AddressDetailsDTO addrDetailsDTO) {
		AddressDetails addrDetails = new AddressDetails();
		addrDetails.setAddress(addrDetailsDTO.getAddress());
		addrDetails.setPhoneNumber(addrDetailsDTO.getPhoneNumber());
		addrDetails.setWebsite(addrDetailsDTO.getWebsite());
		return addrDetails;
	}
	
	public static BillingDetailsDTO transform(BillingDetails billingDetails) {
		BillingDetailsDTO billingDetailsDTO = new BillingDetailsDTO();
		billingDetailsDTO.setCardHolderName(billingDetails.getCardHolderName());
		billingDetailsDTO.setCardNumber(billingDetails.getCardNumber());
		billingDetailsDTO.setValidity((HighmarkUtil.DateToStr(billingDetails.getValidity())));
		return billingDetailsDTO;
	}
	
	public static BillingDetails transform(BillingDetailsDTO billingDetailsDTO) {
		BillingDetails billingDetails = new BillingDetails();
		billingDetails.setCardHolderName(billingDetailsDTO.getCardHolderName());
		billingDetails.setCardNumber(billingDetailsDTO.getCardNumber());
		billingDetails.setValidity(HighmarkUtil.StrtoDate(billingDetailsDTO.getValidity()));
		return billingDetails;
	}
	
	public static DepositDetailsDTO transform(DepositDetails depositDetails) {
		DepositDetailsDTO depositDetailsDTO = new DepositDetailsDTO();
		depositDetailsDTO.setAccHolderName(depositDetails.getAccHolderName());
		depositDetailsDTO.setBankAccountNumber(depositDetails.getBankAccountNumber());
		depositDetailsDTO.setBankLocation(depositDetails.getBankLocation());
		depositDetailsDTO.setRoutingNumber(depositDetails.getRoutingNumber());
		return depositDetailsDTO;
		
	}
	
	public static DepositDetails transform(DepositDetailsDTO depositDetailsDTO) {
		DepositDetails depositDetails = new DepositDetails();
		depositDetails.setAccHolderName(depositDetailsDTO.getAccHolderName());
		depositDetails.setBankAccountNumber(depositDetailsDTO.getBankAccountNumber());
		depositDetails.setBankLocation(depositDetailsDTO.getBankLocation());
		depositDetails.setRoutingNumber(depositDetailsDTO.getRoutingNumber());
		return depositDetails;
	}
	
	public static User transform(UserDTO userDTO) {
		User user = new User();
		//user.setContactNumber(userDTO.getContactNumber());
		user.setEmail(userDTO.getEmail());
	//	user.setName(userDTO.getName());
		//user.setOrganizationName(userDTO.getOrganizationName());
		user.setPassword(userDTO.getPassword());
		//user.setUserCategory(userDTO.getUserCategory());
		user.setUserDetailsId(userDTO.getUserDetailsId());
		user.setUserType(userDTO.getUserType());
		user.setName(userDTO.getName());
		user.setCorporationName(userDTO.getCorporationName());
		return user;
	}
	
	public static UserDTO transform(User user) {
		UserDTO userDTO = new UserDTO();
		//userDTO.setContactNumber(user.getContactNumber());
		userDTO.setEmail(user.getEmail());
		//userDTO.setName(user.getName());
		//userDTO.setOrganizationName(user.getOrganizationName());
		userDTO.setPassword(user.getPassword());
		//userDTO.setUserCategory(user.getUserCategory());
		userDTO.setUserDetailsId(user.getUserDetailsId());
		userDTO.setUserType(user.getUserType());
		userDTO.setName(user.getName());
		userDTO.setCorporationName(user.getCorporationName());
		return userDTO;
	}
	
	public static Purchase transform(PurchaseDTO purchaseDTO) {
		Purchase purchase = new Purchase();
		purchase.setProducerId(purchaseDTO.getProducerId());
		purchase.setProducerName(purchaseDTO.getProducerName());
		purchase.setConsumerId(purchaseDTO.getConsumerId());
		purchase.setConsumerName(purchaseDTO.getConsumerName());
		purchase.setAssetId(purchaseDTO.getAssetId());
		purchase.setAssetName(purchaseDTO.getAssetName());
		purchase.setPurchaseDate(purchaseDTO.getPurchaseDate());
		purchase.setPrice(purchaseDTO.getPrice());
		purchase.setPriceUnit(purchaseDTO.getPriceUnit());
		purchase.setLicensing(purchaseDTO.getLicensing());
		purchase.setPricingOption(purchaseDTO.getPriceUnit());
		purchase.setFeatures(purchaseDTO.getFeatures());
		return purchase;
	}
	
	public static PurchaseDTO transform(Purchase purchase) {
		PurchaseDTO purchaseDTO = new PurchaseDTO();
		purchaseDTO.setProducerId(purchase.getProducerId());
		purchaseDTO.setProducerName(purchase.getProducerName());
		purchaseDTO.setConsumerId(purchase.getConsumerId());
		purchaseDTO.setConsumerName(purchase.getConsumerName());
		purchaseDTO.setAssetId(purchase.getAssetId());
		purchaseDTO.setAssetName(purchase.getAssetName());
		purchaseDTO.setPurchaseDate(purchase.getPurchaseDate());
		purchaseDTO.setPrice(purchase.getPrice());
		purchaseDTO.setPriceUnit(purchase.getPriceUnit());
		purchaseDTO.setLicensing(purchase.getLicensing());
		purchaseDTO.setPricingOption(purchase.getPriceUnit());
		purchaseDTO.setFeatures(purchase.getFeatures());

		return purchaseDTO;
	}
	public static AssetDTO transform(Asset asset) {
		AssetDTO assetDTO  = new AssetDTO();

		assetDTO.setName(asset.getName());
		assetDTO.setAssetType(asset.getAssetType());
        assetDTO.setDomain(asset.getDomain());
        assetDTO.setInterfaceType(asset.getInterfaceType());	
        assetDTO.setDeployomentType(asset.getDeployomentType());	
        assetDTO.setOsType(asset.getOsType());
        assetDTO.setLicensingTerms(asset.getLicensingTerms());	
        assetDTO.setPrice(asset.getPrice());	
        assetDTO.setPricingUnit(asset.getPricingUnit());	
        assetDTO.setFeatures(asset.getFeatures());
/*       assetDTO.setFeatureType(asset.getFeatureType());	
        assetDTO.setFeatureTechnology(asset.getFeatureTechnology());	
        assetDTO.setFeatureName(asset.getFeatureName());	
        assetDTO.setFeatureDescription(asset.getFeatureDescription());*/
        assetDTO.setProducerName(asset.getProducerName());
        assetDTO.setServiceDuraiton(asset.getServiceDuraiton());
        assetDTO.setServiceBundledPrice(asset.getServiceBundledPrice());
        assetDTO.setPersistentPerCPU(asset.getPersistentPerCPU());
        assetDTO.setPersistentPerFeature(asset.getPersistentPerFeature());
        assetDTO.setPersistentPrice(asset.getPersistentPrice());
		
		return assetDTO;
	}
	
	public static Asset transform(AssetDTO assetDTO) {
		Asset asset = new Asset();
		asset.setName(assetDTO.getName());
		asset.setAssetType(assetDTO.getAssetType());
        asset.setDomain(assetDTO.getDomain());
        asset.setInterfaceType(assetDTO.getInterfaceType());	
        asset.setDeployomentType(assetDTO.getDeployomentType());	
        asset.setOsType(assetDTO.getOsType());
        asset.setLicensingTerms(assetDTO.getLicensingTerms());	
        asset.setPrice(assetDTO.getPrice());	
        asset.setPricingUnit(assetDTO.getPricingUnit());	
        asset.setFeatures(assetDTO.getFeatures());
     
/*        asset.setFeatureType(assetDTO.getFeatureType());	
        asset.setFeatureTechnology(assetDTO.getFeatureTechnology());	
        asset.setFeatureName(assetDTO.getFeatureName());	
        asset.setFeatureDescription(assetDTO.getFeatureDescription());*/	
        asset.setProducerName(assetDTO.getProducerName());

        asset.setServiceDuraiton(assetDTO.getServiceDuraiton());
        asset.setServiceBundledPrice(assetDTO.getServiceBundledPrice());
        asset.setPersistentPerCPU(assetDTO.getPersistentPerCPU());
        asset.setPersistentPerFeature(assetDTO.getPersistentPerFeature());
        asset.setPersistentPrice(assetDTO.getPersistentPrice());
		return asset;
	}
	public static JoinBountyDTO transform(JoinBounty joinBounty) {
		JoinBountyDTO joinBountyDTO  = new JoinBountyDTO();
		joinBountyDTO.setBountyID(joinBounty.getBountyID());
		joinBountyDTO.setBountyName(joinBounty.getBountyName());
		joinBountyDTO.setAssetID(joinBounty.getAssetID());
		joinBountyDTO.setAssetName(joinBounty.getAssetName());
		joinBountyDTO.setProducerID(joinBounty.getProducerID());
		joinBountyDTO.setProducerName(joinBounty.getProducerName());
		joinBountyDTO.setTesterID(joinBounty.getTesterID());
		joinBountyDTO.setTesterName(joinBounty.getTesterName());
		joinBountyDTO.setJoinDate(joinBounty.getJoinDate());
		return joinBountyDTO;
	}
	
	public static JoinBounty transform(JoinBountyDTO joinBountyDTO) {
		JoinBounty joinBounty  = new JoinBounty();
		joinBounty.setBountyID(joinBountyDTO.getBountyID());
		joinBounty.setBountyName(joinBountyDTO.getBountyName());
		joinBounty.setAssetID(joinBountyDTO.getAssetID());
		joinBounty.setAssetName(joinBountyDTO.getAssetName());
		joinBounty.setProducerID(joinBountyDTO.getProducerID());
		joinBounty.setProducerName(joinBountyDTO.getProducerName());
		joinBounty.setTesterID(joinBountyDTO.getTesterID());
		joinBounty.setTesterName(joinBountyDTO.getTesterName());
		joinBounty.setJoinDate(joinBountyDTO.getJoinDate());
		return joinBounty;
	}
	
	//Adding after demo
	
	public static UserDetails transform(UserDetailsDTO userDetailsDTO) {
		UserDetails userDetails = new UserDetails();
		userDetails.setEntityType(userDetailsDTO.getEntityType());
		//userDetails.setPassword(userDetailsDTO.getPassword());
		userDetails.setCorporationName(userDetailsDTO.getCorporationName());
		userDetails.setContactFirstName(userDetailsDTO.getContactFirstName());
		userDetails.setContactLastName(userDetailsDTO.getContactLastName());
		userDetails.setContactEmail(userDetailsDTO.getContactEmail());
		userDetails.setContactPhoneNumber(userDetailsDTO.getContactPhoneNumber());
		userDetails.setAddress1(userDetailsDTO.getAddress1());
		userDetails.setAddres2(userDetailsDTO.getAddres2());
		userDetails.setCountry(userDetailsDTO.getCountry());
		userDetails.setCity(userDetailsDTO.getCity());
		userDetails.setProvince(userDetailsDTO.getProvince());
		userDetails.setState(userDetailsDTO.getState());
		userDetails.setPostalCode(userDetailsDTO.getPostalCode());
		userDetails.setCorporationNumber(userDetailsDTO.getCorporationNumber());
		userDetails.setRegistrationNumber(userDetailsDTO.getRegistrationNumber());
		userDetails.setTaxNumber(userDetailsDTO.getTaxNumber());
		userDetails.setYearlyTurnover(userDetailsDTO.getYearlyTurnover());
		userDetails.setNumberOfEmployees(userDetailsDTO.getNumberOfEmployees());
		userDetails.setUserType(userDetailsDTO.getUserType());
		
		return userDetails;
	}
	
	public static UserDetailsDTO transform(UserDetails userDetails) {
		UserDetailsDTO userDetailsDTO = new UserDetailsDTO();
		userDetailsDTO.setEntityType(userDetails.getEntityType());
		//userDetailsDTO.setPassword(userDetails.getPassword());
		userDetailsDTO.setCorporationName(userDetails.getCorporationName());
		userDetailsDTO.setContactFirstName(userDetails.getContactFirstName());
		userDetailsDTO.setContactLastName(userDetails.getContactLastName());
		userDetailsDTO.setContactEmail(userDetails.getContactEmail());
		userDetailsDTO.setContactPhoneNumber(userDetails.getContactPhoneNumber());
		userDetailsDTO.setAddress1(userDetails.getAddress1());
		userDetailsDTO.setAddres2(userDetails.getAddres2());
		userDetailsDTO.setCountry(userDetails.getCountry());
		userDetailsDTO.setCity(userDetails.getCity());
		userDetailsDTO.setProvince(userDetails.getProvince());
		userDetailsDTO.setState(userDetails.getState());
		userDetailsDTO.setPostalCode(userDetails.getPostalCode());
		userDetailsDTO.setCorporationNumber(userDetails.getCorporationNumber());
		userDetailsDTO.setRegistrationNumber(userDetails.getRegistrationNumber());
		userDetailsDTO.setTaxNumber(userDetails.getTaxNumber());
		userDetailsDTO.setYearlyTurnover(userDetails.getYearlyTurnover());
		userDetailsDTO.setNumberOfEmployees(userDetails.getNumberOfEmployees());
		userDetailsDTO.setUserType(userDetails.getUserType());
		return userDetailsDTO;
	}
	
	//Adding TestResult
	public static TestResult transform(TestResultDTO testResultDTO) {
		TestResult testResult= new TestResult();
		testResult.setBountyName(testResultDTO.getBountyName());
		testResult.setAssetName(testResultDTO.getAssetName());
/*		testResult.setFeatureType(testResultDTO.getFeatureType());
		testResult.setFunctionalFeature(testResultDTO.getFunctionalFeature());*/
		testResult.setFeatures(testResultDTO.getFeatures());
		testResult.setPlatformProvider(testResultDTO.getPlatformProvider());
		testResult.setPlatformOS(testResultDTO.getPlatformOS());
		testResult.setTestType(testResultDTO.getTestType());
		testResult.setToolsUsed(testResultDTO.getToolsUsed());
		testResult.setTestDate(testResultDTO.getTestDate());
		testResult.setRecommendation(testResultDTO.getRecommendation());
		testResult.setTestReport(testResultDTO.getTestReport());
		testResult.setURL(testResultDTO.getURL());
	
		testResult.setTestData(testResultDTO.getTestData());
		testResult.setScriptLog(testResultDTO.getScriptLog());
		testResult.setTestReportFilaname(testResultDTO.getTestReportFilaname());
		testResult.setTestDataFilename(testResultDTO.getTestDataFilename());
		testResult.setTestScriptFilename(testResultDTO.getTestScriptFilename());
		testResult.setScriptLogFilename(testResultDTO.getScriptLogFilename());
		return testResult;
	}
	
	public static TestResultDTO transform(TestResult testResult) {
		TestResultDTO testResultDTO= new TestResultDTO();
		testResultDTO.setBountyName(testResult.getBountyName());
		testResultDTO.setAssetName(testResult.getAssetName());
/*		testResultDTO.setFeatureType(testResult.getFeatureType());
		testResultDTO.setFunctionalFeature(testResult.getFunctionalFeature());*/
		testResultDTO.setFeatures(testResult.getFeatures());
		testResultDTO.setPlatformProvider(testResult.getPlatformProvider());
		testResultDTO.setPlatformOS(testResult.getPlatformOS());
		testResultDTO.setTestType(testResult.getTestType());
		testResultDTO.setToolsUsed(testResult.getToolsUsed());
		testResultDTO.setTestDate(testResult.getTestDate());
		testResultDTO.setRecommendation(testResult.getRecommendation());
		testResultDTO.setTestReport(testResult.getTestReport());
		testResultDTO.setURL(testResult.getURL());
	
		testResultDTO.setTestData(testResult.getTestData());
		testResultDTO.setScriptLog(testResult.getScriptLog());
		testResultDTO.setTestReportFilaname(testResult.getTestReportFilaname());
		testResultDTO.setTestDataFilename(testResult.getTestDataFilename());
		testResultDTO.setTestScriptFilename(testResult.getTestScriptFilename());
		testResultDTO.setScriptLogFilename(testResult.getScriptLogFilename());
		
		return testResultDTO;
	}
	
	//Adding Validator
	public static Validator transform(ValidatorDTO validatorDTO) {
		Validator validator= new Validator();
		validator.setBountyName(validatorDTO.getBountyName());
		validator.setAssetName(validatorDTO.getAssetName());
		validator.setRewards(validatorDTO.getRewards());
/*		validator.setClaimFeatureType(validatorDTO.getClaimFeatureType());
		validator.setClaimFeatureName(validatorDTO.getClaimFeatureName());*/
		validator.setFeatures(validatorDTO.getFeatures());
		validator.setPlatform(validatorDTO.getPlatform());
		validator.setToolsUsed(validatorDTO.getToolsUsed());
		validator.setRecommendation(validatorDTO.getRecommendation());
		validator.setTestReport(validatorDTO.getTestReport());
		validator.setTestScript(validatorDTO.getTestScript());
		validator.setScriptLog(validatorDTO.getScriptLog());
		validator.setSystemRecommendation(validatorDTO.getSystemRecommendation());
		validator.setSystemConfidence(validatorDTO.getSystemConfidence());
		validator.setValidationRecommendation(validatorDTO.getValidationRecommendation());
		validator.setValidationMethod(validatorDTO.getValidationMethod());
		validator.setValidationReport(validatorDTO.getValidationReport());
		validator.setValidatorBugsReport(validatorDTO.getValidatorBugsReport());
		validator.setValidatorTestScripts(validatorDTO.getValidatorTestScripts());
		validator.setValidatorScriptLog(validatorDTO.getValidatorScriptLog());
		validator.setValidatorDate(validatorDTO.getValidatorDate());
		validator.setBugsReportFilaname(validatorDTO.getBugsReportFilaname());
		validator.setTestScriptFilename(validatorDTO.getTestScriptFilename());
		validator.setScriptLogFilename(validatorDTO.getScriptLogFilename());
		validator.setValidationReportFilename(validatorDTO.getValidationReportFilename());
		validator.setValidatorBugsReportFilename(validatorDTO.getValidatorBugsReportFilename());
		validator.setValidatorTestScriptsFilename(validatorDTO.getValidatorTestScriptsFilename());
		validator.setValidatorScriptLogFilename(validatorDTO.getValidatorScriptLogFilename());
		
		
		return validator;
	}
	
	public static ValidatorDTO transform(Validator validator) {
		ValidatorDTO validatorDTO= new ValidatorDTO();
		validatorDTO.setBountyName(validator.getBountyName());
		validatorDTO.setAssetName(validator.getAssetName());
		validatorDTO.setRewards(validator.getRewards());
/*		validatorDTO.setClaimFeatureType(validator.getClaimFeatureType());
		validatorDTO.setClaimFeatureName(validator.getClaimFeatureName());*/
		validatorDTO.setFeatures(validator.getFeatures());
		validatorDTO.setPlatform(validator.getPlatform());
		validatorDTO.setToolsUsed(validator.getToolsUsed());
		validatorDTO.setRecommendation(validator.getRecommendation());
		validatorDTO.setTestReport(validatorDTO.getTestReport());
		validatorDTO.setTestScript(validator.getTestScript());
		validatorDTO.setScriptLog(validator.getScriptLog());
		validatorDTO.setSystemRecommendation(validator.getSystemRecommendation());
		validatorDTO.setSystemConfidence(validator.getSystemConfidence());
		validatorDTO.setValidationRecommendation(validator.getValidationRecommendation());
		validatorDTO.setValidationMethod(validator.getValidationMethod());
		validatorDTO.setValidationReport(validator.getValidationReport());
		validatorDTO.setValidatorBugsReport(validator.getValidatorBugsReport());
		validatorDTO.setValidatorTestScripts(validator.getValidatorTestScripts());
		validatorDTO.setValidatorScriptLog(validator.getValidatorScriptLog());
		validatorDTO.setValidatorDate(validator.getValidatorDate());
		validatorDTO.setBugsReportFilaname(validator.getBugsReportFilaname());
		validatorDTO.setTestScriptFilename(validator.getTestScriptFilename());
		validatorDTO.setScriptLogFilename(validator.getScriptLogFilename());
		validatorDTO.setValidationReportFilename(validator.getValidationReportFilename());
		validatorDTO.setValidatorBugsReportFilename(validator.getValidatorBugsReportFilename());
		validatorDTO.setValidatorTestScriptsFilename(validator.getValidatorTestScriptsFilename());
		validatorDTO.setValidatorScriptLogFilename(validator.getValidatorScriptLogFilename());
		return validatorDTO;
	}
	
	
}
