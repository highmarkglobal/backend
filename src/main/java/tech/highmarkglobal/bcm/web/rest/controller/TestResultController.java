package tech.highmarkglobal.bcm.web.rest.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


import com.mongodb.gridfs.GridFSDBFile;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import tech.highmarkglobal.bcm.domain.TestResult;
import tech.highmarkglobal.bcm.service.BlockchainTestResultService;
import tech.highmarkglobal.bcm.service.TestResultService;
import tech.highmarkglobal.bcm.web.dto.TestResultDTO;

@RestController
@RequestMapping("/api/testResult")
@Api(value = "/testResult-controller", description = "Create and manage various product doamins in the system")
public class TestResultController {

	@Autowired
	TestResultService testResultService;
	
	@Autowired
	private  GridFsOperations gridFsOperations;

	@Autowired
	BlockchainTestResultService bcTestResultService;

    @ApiOperation(value = "Get all Test Results Type in ths system", httpMethod = "GET", response = TestResultDTO.class, responseContainer = "List")
    @RequestMapping(value = "/getAllTestResults", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TestResult>> getAllModules() {
        List<TestResult> testResult = testResultService.findAll();
        return new ResponseEntity<>(testResult, HttpStatus.OK);
    }
	
	@ApiOperation(value = "Add TestResults to existing System", httpMethod = "POST", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid TestResults"),
            @ApiResponse(code = 500, message = "Error encountered when adding TestResult"),
            @ApiResponse(code = 200, message = "Test Results added")})
    @RequestMapping(value = "/saveTestResult", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE, consumes = { "multipart/form-data" })
	//@PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE, consumes = { "multipart/form-data" })
	 public ResponseEntity<?> addTestResult  (@RequestPart(value= "info") TestResultDTO testResultDTO,@RequestPart(value = "reportfile", required = false) MultipartFile reportFile, @RequestPart(value = "testdatafile", required = false) MultipartFile testDatafile ,@RequestPart(value = "scriptlogfile", required = false) MultipartFile scriptLogFile) throws IOException
	 {
		TestResult testResult = testResultService.saveTestResult(testResultDTO, (reportFile==null)?null:reportFile.getBytes(),(testDatafile==null)?null:testDatafile.getBytes(),(scriptLogFile==null)?null:scriptLogFile.getBytes());

		HttpStatus status = bcTestResultService.add(testResult);
		
		return new ResponseEntity<>("Test Results added to System", HttpStatus.OK);
		
	 }
	 
	 @RequestMapping(value = "/getTestreport", method = RequestMethod.GET)
	public String retrieveTestFile(String id) throws IOException{
		// read file from MongoDB
		GridFSDBFile imageFile = gridFsOperations.findOne(new Query(Criteria.where("_id").is(id)));
		
		
		//File relativeFile = new File("/testingvalidator/test.ppt");
		// Save file back to local disk
		//imageFile.writeTo(relativeFile);
		
		System.out.println(" File Name:" + imageFile.getFilename() + imageFile.getId());
		
		return "Done";
	}
	 

}