package tech.highmarkglobal.bcm.web.rest.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import tech.highmarkglobal.bcm.domain.User;
import tech.highmarkglobal.bcm.domain.UserDetails;
import tech.highmarkglobal.bcm.repo.UserDetailsRepository;
import tech.highmarkglobal.bcm.repo.UserRepository;
import tech.highmarkglobal.bcm.security.JwtTokenProvider;
import tech.highmarkglobal.bcm.service.BlockchainParticipantService;
import tech.highmarkglobal.bcm.service.UserService;
import tech.highmarkglobal.bcm.web.dto.LoginDTO;
import tech.highmarkglobal.bcm.web.dto.UserDTO;
import tech.highmarkglobal.bcm.web.dto.UserDetailsDTO;
import tech.highmarkglobal.bcm.web.payload.AppApiResponse;
import tech.highmarkglobal.bcm.web.payload.JwtAuthenticationResponse;
import tech.highmarkglobal.bcm.web.transformer.DTOToDomainTransformer;

/**
 * @author arun
 */
@RestController
@RequestMapping("/api/auth")
@Api(value = "/auth-controller", description = "Authentication and Creation of user in the system")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;
    
    @Autowired
    private JwtTokenProvider tokenProvider;
    

    
    @Autowired
    private UserRepository userRepository;
	@Autowired
	
    private PasswordEncoder passwordEncoder;
	

	@Autowired
	private UserDetailsRepository userDetailsRepo;
    
    @Autowired
    private BlockchainParticipantService participantService;

    @ApiOperation(value = "User SignIN ", httpMethod = "POST", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully signed IN"),
            @ApiResponse(code = 400, message = "Bad Credentials. Either username/password is not matched")})
    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginDTO loginDTO) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginDTO.getEmail(),
                        loginDTO.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        User user = userService.getUserByEmail(loginDTO.getEmail());
        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt,user.getId(),user.getName(),user.getEmail(),user.getUserType()));
    }

    @ApiOperation(value = "Create User ", httpMethod = "POST", response = UserDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Adding the user is successfully"),
            @ApiResponse(code = 400, message = "Username/Email already exists"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody UserDetailsDTO signUpDTO) {
    	
    	if(userService.isEmailExits(signUpDTO.getContactEmail())){
            return new ResponseEntity<AppApiResponse>(new AppApiResponse(false, "Email Address already in use!"),
                    HttpStatus.BAD_REQUEST);
        }
        
        UserDetails userDetails = userService.save(signUpDTO);

        /*
         * AXIUS BLOCKCHAIN
         * save a new user in the blockchain as a participant in chaincode         *   
         */
       
        HttpStatus status = participantService.add(userDetails);
        // the result of adding participant to blockchain will be handled later
        // if status === 200 --> OK
        
        return new ResponseEntity<>(signUpDTO, HttpStatus.OK);
    }
    
    @ApiOperation(value = "getUserDetailsById", httpMethod = "GET", response = UserDetailsDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid UserDetails "),
            @ApiResponse(code = 500, message = "Error encountered while adding UserDetails "),
            @ApiResponse(code = 200, message = "UserDetails added")})
    @RequestMapping(value = "/getUserDetailsById", method = RequestMethod.GET)
    public ResponseEntity<?> getUserDetails(String userId) {
		
		UserDetails ud = userService.getUserDetails(userId);
		
		UserDetailsDTO dto = DTOToDomainTransformer.transform(ud);
		
		return new ResponseEntity<>(dto, HttpStatus.OK);
		
	}
    
    
    @ApiOperation(value = "UpdatingthePassword", httpMethod = "POST")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid UserDetails "),
            @ApiResponse(code = 500, message = "Error encountered while adding UserDetails "),
            @ApiResponse(code = 200, message = "UserDetails added")})
    @RequestMapping(value = "/UpdatingPassword", method = RequestMethod.POST)
    public ResponseEntity<?> UpdatingPassword(String userId,String password) {
		
		UserDetails ud = userDetailsRepo.findByContactEmail(userId);
		UserDetailsDTO dto = DTOToDomainTransformer.transform(ud);
		User user=userService.getUserByEmail(userId);
		
		ud.setPassword(passwordEncoder.encode(password));
		
		UserDetails userReturn=userDetailsRepo.save(ud);
		
		
		user.setPassword(userReturn.getPassword());
		
		userRepository.save(user);
		
		return new ResponseEntity<>(dto, HttpStatus.OK);
		
	}
}
