package tech.highmarkglobal.bcm.web.rest.controller;


import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import tech.highmarkglobal.bcm.domain.JoinBounty;
import tech.highmarkglobal.bcm.exception.ResourceNotFoundException;
import tech.highmarkglobal.bcm.service.BlockchainContractBountyService;
import tech.highmarkglobal.bcm.service.JoinBountyService;
import tech.highmarkglobal.bcm.web.dto.JoinBountyDTO;


@RestController
@RequestMapping("/api/bounty")
@Api(value = "/joinbounty-controller", description = "Create and manage various bounty in the system")
public class JoinBountyController {
	
	@Autowired 
	JoinBountyService joinBountyService;
	
	@Autowired
	BlockchainContractBountyService blockchainJoinBountyService;

	@ApiOperation(value = "Join Bounty to existing System", httpMethod = "POST", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid Bounty"),
            @ApiResponse(code = 500, message = "Error encountered when adding Asset"),
            @ApiResponse(code = 200, message = "Bounty is successfully  added")})
    @RequestMapping(value = "/joinBounty", method = RequestMethod.POST)
    public ResponseEntity<?> addAsset(@RequestBody JoinBountyDTO joinBountyDTO) {
		
		
		JoinBounty joinBounty = joinBountyService.saveJoinBounty(joinBountyDTO);
		
		/*
         * AXIUS BLOCKCHAIN
         * save a new user in the blockchain as a participant in chaincode         *   
         */
       
        HttpStatus status = blockchainJoinBountyService.join(joinBounty);
        // the result of adding participant to blockchain will be handled later
        // if status === 200 --> OK

		
		 return new ResponseEntity<>("Join Bounty is successfully added to the System", HttpStatus.OK);
		
	}


    @ApiOperation(value = "Get all Join Bounty data in ths system", httpMethod = "GET", response = JoinBountyDTO.class, responseContainer = "List")
    @RequestMapping(value = "/getAllJoinBounty", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<JoinBounty>> getAllModules() {
        List<JoinBounty> joinBounty = joinBountyService.findAll();
        if (joinBounty == null || joinBounty.isEmpty()) {
            String errorMessage = String.format("Cannot find any join bounty details configured in the System ");
            throw new ResourceNotFoundException(errorMessage);
        }
        return new ResponseEntity<>(joinBounty, HttpStatus.OK);
    }
    
	/**
	 * 
	 * @return
	 */
	@ApiOperation(value = "Find join bounty by bounty id", httpMethod = "GET", response = JoinBountyDTO.class, responseContainer = "List")
	@ApiResponses(value = {
	            @ApiResponse(code = 400, message = "Invalid bounty Id"),
	            @ApiResponse(code = 500, message = "Error encountered while fetching bounty"),
	            @ApiResponse(code = 200, message = "Asset fetched by bountyId")})
    @RequestMapping(value = "/joinBountyByBountyId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findByBountyId(String bountyID) {
		JoinBounty joinBounty = joinBountyService.getByBountyID(bountyID);
		
		return new ResponseEntity<>(joinBounty, HttpStatus.OK);
	}
	
	/**
	 * 
	 * @return
	 */
	@ApiOperation(value = "Find join bounty by Tester id", httpMethod = "GET", response = JoinBountyDTO.class, responseContainer = "List")
	@ApiResponses(value = {
	            @ApiResponse(code = 400, message = "Invalid Tester Id"),
	            @ApiResponse(code = 500, message = "Error encountered while fetching bounty"),
	            @ApiResponse(code = 200, message = "Asset fetched by bountyId")})
    @RequestMapping(value = "/bountyByTesterId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findByTesterId(String testerID) {
		Collection<JoinBounty> joinBounty = joinBountyService.getByTesterID(testerID);
		
		return new ResponseEntity<>(joinBounty, HttpStatus.OK);
	}
	



}
