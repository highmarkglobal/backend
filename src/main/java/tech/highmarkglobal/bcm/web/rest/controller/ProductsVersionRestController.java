package tech.highmarkglobal.bcm.web.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tech.highmarkglobal.bcm.domain.ProductVersion;
import tech.highmarkglobal.bcm.repo.ProductVersionRepository;
import tech.highmarkglobal.bcm.service.ProductVersionService;
import tech.highmarkglobal.bcm.web.dto.ProductVersionDTO;


@RestController
@RequestMapping("/api/products")
public class ProductsVersionRestController {
	
	
	@Autowired
	ProductVersionService productsVersionService;
	
	
	@Autowired
	ProductVersionRepository productVersionRepository;
	
	/**
	* Create a new ProductsVersion entity
	* 
	*/
	@RequestMapping(value = "/ProductsVersion", method = RequestMethod.POST)
	public ProductVersion newProductsVersion(@RequestBody ProductVersionDTO productversionDTO) {
		productsVersionService.saveProductsVersion(productversionDTO);
		return productVersionRepository.findByProductId(productversionDTO.getProductId());
	}

}
