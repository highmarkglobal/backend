package tech.highmarkglobal.bcm.web.rest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import tech.highmarkglobal.bcm.domain.UserDetails;
import tech.highmarkglobal.bcm.service.UserDetailsService;
import tech.highmarkglobal.bcm.web.dto.ProductDTO;
import tech.highmarkglobal.bcm.web.dto.UserDetailsDTO;
import tech.highmarkglobal.bcm.web.transformer.DTOToDomainTransformer;

@RestController
@RequestMapping("/api/userdetails")
@Api(value = "/userdetails-controller", description = "Create and manage various userdetails in the system")
public class UserDetailsController {
	
	 private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	    
	    @Autowired
	    private UserDetailsService userDetailsService;
	    
	    @ApiOperation(value = "Create UserDetails", httpMethod = "POST", response = UserDetailsDTO.class)
	    @ApiResponses(value = {
	            @ApiResponse(code = 400, message = "Invalid UserDetails "),
	            @ApiResponse(code = 500, message = "Error encountered while adding UserDetails "),
	            @ApiResponse(code = 200, message = "UserDetails added")})
	    @RequestMapping(value = "/saveUserDetails", method = RequestMethod.POST)
	    public ResponseEntity<?> addUserDetails(@RequestBody UserDetailsDTO userDetailsDTO) {
			
			userDetailsService.saveUserDetails(userDetailsDTO);
			
			return new ResponseEntity<>(userDetailsDTO, HttpStatus.OK);
			
		}
	    
/*	    @ApiOperation(value = "Add Money To Wallet", httpMethod = "POST", response = UserDetailsDTO.class)
	    @ApiResponses(value = {
	            @ApiResponse(code = 400, message = "Invalid UserDetails "),
	            @ApiResponse(code = 500, message = "Error encountered while adding money to wallet "),
	            @ApiResponse(code = 200, message = "Successfully added money to wallet")})
	    @RequestMapping(value = "/addMoneyToWallet", method = RequestMethod.POST)
	    public ResponseEntity<?> addMoneyToWallet(String userId, Double amount) {
	    	UserDetailsDTO userDetailsDTO = new UserDetailsDTO();
	    	UserDetails ud = userDetailsService.addMoneyToWallet(userId, amount);
	    	userDetailsDTO = DTOToDomainTransformer.transform(ud);
	    	return new ResponseEntity<>(userDetailsDTO, HttpStatus.OK);
	    }*/
	    
	    
	    @ApiOperation(value = "getUserDetailsById", httpMethod = "GET", response = UserDetailsDTO.class)
	    @ApiResponses(value = {
	            @ApiResponse(code = 400, message = "Invalid UserDetails "),
	            @ApiResponse(code = 500, message = "Error encountered while adding UserDetails "),
	            @ApiResponse(code = 200, message = "UserDetails added")})
	    @RequestMapping(value = "/getUserDetailsById", method = RequestMethod.GET)
	    public ResponseEntity<?> getUserDetails(String userId) {
			
			UserDetails ud = userDetailsService.getUserDetails(userId);
			
			UserDetailsDTO dto = DTOToDomainTransformer.transform(ud);
			
			return new ResponseEntity<>(dto, HttpStatus.OK);
			
		}
}
