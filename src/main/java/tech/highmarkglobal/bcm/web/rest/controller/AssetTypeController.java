package tech.highmarkglobal.bcm.web.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import tech.highmarkglobal.bcm.domain.AssetType;
import tech.highmarkglobal.bcm.exception.ResourceNotFoundException;
import tech.highmarkglobal.bcm.service.AssetTypeService;
import tech.highmarkglobal.bcm.web.dto.AssetTypeDTO;
import tech.highmarkglobal.bcm.web.dto.BountyProgramDTO;
import tech.highmarkglobal.bcm.web.dto.ProductDomainDTO;

@RestController
@RequestMapping("/api/asset")
@Api(value = "/assettype-controller", description = "Create and manage various product doamins in the system")
public class AssetTypeController {
	
	@Autowired 
	AssetTypeService assetTypeService;

	@ApiOperation(value = "Add Asset type to existing System", httpMethod = "POST", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid Asset Type"),
            @ApiResponse(code = 500, message = "Error encountered when adding Asset Type"),
            @ApiResponse(code = 200, message = "asset type added")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<?> addAssetType(@RequestBody AssetTypeDTO assetTypeDTO) {
		
		
		assetTypeService.saveAssetType(assetTypeDTO);
		
		 return new ResponseEntity<>("AssetType added to System", HttpStatus.OK);
		
	}
    @ApiOperation(value = "Get all Asset Type in ths system", httpMethod = "GET", response = ProductDomainDTO.class, responseContainer = "List")
    @RequestMapping(value = "/assetTypes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AssetType>> getAllModules() {
        List<AssetType> assetTypes = assetTypeService.findAll();
        if (assetTypes == null || assetTypes.isEmpty()) {
            String errorMessage = String.format("Cannot find any Asset Types configured in System ");
            throw new ResourceNotFoundException(errorMessage);
        }
        return new ResponseEntity<>(assetTypes, HttpStatus.OK);
    }
    
    /**
	 * 
	 * @param name
	 * @return
	 */
	@ApiOperation(value = "View Asset By Id", httpMethod = "GET", response = BountyProgramDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid Asset"),
            @ApiResponse(code = 500, message = "Error encountered while adding Asset"),
            @ApiResponse(code = 200, message = "Asset fetched")})
    @RequestMapping(value = "/viewAssetsById", method = RequestMethod.GET)
    public ResponseEntity<?> findById(String id) {
		AssetTypeDTO assetTypeDTO = assetTypeService.getById(id);
		//logger.info(" bountyProgram DTO :{}",bountyProgram);
		return new ResponseEntity<>(assetTypeDTO, HttpStatus.OK);
	}


}
