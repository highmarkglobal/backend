package tech.highmarkglobal.bcm.web.rest.controller;

import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import tech.highmarkglobal.bcm.domain.Validator;
import tech.highmarkglobal.bcm.service.BlockchainValidationResultService;
import tech.highmarkglobal.bcm.service.ValidatorService;
import tech.highmarkglobal.bcm.web.dto.ValidatorDTO;




@RestController
@RequestMapping("/api/validatorResults")
@Api(value = "/validatorResults-controller", description = "Create and manage various product doamins in the system")
public class ValidatorController {
	
	@Autowired 
	ValidatorService validatorService;
	
	@Autowired
	BlockchainValidationResultService blockchainValidationService;

	@ApiOperation(value = "Add validator results to existing System", httpMethod = "POST", response = String.class)
	@ApiResponses(value = {
	@ApiResponse(code = 400, message = "Invalid validator results"),
	@ApiResponse(code = 500, message = "Error encountered when adding validator results"),
	@ApiResponse(code = 200, message = "Validator results added")})
	@RequestMapping(value = "/saveTestResultRetest", method = RequestMethod.POST)
    public ResponseEntity<?> addValidatorRetest(@RequestPart(value ="info") ValidatorDTO validatorDTO,
    		@RequestPart(value = "validatorreportfile", required = false) MultipartFile validatorreportFile,@RequestPart(value = "validatortestreportfile", required = false) MultipartFile validatortestreportFile, @RequestPart(value = "validatortestscriptfile", required = false) MultipartFile validatortestScriptfile ,@RequestPart(value = "validatorscriptlogfile", required = false) MultipartFile validatorscriptLogFile) throws ParseException, IOException, java.text.ParseException {
		

		Validator validator = validatorService.savevalidatorResults(validatorDTO,
								(validatorreportFile==null)?null:validatorreportFile.getBytes(),
										(validatortestreportFile==null)?null:validatortestreportFile.getBytes(),
												(validatortestScriptfile==null)?null:validatortestScriptfile.getBytes(),
														(validatorscriptLogFile==null)?null:validatorscriptLogFile.getBytes());	
		
		/*
         * AXIUS BLOCKCHAIN
         * save a new user in the blockchain as a participant in chaincode         *   
         */       
        HttpStatus status = blockchainValidationService.add(validator);
        // the result of adding participant to blockchain will be handled later
        // if status === 200 --> OK
        
		 return new ResponseEntity<>("Test Results added to System", HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Add validator results to existing System", httpMethod = "POST", response = String.class)
	@ApiResponses(value = {
	@ApiResponse(code = 400, message = "Invalid validator results"),
	@ApiResponse(code = 500, message = "Error encountered when adding validator results"),
	@ApiResponse(code = 200, message = "Validator results added")})
	@RequestMapping(value = "/saveTestResult", method = RequestMethod.POST)
	public ResponseEntity<?> addValidator(@RequestPart(value ="info") ValidatorDTO validatorDTO) throws java.text.ParseException, ParseException
	{
		Validator validator = validatorService.savevalidatorResults(validatorDTO);
												
		/*
         * AXIUS BLOCKCHAIN
         * save a new user in the blockchain as a participant in chaincode         *   
         */       
        HttpStatus status = blockchainValidationService.add(validator);
        // the result of adding participant to blockchain will be handled later
        // if status === 200 --> OK
        return new ResponseEntity<>("Test Results added to System", HttpStatus.OK);
		
	}


}