package tech.highmarkglobal.bcm.web.rest.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import tech.highmarkglobal.bcm.domain.Product;
import tech.highmarkglobal.bcm.domain.ProductSearch;
import tech.highmarkglobal.bcm.exception.ResourceNotFoundException;
import tech.highmarkglobal.bcm.service.ProductService;
import tech.highmarkglobal.bcm.web.dto.PricingTemplateDTO;
import tech.highmarkglobal.bcm.web.dto.ProductDTO;
import tech.highmarkglobal.bcm.web.dto.ProductDomainDTO;
import tech.highmarkglobal.bcm.web.transformer.DTOToDomainTransformer;

@RestController
@RequestMapping("/api/products")
@Api(value = "/asset-controller", description = "Create and manage various product doamins in the system")
public class ProductsRestController {
	
	
	
	@Autowired
	private ProductService productService;
	
		/**
		 * 
		 * @param productDTO
		 * @return
		 */
	@ApiOperation(value = "Create Asset ", httpMethod = "POST", response = ProductDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid Asset "),
            @ApiResponse(code = 500, message = "Error encountered while adding Asset "),
            @ApiResponse(code = 200, message = "Asset added")})
    @RequestMapping(value = "/saveAsset", method = RequestMethod.POST)
    public ResponseEntity<?> addAsset(@RequestBody ProductDTO productDTO) {
		
		
		productDTO = productService.saveProduct(productDTO);
		
		if (productDTO == null) {
			return new ResponseEntity<>("Insufficient Balance", HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<>(productDTO, HttpStatus.OK);
		
	}
	/**
	 * 
	 * @return
	 */
	@ApiOperation(value = "Find asset by producer id", httpMethod = "GET", response = ProductDomainDTO.class, responseContainer = "List")
	@ApiResponses(value = {
	            @ApiResponse(code = 400, message = "Invalid Producer Id"),
	            @ApiResponse(code = 500, message = "Error encountered while fetching Asset"),
	            @ApiResponse(code = 200, message = "Asset fetched by producerId")})
    @RequestMapping(value = "/assetsByProducerId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getProductsByProducerId(String producerId) {
        Collection<ProductDTO> products = productService.findProductsByProducerId(producerId);
        if (products == null || products.isEmpty()) {
            String errorMessage = String.format("No Assets were created by the producer");
            throw new ResourceNotFoundException(errorMessage);
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }
	
	
	/**
	 * 
	 * @return
	 */
	@ApiOperation(value = "Get all Assets in ths system", httpMethod = "GET", response = ProductDomainDTO.class, responseContainer = "List")
    @RequestMapping(value = "/assets", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<ProductDTO>> getAllProducts() {
        Collection<ProductDTO> products = productService.findAll();
        if (products == null || products.isEmpty()) {
            String errorMessage = String.format("Cannot find any products in the System ");
            throw new ResourceNotFoundException(errorMessage);
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }
	
	/**
	 * 
	 * @param id
	 * @param productDTO
	 * @return
	 */
	@ApiOperation(value = "Update Product ",httpMethod = "PUT")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Returns update Asset  after update"),
            @ApiResponse(code = 400, message = "Missing details in the request"),
            @ApiResponse(code = 501, message = "Error encountered when adding Asset ")})
    @RequestMapping(value = {"/update/{assetname}"}, method = {RequestMethod.PUT})
    public ResponseEntity<?> updateAsset(@PathVariable("id") String id, @RequestBody ProductDTO productDTO) {
	        Product _product = productService.findById(id);
	        if (_product == null) 
	            return new ResponseEntity<>("No Product exist with the given Id :" + id, HttpStatus.BAD_REQUEST);
	        _product.setStage(productDTO.getStage());
	        Product update_product = productService.updateProduct(productDTO);
	        return new ResponseEntity<>(update_product, HttpStatus.OK);
	       
	}
	
	/**
	 * 
	 * @param assetname
	 * @return
	 */
	@ApiOperation(value = "Delete Asset ", httpMethod = "DELETE", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Deleted the asset successfully"),
            @ApiResponse(code = 404, message = "Cannot find asset name"),
            @ApiResponse(code = 500, message = "Cannot delete  asset ")})
    @RequestMapping(value = "/asset/{assetname}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteDeviceType(@ApiParam(name = "assetname", value = "Asset Name", required = true)
                                                   @PathVariable("assetname") String assetname) {
		String message="";
		
		try {
			productService.deleteAsset(assetname);
        } catch (Exception ex) {
            message = String.format("Failed to delete asset  name = '%s' ", assetname);
            return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
        }
		return new ResponseEntity<>("Deleted Asset with the given assetname = " + assetname, HttpStatus.OK);
		
	}
     /**
      * 
      * @param id
      * @return
      */
	@ApiOperation(value = "Get Asset pricing ", httpMethod = "GET", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Get the asset pricing details"),
            @ApiResponse(code = 404, message = "Cannot find pricing info")
            })
    @RequestMapping(value = "/assetpricing/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getPricings(@ApiParam(name = "id", value = "Id", required = true)
                                                   @PathVariable("id") String id) {
		StringBuffer message=new StringBuffer();
		List<PricingTemplateDTO> pricings=null;
		ProductDTO dto=new ProductDTO();
		 Product _product = productService.findById(id);
		  if (_product == null) 
		  {
			  message.append("No Product exist with the given Id :" + id);
	          return new ResponseEntity<>(message.toString(), HttpStatus.BAD_REQUEST);
		  }
		 /* dto.setName(_product.getName());
		  dto.setServiceCharge(_product.getServiceCharge());
		  dto.setStage(_product.getStage());
		  if(_product.getAssetType()!=null)
		  dto.setAssetType(_product.getAssetType().getName());
		  if(_product.getProductDomain()!=null)
		  dto.setDomainName(_product.getProductDomain().getDomainName());*/
		  dto=DTOToDomainTransformer.transform(_product);
		/*try {
			 pricings = productService.getPricingPolicies(_product.getName(), id);
			 if(pricings!=null)
			 dto.setPricingTemplateDtos(pricings);
        } catch (Exception ex) {
            message.append("Failed to get asset  pricings = '%s' "+ id);
            return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
        }*/
		return new ResponseEntity<>(dto, HttpStatus.OK);
		
	}
		
    
/*	@ApiOperation(value = "Get Assets by Price Range", httpMethod = "GET", response = ProductDTO.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Get assets by price range"),
            @ApiResponse(code = 404, message = "Cannot find products")
            })
    @RequestMapping(value = "/assetsByPriceRange", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAssetsByPriceRange(Double start, Double end) {
    	Collection<Product> products = productService.findProductsByPriceRange(start,end);
    	Collection<ProductDTO> productsDTOs = new ArrayList<ProductDTO>();
    	for(Product p : products) {
    		ProductDTO prodDto = DTOToDomainTransformer.transform(p);
    		productsDTOs.add(prodDto);
    	}
    	
    	return new ResponseEntity<>(productsDTOs, HttpStatus.OK);
    }*/
	
	 
	@ApiOperation(value = "Get Assets by Domain", httpMethod = "GET", response = ProductDTO.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Get assets by domain"),
            @ApiResponse(code = 404, message = "Cannot find products")
            })
    @RequestMapping(value = "/assetsByDomain", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAssetsByDomain(String prodDomain) {
    	Collection<Product> products = productService.findProductsByProductDomain(prodDomain);
    	Collection<ProductDTO> productsDTOs = new ArrayList<ProductDTO>();
    	for(Product p : products) {
    		ProductDTO prodDto = DTOToDomainTransformer.transform(p);
    		productsDTOs.add(prodDto);
    	}
    	
    	return new ResponseEntity<>(productsDTOs, HttpStatus.OK);
    }
	
	@ApiOperation(value = "Search Asset", httpMethod = "POST", response = ProductDTO.class, responseContainer =  "List")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid search criteria "),
            @ApiResponse(code = 500, message = "Error encountered while adding Asset "),
            @ApiResponse(code = 200, message = "Asset added")})
    @RequestMapping(value = "/searchAsset", method = RequestMethod.POST)
    public ResponseEntity<?> searchAsset(@RequestBody ProductSearch search) {
		
		Collection<Product> products = productService.searchAsset(search);
		Collection<ProductDTO> productsDTOs = new ArrayList<ProductDTO>();
		for(Product p : products) {
    		ProductDTO prodDto = DTOToDomainTransformer.transform(p);
    		productsDTOs.add(prodDto);
    	}
		
		return new ResponseEntity<>(productsDTOs, HttpStatus.OK);
	}

}
