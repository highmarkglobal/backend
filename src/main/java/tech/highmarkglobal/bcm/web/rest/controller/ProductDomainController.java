package tech.highmarkglobal.bcm.web.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import tech.highmarkglobal.bcm.domain.ProductDomain;
import tech.highmarkglobal.bcm.exception.ResourceNotFoundException;
import tech.highmarkglobal.bcm.service.DomainService;
import tech.highmarkglobal.bcm.web.dto.ProductDomainDTO;

@RestController
@RequestMapping("/api/productdomain")
@Api(value = "/productdomain-controller", description = "Create and manage various product doamins in the system")
public class ProductDomainController {
	
	@Autowired
	DomainService  domainService;
	
	@ApiOperation(value = "Add Product Domain to existing section", httpMethod = "POST", response = String.class)
	@ApiResponses(value = {
	        @ApiResponse(code = 400, message = "Invalid section Id or missing formset/formId/itemCount"),
	        @ApiResponse(code = 500, message = "Error encountered when adding Product Domain"),
	        @ApiResponse(code = 200, message = "product Domain Added")})
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ResponseEntity<?> addDomain(@RequestBody List<ProductDomainDTO> productDomain) {
		
		
		domainService.saveDomains(productDomain);
		
		return new ResponseEntity<>(productDomain, HttpStatus.OK);
		
	}
	@ApiOperation(value = "Get all Product Domain in this system", httpMethod = "GET", response = ProductDomainDTO.class, responseContainer = "List")
	@RequestMapping(value = "/productDomains", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductDomain>> getAllModules() {
	    List<ProductDomain> productDomains = domainService.findAll();
	    if (productDomains == null || productDomains.isEmpty()) {
	        String errorMessage = String.format("Cannot find any productDomains configured in System ");
	        throw new ResourceNotFoundException(errorMessage);
	    }
	    return new ResponseEntity<>(productDomains, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Get Product Domain by Id", httpMethod = "GET", response = ProductDomainDTO.class)
	@RequestMapping(value = "/productDomainById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
	        @ApiResponse(code = 400, message = "Invalid productDomainId"),
	        @ApiResponse(code = 500, message = "Error encountered while fetching product domain by ID"),
	        @ApiResponse(code = 200, message = "Product Domain fetched successfully")})
	public ResponseEntity<?> getDomainById(String domainId) {
		
		ProductDomainDTO productDomain = domainService.findByDomainId(domainId);
		return new ResponseEntity<>(productDomain, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Get Product Domain by Name", httpMethod = "GET", response = ProductDomainDTO.class, responseContainer = "List")
	@RequestMapping(value = "/productDomainByName", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
	        @ApiResponse(code = 400, message = "Invalid productDomainName"),
	        @ApiResponse(code = 500, message = "Error encountered while fetching product domain by Name"),
	        @ApiResponse(code = 200, message = "Product Domain fetched successfully")})
	public ResponseEntity<?> getDomainByName(String domainName) {
		
		List<ProductDomainDTO> productDomain = domainService.findByDomainName(domainName);
		return new ResponseEntity<>(productDomain, HttpStatus.OK);
	}

}
