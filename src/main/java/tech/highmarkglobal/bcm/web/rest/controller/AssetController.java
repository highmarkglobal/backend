package tech.highmarkglobal.bcm.web.rest.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.spring.web.json.Json;
import tech.highmarkglobal.bcm.domain.Asset;
import tech.highmarkglobal.bcm.domain.FeaturesDetails;
import tech.highmarkglobal.bcm.exception.ResourceNotFoundException;
import tech.highmarkglobal.bcm.service.AssetService;
import tech.highmarkglobal.bcm.service.BlockchainAssetService;
import tech.highmarkglobal.bcm.web.dto.AssetDTO;



@RestController
@RequestMapping("/api/asset")
@Api(value = "/asset-controller", description = "Create and manage various product doamins in the system")
public class AssetController {
	
	@Autowired 
	AssetService assetService;
	
	@Autowired
	BlockchainAssetService blockchainAssetService;

	@ApiOperation(value = "Add Asset to existing System", httpMethod = "POST", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid Asset"),
            @ApiResponse(code = 500, message = "Error encountered when adding Asset"),
            @ApiResponse(code = 200, message = "asset added")})
    @RequestMapping(value = "/saveAsset", method = RequestMethod.POST)
    public ResponseEntity<?> addAsset(@RequestBody AssetDTO assetDTO) {
		
		
		Asset asset = assetService.saveAsset(assetDTO);
		
		/*
         * AXIUS BLOCKCHAIN
         * save a new user in the blockchain as a participant in chaincode         *   
         */
        
        HttpStatus status = blockchainAssetService.add(asset);
        // the result of adding participant to blockchain will be handled later
        // if status === 200 --> OK
		
		 return new ResponseEntity<>("Asset added to System", HttpStatus.OK);
		
	}

	
	@ApiOperation(value = "View Asset By Name", httpMethod = "POST", response = AssetDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid Asset"),
            @ApiResponse(code = 500, message = "Error encountered while adding Asset"),
            @ApiResponse(code = 200, message = "Asset fetched")})
    @RequestMapping(value = "/searchAsset", method = RequestMethod.POST)
    public ResponseEntity<?> findByName(@RequestBody String name) {
		Collection<Asset> asset = assetService.getByName(name);
		return new ResponseEntity<>(asset, HttpStatus.OK);
	}

	
    @ApiOperation(value = "Get all Asset Type in ths system", httpMethod = "GET", response = AssetDTO.class, responseContainer = "List")
    @RequestMapping(value = "/getAllAssets", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Asset>> getAllModules() {
        List<Asset> assetTypes = assetService.findAll();
        if (assetTypes == null || assetTypes.isEmpty()) {
            String errorMessage = String.format("Cannot find any Asset Types configured in System ");
            throw new ResourceNotFoundException(errorMessage);
        }

        return new ResponseEntity<>(assetTypes, HttpStatus.OK);
    }
    
	/**
	 * 
	 * @return
	 */
	@ApiOperation(value = "Find asset by producer id", httpMethod = "GET", response = AssetDTO.class)
	@ApiResponses(value = {
	            @ApiResponse(code = 400, message = "Invalid Producer Id"),
	            @ApiResponse(code = 500, message = "Error encountered while fetching Asset"),
	            @ApiResponse(code = 200, message = "Asset fetched by producerId")})
    @RequestMapping(value = "/assetsByProducerId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findByProducerId(String producerId) {
		Collection<Asset> asset = assetService.getByProducerId(producerId);
		//logger.info(" bountyProgram DTO :{}",bountyProgram);
		return new ResponseEntity<>(asset, HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * @return
	 * @throws ParseException 
	 */
	@ApiOperation(value = "Find asset by Asset id", httpMethod = "GET", response = AssetDTO.class)
	@ApiResponses(value = {
	            @ApiResponse(code = 400, message = "Invalid Asset Id"),
	            @ApiResponse(code = 500, message = "Error encountered while fetching Asset"),
	            @ApiResponse(code = 200, message = "Asset fetched by AssetId")})
    @RequestMapping(value = "/assetsByAssetId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findByAssetId(String assetId) throws ParseException {
		Asset asset = assetService.getById(assetId);
		return new ResponseEntity<>(asset, HttpStatus.OK);
	}
	
	
	@ApiOperation(value = "Find features by Asset id", httpMethod = "GET", response = AssetDTO.class)
	@ApiResponses(value = {
	            @ApiResponse(code = 400, message = "Invalid features "),
	            @ApiResponse(code = 500, message = "Error encountered while fetching Asset"),
	            @ApiResponse(code = 200, message = "Asset fetched by AssetId")})
    @RequestMapping(value = "/features", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findByAssetId1(String assetId) throws ParseException {
		FeaturesDetails featuresDetails = assetService.getByAssetId(assetId);
		
	//	System.out.println(featuresDetails.getFeature_tester().get("list"));
		
		
		ArrayList<JSONObject> al=new ArrayList<JSONObject>();
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		
		String jsonasset = gson.toJson(featuresDetails.getFeature_tester());
		JSONParser parser = new JSONParser();
		
		Object ob = parser.parse(jsonasset);
		JSONObject jsonObjectAsset=(JSONObject) ob;
		JSONArray jsonarrAsset=(JSONArray)jsonObjectAsset.get("list");	
		//System.out.println(jsonarrAsset);
		
		  for(int i=0;i<jsonarrAsset.size();i++) 
		  { 
			  JSONObject jsonobject =(JSONObject)jsonarrAsset.get(i);
			  
			  String filtering=jsonobject.get("Tester").toString().replace("{n", "{").replace("n}", "}").replace("}\"", ",").replace("{", "");
		  
			 String finaltestdata= "["+filtering.substring(filtering.indexOf("]")+1).replace("}","")+"]";
			 if(!(finaltestdata.contains("[]")))
			 {
			 jsonobject.put("Tester", finaltestdata);
			 
			al.add(jsonobject);
			 }
			 

			 
			// System.out.println(jsonarrAsset.get(i));
			  
		  }

		  featuresDetails.getFeature_tester().replace("list",al);
		  
		  //finallist.replaceAll("\\n", "").replaceAll("  ", "")
		 
		return new ResponseEntity<>(featuresDetails, HttpStatus.OK);
	}
	


}
