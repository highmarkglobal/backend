package tech.highmarkglobal.bcm.web.rest.controller;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;


import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
//import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.pdf.PdfWriter;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import tech.highmarkglobal.bcm.domain.Purchase;
import tech.highmarkglobal.bcm.exception.ResourceNotFoundException;
import tech.highmarkglobal.bcm.repo.PurchaseRepository;
import tech.highmarkglobal.bcm.service.BlockchainContractAssetService;
//import tech.highmarkglobal.bcm.exception.ResourceNotFoundException;
import tech.highmarkglobal.bcm.service.PurchaseService;

import tech.highmarkglobal.bcm.web.dto.ProductDomainDTO;
import tech.highmarkglobal.bcm.web.dto.PurchaseDTO;




@RestController
@RequestMapping("/api/buyAsset")
@Api(value = "/purchase-controller", description = "Create and manage various product doamins in the system")
public class PurchaseController {
	
	@Autowired 
	PurchaseService purchaseService;
	
	@Autowired
	BlockchainContractAssetService blockchainContractAsset;
	
	@Autowired 
	PurchaseRepository purchaserepo;
	
//	private static final String FILE_NAME = "C://Users//Archana TR//Documents//BuyAssets.json";

	@ApiOperation(value = "Purchase Details of each assets", httpMethod = "POST", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid Purchase"),
            @ApiResponse(code = 500, message = "Error encountered when buying an Asset Type"),
            @ApiResponse(code = 200, message = "Purchase details added")})
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<?> addPurchase(@RequestBody PurchaseDTO purchaseDTO) throws IOException, DocumentException {
		
	Purchase purchase = purchaseService.savePurchase(purchaseDTO); 
	
	
	/*
     * AXIUS BLOCKCHAIN
     * save purchase transaction in the blockchain   
     */
	
	System.out.println("***** Buy Asset *****");
	//Purchase purchase = purchaserepo.findByProducerConsumerAssetDate(purchaseDTO.getProducerId(), purchaseDTO.getConsumerId(), purchaseDTO.getAssetId(), purchaseDTO.getPurchaseDate());
    HttpStatus status = blockchainContractAsset.makeContract(purchase);	
	return new ResponseEntity<>("Purchase details of an AssetType are added to System", HttpStatus.OK);
		
	}
	
	
    @ApiOperation(value = "Get all Purchase details from the system", httpMethod = "GET", response = ProductDomainDTO.class, responseContainer = "List")
    @RequestMapping(value = "/ViewAllPurchaseDetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Purchase>> getAllModules() {
        List<Purchase> purchase = purchaseService.findAll();
        if (purchase == null || purchase.isEmpty()) {
            String errorMessage = String.format("Cannot find any Purchase details configured in the System ");
            throw new ResourceNotFoundException(errorMessage);
        }
        return new ResponseEntity<>(purchase, HttpStatus.OK);
    }
   
	
    @ApiOperation(value = "Get all Purchase details from the system according to ConsumerId", httpMethod = "GET", response = PurchaseDTO.class, responseContainer = "List")
    @RequestMapping(value = "/viewByConsumerId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<Purchase>> getAllModulesCon(String consumerId) {
    	Collection<Purchase> purchase = purchaseService.findByConsumerId(consumerId);
        return new ResponseEntity<>(purchase, HttpStatus.OK);
    }
   
    

}
