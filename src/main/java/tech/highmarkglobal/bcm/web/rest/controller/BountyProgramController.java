package tech.highmarkglobal.bcm.web.rest.controller;

import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import tech.highmarkglobal.bcm.domain.BountyProgram;
import tech.highmarkglobal.bcm.domain.BountySearch;
import tech.highmarkglobal.bcm.domain.Product;
import tech.highmarkglobal.bcm.domain.ProductSearch;
import tech.highmarkglobal.bcm.service.BlockchainBountyService;
import tech.highmarkglobal.bcm.service.BountyProgramService;
import tech.highmarkglobal.bcm.web.dto.BountyProgramDTO;
import tech.highmarkglobal.bcm.web.dto.ProductDTO;
import tech.highmarkglobal.bcm.web.payload.AppApiResponse;
import tech.highmarkglobal.bcm.web.transformer.DTOToDomainTransformer;

@RestController
@RequestMapping("/api/bountyprogram")
@Api(value = "/BountyProgramController", description = "Create and manage various Bounty programs in the system")
public class BountyProgramController {
	
	private static final Logger logger = LoggerFactory.getLogger(BountyProgramController.class);
	
	@Autowired(required=false)
	BountyProgramService bountyProgramService;
	
	@Autowired
	BlockchainBountyService blockchainBountyService;
	/**
	 * 
	 * @param bountyProgramDTO
	 * @return
	 */
	@ApiOperation(value = "Create Bounty ", httpMethod = "POST", response = BountyProgramDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid Bounty "),
            @ApiResponse(code = 500, message = "Error encountered when adding BountyProgram"),
            @ApiResponse(code = 200, message = "BountyProgram added")})
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<?> createBounty(@RequestBody BountyProgramDTO bountyProgramDTO) {
		BountyProgram bountyProgram = bountyProgramService.saveBountyProgram(bountyProgramDTO);
		if (bountyProgram == null) {
			return new ResponseEntity<>("Insufficient Balance", HttpStatus.BAD_REQUEST);
		}
		logger.info(" bountyProgram :{}",bountyProgram);
		
		/*
         * AXIUS BLOCKCHAIN
         * save a new user in the blockchain as a participant in chaincode         *   
         */
       
        HttpStatus status = blockchainBountyService.add(bountyProgram);
        // the result of adding participant to blockchain will be handled later
        // if status === 200 --> OK
		return new ResponseEntity<>(bountyProgram, HttpStatus.OK);
		
	}
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	@ApiOperation(value = "View Bounty ", httpMethod = "GET", response = BountyProgramDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid BountyProgram"),
            @ApiResponse(code = 500, message = "Error encountered while adding BountyProgram"),
            @ApiResponse(code = 200, message = "BountyProgram added")})
    @RequestMapping(value = "/viewBounty/{name}", method = RequestMethod.GET)
    public ResponseEntity<?> viewBounty(@ApiParam(name = "name", value = "name", required = true) @PathVariable("name") String name) {
		BountyProgramDTO bountyProgram = bountyProgramService.findBounty(name);
		logger.info(" bountyProgram DTO :{}",bountyProgram);
		return new ResponseEntity<>(bountyProgram, HttpStatus.OK);
		
	}
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	@ApiOperation(value = "View All Bounty ", httpMethod = "GET", response = BountyProgramDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid Bounty "),
            @ApiResponse(code = 500, message = "Error encountered while fetching Bounty"),
            @ApiResponse(code = 200, message = "Bounties Fetched")})
    @RequestMapping(value = "/viewall", method = RequestMethod.GET)
    public ResponseEntity<?> viewAllBountys() {
		Collection<BountyProgramDTO> bountyPrograms = bountyProgramService.findBountys();
		logger.info(" bountyPrograms DTO :{}",bountyPrograms);
		return new ResponseEntity<>(bountyPrograms, HttpStatus.OK);
		
	}
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	@ApiOperation(value = "View Bounty By ProductId", httpMethod = "GET", response = BountyProgramDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid Product ID "),
            @ApiResponse(code = 500, message = "Error enncountered while fetching Bounty"),
            @ApiResponse(code = 200, message = "Bounty fetched")})
    @RequestMapping(value = "/viewboutiesbyproductid", method = RequestMethod.GET)
    public ResponseEntity<?> findBountiesByProductId(String productId) {
		Collection<BountyProgramDTO> bountyPrograms = bountyProgramService.findBountiesByProductId(productId);
		if (bountyPrograms == null || bountyPrograms.size() == 0) {
			return new ResponseEntity<AppApiResponse>(new AppApiResponse(false, "Invalid Product Id"),HttpStatus.BAD_REQUEST);
		}
		logger.info(" bountyPrograms DTO :{}",bountyPrograms);
		return new ResponseEntity<>(bountyPrograms, HttpStatus.OK);
		
	}
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	@ApiOperation(value = "View Bounty By ProducerId", httpMethod = "GET", response = BountyProgramDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid Producer ID "),
            @ApiResponse(code = 500, message = "Error enncountered while fetching Bounty"),
            @ApiResponse(code = 200, message = "Bounty fetched")})
    @RequestMapping(value = "/viewboutiesbyproducerid", method = RequestMethod.GET)
    public ResponseEntity<?> findBountiesByProducerId(String producerId) {
		Collection<BountyProgramDTO> bountyPrograms = bountyProgramService.findBountiesByProducerId(producerId);
		if (bountyPrograms == null || bountyPrograms.size() == 0) {
			return new ResponseEntity<AppApiResponse>(new AppApiResponse(false, "Invalid Producer Id"),HttpStatus.BAD_REQUEST);
		}
		logger.info(" bountyPrograms DTO :{}",bountyPrograms);
		return new ResponseEntity<>(bountyPrograms, HttpStatus.OK);
		
	}
	
	@ApiOperation(value = "Search BountyProgram", httpMethod = "POST", response = BountyProgramDTO.class, responseContainer =  "List")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid search criteria "),
            @ApiResponse(code = 500, message = "Error encountered while searching for BountyProgram "),
            @ApiResponse(code = 200, message = "Bounty Fetched")})
    @RequestMapping(value = "/searchBountyProgram", method = RequestMethod.POST)
    public ResponseEntity<?> searchBountyProgram(@RequestBody BountySearch search) {
		
		Collection<BountyProgram> bounties = bountyProgramService.searchBounties(search);
/*		System.out.println("Bounties size:"+bounties.size());
		Collection<BountyProgramDTO> bountyDTOs = new ArrayList<BountyProgramDTO>();
		for(BountyProgram bp : bounties) {
    		BountyProgramDTO bpDto = DTOToDomainTransformer.transform(bp);
    		bountyDTOs.add(bpDto);
    	}
		*/
		return new ResponseEntity<>(bounties, HttpStatus.OK);
	}
	
	/**
	 * 
	 * @return
	 */
	@ApiOperation(value = "Find bounty by id", httpMethod = "GET", response = BountyProgramDTO.class)
	@ApiResponses(value = {
	            @ApiResponse(code = 400, message = "Invalid Bounty Id"),
	            @ApiResponse(code = 500, message = "Error encountered while fetching Bounty"),
	            @ApiResponse(code = 200, message = "Bounty fetched by BountyId")})
    @RequestMapping(value = "/bountyById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findByBountyId(String id) {
		BountyProgram bountyProgram = bountyProgramService.getById(id);
		//logger.info(" bountyProgram DTO :{}",bountyProgram);
		return new ResponseEntity<>(bountyProgram, HttpStatus.OK);
	}
	


}
