package tech.highmarkglobal.bcm.web.rest.controller;

import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import tech.highmarkglobal.bcm.domain.User;
import tech.highmarkglobal.bcm.domain.Wallet;
import tech.highmarkglobal.bcm.service.PurchaseService;
import tech.highmarkglobal.bcm.service.ValidatorService;


@RestController
@RequestMapping("/api/wallet")

public class WalletController {

	
	private static final Logger logger = LoggerFactory.getLogger(TransactionController.class);
	
	@Autowired
	ValidatorService validatorService;
	
	
	@Autowired
	PurchaseService purchaseService;
	/**
	 * 
	 * @return
	 */
	@ApiOperation(value = "Find your wallet details for testing bounty", httpMethod = "GET")
	@ApiResponses(value = {
	            @ApiResponse(code = 400, message = "Invalid"),
	            @ApiResponse(code = 500, message = "Error encountered"),
	            @ApiResponse(code = 200, message = "Wallet details Fetech")})
    @RequestMapping(value = "/testingBounty", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findById1(String Id) throws java.text.ParseException{
		//Collection<Asset> asset = assetService.getByProducerId(producerId);
		//logger.info(" bountyProgram DTO :{}",bountyProgram);
		//User user=userRepository.findById(id);
		List<Wallet> wallet =validatorService.getById(Id);
		return new ResponseEntity<>(wallet, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Find your wallet details for Buying Asset", httpMethod = "GET")
	@ApiResponses(value = {
	            @ApiResponse(code = 400, message = "Invalid"),
	            @ApiResponse(code = 500, message = "Error encountered"),
	            @ApiResponse(code = 200, message = "Wallet details Fetech")})
    @RequestMapping(value = "/buyingasset", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findById(String Id) {
		//Collection<Asset> asset = assetService.getByProducerId(producerId);
		//logger.info(" bountyProgram DTO :{}",bountyProgram);
		
		List<Wallet> wallet =purchaseService.getById(Id);
		return new ResponseEntity<>(wallet, HttpStatus.OK);
		
	}
	
}
