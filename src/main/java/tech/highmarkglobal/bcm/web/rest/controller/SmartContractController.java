package tech.highmarkglobal.bcm.web.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import tech.highmarkglobal.bcm.service.SmartContractService;
import tech.highmarkglobal.bcm.web.dto.BountyProgramDTO;
import tech.highmarkglobal.bcm.web.dto.SmartContractDTO;

@RestController
@RequestMapping("/api/smartcontract")
public class SmartContractController {
	
	@Autowired
	private SmartContractService smartContractService;
	
	//@PostMapping("/create")
	
	@ApiOperation(value = "Create SmartContact", httpMethod = "POST", response = BountyProgramDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid SmartContract"),
            @ApiResponse(code = 500, message = "Error encountered while adding SmartContact"),
            @ApiResponse(code = 200, message = "SmartContract Added")})
	SmartContractDTO saveContract(@RequestBody SmartContractDTO smartContractDTO)
	{
		SmartContractDTO contractDTO = smartContractService.saveSmartContract(smartContractDTO);
		return contractDTO;
		
	}

}
