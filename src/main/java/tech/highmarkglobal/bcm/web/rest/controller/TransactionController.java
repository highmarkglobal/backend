package tech.highmarkglobal.bcm.web.rest.controller;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import tech.highmarkglobal.bcm.domain.Transaction;
import tech.highmarkglobal.bcm.service.BountyProgramService;
import tech.highmarkglobal.bcm.service.TransactionService;
import tech.highmarkglobal.bcm.web.dto.BountyProgramDTO;

@RestController
@RequestMapping("/api/transaction")

public class TransactionController {

	
	private static final Logger logger = LoggerFactory.getLogger(TransactionController.class);
	
	@Autowired
	TransactionService txnService;
	
	@ApiOperation(value = "Create Transaction", httpMethod = "POST", response = Transaction.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid data"),
            @ApiResponse(code = 500, message = "Error encountered when creating transaction"),
            @ApiResponse(code = 200, message = "BountyProgram added")})
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<?> createTransaction(@RequestBody Transaction txn) {
		Transaction trans = txnService.saveTransaction(txn);
		//logger.info(" bountyProgram DTO :{}",bountyProgram);
		return new ResponseEntity<>(trans, HttpStatus.OK);
		
	}

	
	@ApiOperation(value = "GetByUserId", httpMethod = "GET", response = Transaction.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid data"),
            @ApiResponse(code = 500, message = "Error encountered when creating transaction"),
            @ApiResponse(code = 200, message = "BountyProgram added")})
    @RequestMapping(value = "/findByUserId", method = RequestMethod.GET)
    public ResponseEntity<?> getByUserId(String userId) {
		System.out.println("User id:"+userId);
		Collection<Transaction> trans = txnService.findTransactionByUserId(userId);
		if (trans == null) {
			return new ResponseEntity<>("No transactions", HttpStatus.BAD_REQUEST);
		}
		//logger.info(" bountyProgram DTO :{}",bountyProgram);
		return new ResponseEntity<>(trans, HttpStatus.OK);
		
	}
	
	
	
	

}
