package tech.highmarkglobal.bcm.web.rest.controller;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tech.highmarkglobal.bcm.domain.User;
import tech.highmarkglobal.bcm.security.CurrentUser;
import tech.highmarkglobal.bcm.security.UserPrincipal;
import tech.highmarkglobal.bcm.service.UserService;
import tech.highmarkglobal.bcm.web.dto.UserDTO;
import tech.highmarkglobal.bcm.web.transformer.DTOToDomainTransformer;

@RestController
@RequestMapping("/api")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    
    @Autowired
    private UserService userService;

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER')")
    public UserPrincipal getCurrentUser(@CurrentUser UserPrincipal currentUser) {
    	logger.info("\nUserId:{} \nUserName:{} \nName:{}", currentUser.getId(), currentUser.getUsername(), currentUser.getName());
        return currentUser;
    }

    @GetMapping("/user/getbyuserid")
    //@PreAuthorize("hasRole('USER')")
    public UserDTO getUserById( String userId) {
    	//logger.info("\nUserId:{} \nUserName:{} \nName:{}", currentUser.getId(), currentUser.getUsername(), currentUser.getName());
    	User user = userService.getUserById(userId);
    	UserDTO userDTO = DTOToDomainTransformer.transform(user);
        return userDTO;
    }

}
