package tech.highmarkglobal.bcm.web.payload;

/**
 * @author arun
 */
public class JwtAuthenticationResponse {
    private String accessToken;
    private String tokenType = "Bearer";
    private String email;
    private String userType;
    private String userId;
    private String name;
    //private String corporationName;

    public JwtAuthenticationResponse(String accessToken,String userId, String name,String email, String userType) {
        this.accessToken = accessToken;
        this.userId = userId;
        this.name=name;
        this.email=email;
        this.userType=userType;
    //    this.corporationName=corporationName;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}






	
}
