package tech.highmarkglobal.bcm.constants;

/**
 * This class will hold all messages sent out from this module.
 * 
 * @author Ramesh
 *
 */
public final class MessageConstants {

	public static final String MEMBER_ADDED_SUCCESSFULLY = "Member successfully added.";
	
	public static final String PRODUCT_ADDED_SUCCESSFULLY = "Product successfully added.";
	
	public static final String UNAUTHORISED = "Sorry, You're not authorized to access this resource.";

	public static class ErrorMessages {
		public static final String MEMBER_ALREDY_EXISTS = "The member already exists.";
	}
	
	public static class Product_ErrorMessages {
		public static final String PRODUCT_ALREDY_EXISTS = "The product already exists.";
	}

}