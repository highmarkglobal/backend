package tech.highmarkglobal.bcm.mail;

import tech.highmarkglobal.bcm.web.dto.UserDTO;
import tech.highmarkglobal.bcm.web.dto.UserDetailsDTO;

public interface UserMailService {

	void onboardingUser(UserDetailsDTO signUpDTO);

}
