package tech.highmarkglobal.bcm.mail.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import tech.highmarkglobal.bcm.mail.UserMailService;
import tech.highmarkglobal.bcm.web.dto.UserDTO;
import tech.highmarkglobal.bcm.web.dto.UserDetailsDTO;

import static tech.highmarkglobal.bcm.util.ExecutorUtil.EXECUTOR_SERVICE;

@Service
public class UserMailServiceImpl implements UserMailService{
	
	@Autowired
    @Qualifier("simpleMailSender")
	public MailSender mailSender;

	@Override
	public void onboardingUser(UserDetailsDTO signUpDTO) {
		EXECUTOR_SERVICE.execute(()->{
			
			String from = "highmarkglobal@gmail.com";
			String to = signUpDTO.getContactEmail(); 
			String subject = "Highmark Credentials"; 
			StringBuffer body = new StringBuffer();
			body
				.append("\nemail: ").append(signUpDTO.getContactEmail())
				.append("\npassword: ").append(signUpDTO.getPassword());
			
			mailSender.sendMail(from, to, subject, body.toString());
			
		});
	}

}
