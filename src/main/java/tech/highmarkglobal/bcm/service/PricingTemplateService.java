package tech.highmarkglobal.bcm.service;
import tech.highmarkglobal.bcm.domain.PricingTemplate;
import tech.highmarkglobal.bcm.web.dto.PricingTemplateDTO;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public interface PricingTemplateService {
	
	public void savePricingTemplates(List<PricingTemplateDTO> pricingTemplateDtos);
	public List<PricingTemplate> getByPriceRange(Double start,Double end);
}
