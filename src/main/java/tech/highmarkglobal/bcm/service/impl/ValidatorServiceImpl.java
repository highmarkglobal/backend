package tech.highmarkglobal.bcm.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.mongodb.util.JSON;

import java.lang.StringBuilder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import tech.highmarkglobal.bcm.domain.Asset;
import tech.highmarkglobal.bcm.domain.BountyProgram;
import tech.highmarkglobal.bcm.domain.FileInfo;
import tech.highmarkglobal.bcm.domain.JoinBounty;
import tech.highmarkglobal.bcm.domain.TestResult;
import tech.highmarkglobal.bcm.domain.User;
import tech.highmarkglobal.bcm.domain.Validator;
import tech.highmarkglobal.bcm.domain.Wallet;
import tech.highmarkglobal.bcm.repo.AssetRepository;
import tech.highmarkglobal.bcm.repo.BountyProgramRepository;
import tech.highmarkglobal.bcm.repo.JoinBountyRepository;
import tech.highmarkglobal.bcm.repo.TestResultRepository;
import tech.highmarkglobal.bcm.repo.UserRepository;
import tech.highmarkglobal.bcm.repo.ValidatorRepository;
import tech.highmarkglobal.bcm.service.AssetService;
import tech.highmarkglobal.bcm.service.FileInfoService;
import tech.highmarkglobal.bcm.service.ValidatorService;
import tech.highmarkglobal.bcm.web.dto.TestResultDTO;
import tech.highmarkglobal.bcm.web.dto.ValidatorDTO;
@Service
public class ValidatorServiceImpl implements ValidatorService{
	
	@Autowired
	ValidatorRepository validatorRepository;
	

	@Autowired
	BountyProgramRepository bountyProgramRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	TestResultRepository testResultRepository;
	
	@Autowired
	AssetRepository assetRepository;
	
	@Autowired
	JoinBountyRepository joinBountyRepository;
	
	@Autowired
	private FileInfoService fileInfoService;
	
	public static final String COLLECTION_NAME = "Validator";

	
	@Override
	public Validator savevalidatorResults(ValidatorDTO validatorDTO, byte[] validationreport,byte[] valbugsreport,byte[] valtestscript,byte[] valscriptlog) throws ParseException, java.text.ParseException {
		//TestResult testResult= testResultRepository.findByBountyName(validatorDTO.getBountyName());
		
		BountyProgram  bountyProgram =bountyProgramRepository.findByBountyName(validatorDTO.getBountyName());
		//Collection<JoinBounty> joinBounty=joinBountyRepository.findByTesterID(testResult.getTesterId())
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ");
		String nowAsISO = df.format(new Date());
		Date validationDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ").parse(nowAsISO);
		
		TestResult testResult123= testResultRepository.findByBountyName(validatorDTO.getBountyName());
		Validator validator= new Validator();
		
		validator.setBountyName(validatorDTO.getBountyName());
		validator.setAssetName(validatorDTO.getAssetName());
		validator.setRewards(validatorDTO.getRewards());
/*		validator.setClaimFeatureType(validatorDTO.getClaimFeatureType());
		validator.setClaimFeatureName(validatorDTO.getClaimFeatureName());*/
		validator.setFeatures(validatorDTO.getFeatures());
		validator.setPlatform(validatorDTO.getPlatform());
		validator.setToolsUsed(validatorDTO.getToolsUsed());
		validator.setRecommendation(validatorDTO.getRecommendation());

		validator.setSystemRecommendation(validatorDTO.getSystemRecommendation());
		validator.setSystemConfidence(validatorDTO.getSystemConfidence());
		validator.setValidationRecommendation(validatorDTO.getValidationRecommendation());
		validator.setValidationMethod(validatorDTO.getValidationMethod());

		validator.setValidatorDate(validationDate);
		validator.setTesterID(validatorDTO.getTesterID());
		validator.setProducerId(bountyProgram.getProducerId());
		
		validator.setTestReport(testResult123.getTestReport());		
		validator.setTestScript(testResult123.getTestData());		
		validator.setScriptLog(testResult123.getScriptLog());
				
		
		 FileInfo validationReport=null;
		 validationReport=	fileInfoService.save(validationreport,"ValidationReport");
		 validator.setValidationReport(validationReport);
		
		FileInfo valBugsReport=null;
		valBugsReport=	fileInfoService.save(valbugsreport,"ValidatorTestData");
		validator.setValidatorBugsReport(valBugsReport);
		
		FileInfo valtTestScript=null;
		valtTestScript=	fileInfoService.save(valtestscript,"ValidatorTestData");
		validator.setValidatorTestScripts(valtTestScript);
		
		FileInfo valScriptLog=null;
		valScriptLog=	fileInfoService.save(valscriptlog,"ValidatorScriptLog");
		validator.setValidatorScriptLog(valScriptLog);
		validator = validatorRepository.save(validator);

		//System.out.println("uplodaing done");
		
		Asset asset = assetRepository.findByName(validator.getAssetName());
		
		User user =userRepository.findById(validator.getTesterID());
		
		TestResult testResult= testResultRepository.findByBountyName(validator.getBountyName());
		
		//added now
		//BountyProgram bountyprogram12=bountyProgramRepository.findByAssetId(asset.getId());
		//TestResult testresults=testResultRepository.findByAssetName(asset.getName());
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		StringBuilder te=new StringBuilder();
		//asset features String
		String jsonasset = gson.toJson(asset.getFeatures());
		//System.out.println(jsonasset);
		
		//validator features String
		String jsonvalidator = gson.toJson(validator.getFeatures());
		//System.out.println(jsonvalidator);
		
		//Bounty features String
		String jsonbounty = gson.toJson(bountyProgram.getFeatures());
		//System.out.println(jsonbounty);
		//Tester features String
		String jsontester = gson.toJson(testResult.getFeatures());
		//System.out.println(jsontester);
		//common
		JSONParser parser = new JSONParser();
		
		//object for asset features ==jsonasset
		Object ob = parser.parse(jsonasset);
		JSONObject jsonObjectAsset=(JSONObject) ob;
		JSONArray jsonarrAsset=(JSONArray)jsonObjectAsset.get("list");	
		//System.out.println(jsonarrAsset.size());
		
		//object for Validator features ==jsonvalidator
		Object ob1 = parser.parse(jsonvalidator);
		JSONObject jsonObjectvalidator=(JSONObject) ob1;
		JSONArray jsonarrValidator=(JSONArray)jsonObjectvalidator.get("list");	
		//System.out.println(jsonarrValidator.size());
		
		//object for bounty features ==jsonbounty
		Object ob2 = parser.parse(jsonbounty);
		JSONObject jsonObjectbounty=(JSONObject) ob2;
		JSONArray jsonarrbounty=(JSONArray)jsonObjectbounty.get("list");	
		//System.out.println(jsonarrbounty.size());
		
		
		//object for tester features ==jsontester
		Object ob3 = parser.parse(jsontester);
		JSONObject jsonObjecttester=(JSONObject) ob3;
		JSONArray jsonarrtester=(JSONArray)jsonObjecttester.get("list");	
		//System.out.println(jsonarrtester.size());
		JSONArray ja = new JSONArray();
		//String featuretester="Tester:";
		
		//JSONArray jsonarrtester=(JSONArray)jsonObjecttester.get("list");
		
		JSONObject Testerjsonarr=new JSONObject();
		for(int j=0;j<jsonarrValidator.size();j++)
		{
		
		for(int i=0;i<jsonarrAsset.size();i++)
		{
			
			JSONObject jsonObValidator = (JSONObject)jsonarrValidator.get(j);
			JSONObject jsonObAssetfe = (JSONObject)jsonarrAsset.get(i);
			
			JSONObject jsonObAsset = (JSONObject)jsonarrAsset.get(i);

			if(jsonObValidator.get("featureId").equals(jsonObAssetfe.get("featureId")))
					
			{
			
			String Tester= user.getName();
			

			System.out.println(jsonObAssetfe.get("Tester"));
			//System.out.println("Present data+++++"+ jsonObAssetfe.get("Tester").getClass());
			
			StringBuilder sb = new StringBuilder();
			
			String  vali = gson.toJson(jsonObAssetfe.get("Tester"));
			
			sb.append(vali);

			if(validator.getValidationRecommendation().equals("pass"))
			{
				System.out.println(jsonObAssetfe.get("Tester").getClass());
				
				Testerjsonarr.putIfAbsent(Tester, "pass");
				
			}
			else if(validator.getValidationRecommendation().equals("fail"))
			{

			Testerjsonarr.putIfAbsent(Tester, "fail");

			}
			String Last = gson.toJson(Testerjsonarr);
			sb.append(Last);
			jsonObAsset.put("Tester",sb.toString().replaceAll("\\\\", ""));
			}
		}
		}

		for(int j=0;j<jsonarrValidator.size();j++)
		{
			for(int l=0;l<jsonarrtester.size();l++)
			{
				
				JSONObject jsonObValidator = (JSONObject)jsonarrValidator.get(j);
				
				JSONObject jsonObtester = (JSONObject)jsonarrtester.get(l);
				
				//JSONObject jsonObAsset = (JSONObject)jsonarrAsset.get(i);
				
				if(jsonObValidator.get("featureId").equals(jsonObtester.get("featureId")))
						
				{
					

				String Tester= user.getName();
				StringBuilder sb = new StringBuilder();
				String  vali = gson.toJson(jsonObtester.get("Tester"));
			
				sb.append(vali);

				if(validator.getValidationRecommendation().equals("pass"))
				{

					Testerjsonarr.putIfAbsent(Tester, "pass");
				}
				if(validator.getValidationRecommendation().equals("fail"))
				{

				Testerjsonarr.putIfAbsent(Tester, "fail");
				}
				String Last = gson.toJson(Testerjsonarr);
				sb.append(Last);
				jsonObtester.put("Tester",sb.toString().replaceAll("\\\\", ""));
				}
			}
	 
		}
		
		for(int j=0;j<jsonarrValidator.size();j++)
		{
			for(int k=0;k<jsonarrbounty.size();k++)
			{
				
				JSONObject jsonObValidator = (JSONObject)jsonarrValidator.get(j);
				
				JSONObject jsonObbounty = (JSONObject)jsonarrbounty.get(k);
				
				//JSONObject jsonObAsset = (JSONObject)jsonarrAsset.get(i);
				
				if(jsonObValidator.get("featureId").equals(jsonObbounty.get("featureId")))
						
				{
					

				String Tester= user.getName();
				StringBuilder sb = new StringBuilder();
				String  vali = gson.toJson(jsonObbounty.get("Tester"));
			
				sb.append(vali);

				if(validator.getValidationRecommendation().equals("pass"))
				{

					Testerjsonarr.putIfAbsent(Tester, "pass");
				}
				if(validator.getValidationRecommendation().equals("fail"))
				{

				Testerjsonarr.putIfAbsent(Tester, "fail");
				}
				String Last = gson.toJson(Testerjsonarr);
				sb.append(Last);
				
				

				
				//String re=sb.toString().replaceAll("\n","");
				
			//	System.out.println(re.replaceAll("\\\\", ""));
				//System.out.println()
				
				//.replaceAll("\\\\", "")
				}
			}
	 
		}
		
		//System.out.println("list printing"+jsonarrAsset);

		
		asset.getFeatures().replace("list", jsonarrAsset);
		
		//System.out.println("Last check of object"+asset.getFeatures().replace("list", jsonarrAsset).getClass());
		asset.getFeatures().replace("list", jsonarrAsset);
		bountyProgram.getFeatures().replace("list", jsonarrbounty);
		testResult.getFeatures().replace("list", jsonarrtester);
		
		
		//System.out.println("asset object" +asset);
		
		//System.out.println("asset object" +bountyProgram);
		
		//System.out.println("asset object" +testResult);
		
		assetRepository.save(asset);
		bountyProgramRepository.save(bountyProgram);
		testResultRepository.save(testResult);

				
		return validator;				
	}
	
	//For validation method other than retest
		public Validator savevalidatorResults(ValidatorDTO validatorDTO) throws java.text.ParseException, ParseException {
			
			BountyProgram  bountyProgram =bountyProgramRepository.findByBountyName(validatorDTO.getBountyName());
			//Collection<JoinBounty> joinBounty=joinBountyRepository.findByTesterID(testResult.getTesterId())
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ");
			String nowAsISO = df.format(new Date());
			Date validationDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ").parse(nowAsISO);
			
			TestResult testResult123= testResultRepository.findByBountyName(validatorDTO.getBountyName());
			Validator validator= new Validator();
			
			validator.setBountyName(validatorDTO.getBountyName());
			validator.setAssetName(validatorDTO.getAssetName());
			validator.setRewards(validatorDTO.getRewards());
			validator.setFeatures(validatorDTO.getFeatures());
			validator.setPlatform(validatorDTO.getPlatform());
			validator.setToolsUsed(validatorDTO.getToolsUsed());
			validator.setRecommendation(validatorDTO.getRecommendation());
			validator.setSystemRecommendation(validatorDTO.getSystemRecommendation());
			validator.setSystemConfidence(validatorDTO.getSystemConfidence());
			validator.setValidationRecommendation(validatorDTO.getValidationRecommendation());
			validator.setValidationMethod(validatorDTO.getValidationMethod());
			validator.setValidatorDate(validationDate);
			validator.setTesterID(validatorDTO.getTesterID());
			validator.setProducerId(bountyProgram.getProducerId());
			
			validator.setTestReport(testResult123.getTestReport());			
			validator.setTestScript(testResult123.getTestData());			
			validator.setScriptLog(testResult123.getScriptLog());		
			
			validatorRepository.save(validator);			
			//System.out.println("uplodaing done");
			
			
			Asset asset = assetRepository.findByName(validator.getAssetName());			
			User user =userRepository.findById(validator.getTesterID());			
			TestResult testResult= testResultRepository.findByBountyName(validator.getBountyName());
			
			//added now
			//BountyProgram bountyprogram12=bountyProgramRepository.findByAssetId(asset.getId());
			//TestResult testresults=testResultRepository.findByAssetName(asset.getName());
			
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			StringBuilder te=new StringBuilder();
			//asset features String
			String jsonasset = gson.toJson(asset.getFeatures());
			//System.out.println(jsonasset);
			
			//validator features String
			String jsonvalidator = gson.toJson(validator.getFeatures());
			//System.out.println(jsonvalidator);
			
			//Bounty features String
			String jsonbounty = gson.toJson(bountyProgram.getFeatures());
		//System.out.println(jsonbounty);
			//Tester features String
			String jsontester = gson.toJson(testResult.getFeatures());
		//	System.out.println(jsontester);
			//common
			JSONParser parser = new JSONParser();
			
			//object for asset features ==jsonasset
			Object ob = parser.parse(jsonasset);
			JSONObject jsonObjectAsset=(JSONObject) ob;
			JSONArray jsonarrAsset=(JSONArray)jsonObjectAsset.get("list");	
		//	System.out.println(jsonarrAsset.size());
			
			//object for Validator features ==jsonvalidator
			Object ob1 = parser.parse(jsonvalidator);
			JSONObject jsonObjectvalidator=(JSONObject) ob1;
			JSONArray jsonarrValidator=(JSONArray)jsonObjectvalidator.get("list");	
			//System.out.println(jsonarrValidator.size());
			
			//object for bounty features ==jsonbounty
			Object ob2 = parser.parse(jsonbounty);
			JSONObject jsonObjectbounty=(JSONObject) ob2;
			JSONArray jsonarrbounty=(JSONArray)jsonObjectbounty.get("list");	
		//	System.out.println(jsonarrbounty.size());
			
			
			//object for tester features ==jsontester
			Object ob3 = parser.parse(jsontester);
			JSONObject jsonObjecttester=(JSONObject) ob3;
			JSONArray jsonarrtester=(JSONArray)jsonObjecttester.get("list");	
			//System.out.println(jsonarrtester.size());
			JSONArray ja = new JSONArray();
			//String featuretester="Tester:";
			
			//JSONArray jsonarrtester=(JSONArray)jsonObjecttester.get("list");
			
			JSONObject Testerjsonarr=new JSONObject();
			for(int j=0;j<jsonarrValidator.size();j++)
			{
			
			for(int i=0;i<jsonarrAsset.size();i++)
			{
				
				JSONObject jsonObValidator = (JSONObject)jsonarrValidator.get(j);
				JSONObject jsonObAssetfe = (JSONObject)jsonarrAsset.get(i);
				
				JSONObject jsonObAsset = (JSONObject)jsonarrAsset.get(i);

				if(jsonObValidator.get("featureId").equals(jsonObAssetfe.get("featureId")))
						
				{
				
				String Tester= user.getName();
				

				System.out.println(jsonObAssetfe.get("Tester"));
			//	System.out.println("Present data+++++"+ jsonObAssetfe.get("Tester").getClass());
				
				StringBuilder sb = new StringBuilder();
				
				String  vali = gson.toJson(jsonObAssetfe.get("Tester"));
				
				sb.append(vali);

				if(validator.getValidationRecommendation().equals("pass"))
				{
					System.out.println(jsonObAssetfe.get("Tester").getClass());
					
					Testerjsonarr.putIfAbsent(Tester, "pass");
					
				}
				else if(validator.getValidationRecommendation().equals("fail"))
				{

				Testerjsonarr.putIfAbsent(Tester, "fail");

				}
				String Last = gson.toJson(Testerjsonarr);
				sb.append(Last);
				jsonObAsset.put("Tester",sb.toString().replaceAll("\\\\", ""));
				}
			}
			}

			for(int j=0;j<jsonarrValidator.size();j++)
			{
				for(int l=0;l<jsonarrtester.size();l++)
				{
					
					JSONObject jsonObValidator = (JSONObject)jsonarrValidator.get(j);
					
					JSONObject jsonObtester = (JSONObject)jsonarrtester.get(l);
					
					//JSONObject jsonObAsset = (JSONObject)jsonarrAsset.get(i);
					
					if(jsonObValidator.get("featureId").equals(jsonObtester.get("featureId")))
							
					{
						

					String Tester= user.getName();
					StringBuilder sb = new StringBuilder();
					String  vali = gson.toJson(jsonObtester.get("Tester"));
				
					sb.append(vali);

					if(validator.getValidationRecommendation().equals("pass"))
					{

						Testerjsonarr.putIfAbsent(Tester, "pass");
					}
					if(validator.getValidationRecommendation().equals("fail"))
					{

					Testerjsonarr.putIfAbsent(Tester, "fail");
					}
					String Last = gson.toJson(Testerjsonarr);
					sb.append(Last);
					jsonObtester.put("Tester",sb.toString().replaceAll("\\\\", ""));
					}
				}
		 
			}
			
			for(int j=0;j<jsonarrValidator.size();j++)
			{
				for(int k=0;k<jsonarrbounty.size();k++)
				{
					
					JSONObject jsonObValidator = (JSONObject)jsonarrValidator.get(j);
					
					JSONObject jsonObbounty = (JSONObject)jsonarrbounty.get(k);
					
					//JSONObject jsonObAsset = (JSONObject)jsonarrAsset.get(i);
					
					if(jsonObValidator.get("featureId").equals(jsonObbounty.get("featureId")))
							
					{
						

					String Tester= user.getName();
					StringBuilder sb = new StringBuilder();
					String  vali = gson.toJson(jsonObbounty.get("Tester"));
				
					sb.append(vali);

					if(validator.getValidationRecommendation().equals("pass"))
					{

						Testerjsonarr.putIfAbsent(Tester, "pass");
					}
					if(validator.getValidationRecommendation().equals("fail"))
					{

					Testerjsonarr.putIfAbsent(Tester, "fail");
					}
					String Last = gson.toJson(Testerjsonarr);
					sb.append(Last);
					


					jsonObbounty.put("Tester",sb.toString().replaceAll("\\\\", ""));

					}
				}
		 
			}
			
		//	System.out.println("list printing"+jsonarrAsset);

			
			asset.getFeatures().replace("list", jsonarrAsset);
			
			//System.out.println("Last check of object"+asset.getFeatures().replace("list", jsonarrAsset).getClass());
			asset.getFeatures().replace("list", jsonarrAsset);
			bountyProgram.getFeatures().replace("list", jsonarrbounty);
			testResult.getFeatures().replace("list", jsonarrtester);
			
			
			//System.out.println("asset object" +asset);
			
			//System.out.println("asset object" +bountyProgram);
			
			//System.out.println("asset object" +testResult);
			
			assetRepository.save(asset);
			bountyProgramRepository.save(bountyProgram);
			testResultRepository.save(testResult);
					
			return validator;	
		}
	

	public List<Wallet> getById(String id) throws java.text.ParseException
	{
		List<Wallet> walletList=new ArrayList<Wallet>();		
		

		User user=userRepository.findById(id);
		if(user.getUserType().contains("Tester"))
		{
		List<Validator> validatorList =validatorRepository.findByTesterID(id);
		
			for (Validator v :validatorList )
			{
				BountyProgram  bountyProgram =bountyProgramRepository.findByBountyName(v.getBountyName());
				Wallet wallet=new Wallet();
				wallet.setId(id);
				wallet.setTransactionIdentifer(v.getBountyName());
				wallet.setTransactionDate(v.getValidatorDate());
				wallet.setAmount(bountyProgram.getReward());
				wallet.setUserType(user.getUserType());
				wallet.setSymbol("Earn");
				wallet.setTransactionType("Test Bounty");
				wallet.setCounterParty(user.getName());
				walletList.add(wallet);
			}
		}
		
		else if(user.getUserType().contains("Producer"))
		{
			List<Validator> validatorList =validatorRepository.findByProducerId(id);
			
			for (Validator v :validatorList )
			{
			
				BountyProgram  bountyProgram =bountyProgramRepository.findByBountyName(v.getBountyName());
				Wallet wallet=new Wallet();
				wallet.setId(id);
				wallet.setTransactionIdentifer(v.getBountyName());
				wallet.setTransactionDate(v.getValidatorDate());
				wallet.setAmount(bountyProgram.getReward());
				wallet.setUserType(user.getUserType());
				wallet.setSymbol("Pay");
				wallet.setTransactionType("Test Bounty");
				//wallet.setCounterParty(bountyProgram.getProducerName());
				wallet.setCounterParty(user.getName());
				walletList.add(wallet);
			
			}
		}
		
		return walletList;
	}


}
