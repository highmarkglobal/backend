package tech.highmarkglobal.bcm.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import io.swagger.annotations.ApiModel;
import tech.highmarkglobal.bcm.domain.Validator;
import tech.highmarkglobal.bcm.service.BlockchainValidationResultService;
import tech.highmarkglobal.blockchain.config.BlockchainConfig;

@ApiModel
@Service
public class BlockchainValidationResultServiceImpl implements BlockchainValidationResultService {

	private HttpStatus callChaincodeAPI(String uri, HashMap<String, String> requestPayload) {

		System.out.println("***** URI: " + uri);

		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("Content-Type", "application/json");

		headers.setAll(map);

		HttpEntity<?> request = new HttpEntity<>(requestPayload, headers);
		ResponseEntity<?> response = new RestTemplate().postForEntity(uri, request, String.class);

		return response.getStatusCode();
	}
	
	@Override
	public HttpStatus add(Validator validator) {
		HashMap<String, String> requestPayload = new HashMap<String, String>();
		requestPayload.put("validateResultId", validator.getId());
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS");
		requestPayload.put("date", df.format(new Date())); //validator.getValidatorDate().toString());
		requestPayload.put("bountyName", validator.getBountyName());
		requestPayload.put("assetName", validator.getAssetName());
		requestPayload.put("validationRecommendation", validator.getValidationRecommendation());
				
		return callChaincodeAPI(BlockchainConfig.URI_VALIDATION_RESULT, requestPayload);
	}

}
