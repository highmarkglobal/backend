package tech.highmarkglobal.bcm.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import tech.highmarkglobal.bcm.domain.AddressDetails;
import tech.highmarkglobal.bcm.domain.BillingDetails;
import tech.highmarkglobal.bcm.domain.DepositDetails;
import tech.highmarkglobal.bcm.domain.User;
import tech.highmarkglobal.bcm.domain.UserDetails;
import tech.highmarkglobal.bcm.mail.UserMailService;
import tech.highmarkglobal.bcm.repo.UserDetailsRepository;
import tech.highmarkglobal.bcm.repo.UserRepository;
import tech.highmarkglobal.bcm.service.UserService;
import tech.highmarkglobal.bcm.web.dto.UserDTO;
import tech.highmarkglobal.bcm.web.dto.UserDetailsDTO;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
    private UserRepository userRepository;
	
	@Autowired
	private UserDetailsRepository userDetailsRepo;
	
	@Autowired
	private UserMailService userMailService;
	
	@Autowired
    private PasswordEncoder passwordEncoder;
	
	
	
	@Override
	public boolean isEmailExits(String email) {
		return userRepository.existsByEmail(email); 
	}

	@Override
	public UserDetails save(UserDetailsDTO userDetailsDTO) {
		// Creating user's account
		
		userDetailsDTO.setPassword(RandomStringUtils.randomAlphanumeric(16));
		
        UserDetails userDetails = new UserDetails(userDetailsDTO.getEntityType(),userDetailsDTO.getCorporationName(),userDetailsDTO.getContactFirstName(),userDetailsDTO.getContactLastName(),userDetailsDTO.getContactEmail(),
        		userDetailsDTO.getPassword(),userDetailsDTO.getContactPhoneNumber(),userDetailsDTO.getAddress1(),userDetailsDTO.getAddres2(),userDetailsDTO.getCountry(),userDetailsDTO.getCity(),userDetailsDTO.getProvince(),userDetailsDTO.getState(),
        		userDetailsDTO.getPostalCode(),userDetailsDTO.getCorporationNumber(),userDetailsDTO.getRegistrationNumber(),userDetailsDTO.getTaxNumber(),userDetailsDTO.getYearlyTurnover(),userDetailsDTO.getNumberOfEmployees(),
        		userDetailsDTO.getUserType());
        
        userDetails.setPassword(passwordEncoder.encode(userDetails.getPassword()));
        
        
        
/*        if(userDetailsDTO.getEntityType().contains("Producer"))
        {
        	userDetailsDTO.setUserType("Producer");
        }
        else if (userDetailsDTO.getEntityType().contains("Consumer"))
        {
        	userDetailsDTO.setUserType("Consumer");
        }
        else {
        	userDetailsDTO.setUserType("Tester");
        }*/
        //user.setRoles(Collections.singleton("ROLE_USER"));
        UserDetails userReturn = userDetailsRepo.save(userDetails);
        
        //Creating User Details object
        User ud = new User();
/*        ud.setAddressDetails(new AddressDetails("","",""));
        ud.setBillingDetails(new BillingDetails("",""));
        ud.setDepositDetails(new DepositDetails("","","",""));
        ud.setWallet(new Double(0));
        ud.setUserId(userReturn.getId());*/
        ud.setId(userReturn.getId());
        ud.setEmail(userReturn.getContactEmail());
        ud.setPassword(userReturn.getPassword());
        ud.setUserDetailsId(userReturn.getId());
        if(userReturn.getEntityType().contains("Institution"))
		{
        	 ud.setName(userReturn.getCorporationName() + "(" +userReturn.getContactFirstName() + " " + userReturn.getContactLastName()+ ")");
		}
        else if(userReturn.getEntityType().contains("Corporation"))
        {
        	 ud.setName(userReturn.getCorporationName() + "(" +userReturn.getContactFirstName() + " " + userReturn.getContactLastName()+ ")");
        }
        else if(userReturn.getEntityType().contains("Non-Profit"))
        {
        	 ud.setName(userReturn.getCorporationName() + "(" +userReturn.getContactFirstName() + " " + userReturn.getContactLastName()+ ")");
        }
        else if(userReturn.getEntityType().contains("Other"))
        {
        	 ud.setName(userReturn.getCorporationName() + "(" +userReturn.getContactFirstName() + " " + userReturn.getContactLastName()+ ")");
        }
       
        else if(userReturn.getEntityType().contains("Individual"))
        {
        	 ud.setName(userReturn.getContactFirstName() + " " + userReturn.getContactLastName());
        }
        //ud.setUserType(userReturn.getUserType());
        if(userReturn.getEntityType().contains("Producer"))
        {
        	ud.setUserType("Producer");
        }
        else if (userReturn.getEntityType().contains("Consumer"))
        {
        	ud.setUserType("Consumer");
        }
        else if(userReturn.getEntityType().contains("Tester"))
        {
        	
        	ud.setUserType("Tester");
        }
        
        else {
        	ud.setUserType("Validator");
        }
        ud.setRoles(Collections.singleton("ROLE_USER"));
        userRepository.save(ud);
        userMailService.onboardingUser(userDetailsDTO);
        
        return userReturn;
	}

	@Override
	public User getUserById(String id) {
		// TODO Auto-generated method stub
		return userRepository.findById(id);
	}

	@Override
	public User getUserByEmail(String email) {
		// TODO Auto-generated method stub
		return userRepository.findByEmail(email).get();
	}
	
	@Override
	public UserDetails getUserDetails(String userId) {
		// TODO Auto-generated method stub
		ArrayList<UserDetails> ud = (ArrayList<UserDetails>) userDetailsRepo.findById(userId);
		UserDetails userDetails = ud.get(0);
		return userDetails;
	}
	

}
