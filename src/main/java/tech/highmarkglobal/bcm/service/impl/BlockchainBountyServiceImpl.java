package tech.highmarkglobal.bcm.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import io.swagger.annotations.ApiModel;
import tech.highmarkglobal.bcm.domain.BountyProgram;
import tech.highmarkglobal.bcm.service.BlockchainBountyService;
import tech.highmarkglobal.bcm.service.BountyProgramService;
import tech.highmarkglobal.bcm.web.dto.BountyProgramDTO;
import tech.highmarkglobal.blockchain.config.BlockchainConfig;

@ApiModel
@Service
public class BlockchainBountyServiceImpl implements BlockchainBountyService {
	@Autowired
	BountyProgramService bountyProgramService;
	
    private HttpStatus callChaincodeAPI(String uri, HashMap<String, String> requestPayload) {
		
		System.out.println("***** URI: " + uri);
		
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("Content-Type",  "application/json");
		
		headers.setAll(map);
		
		HttpEntity<?> request = new HttpEntity<>(requestPayload, headers);
		ResponseEntity<?> response = new RestTemplate().postForEntity(uri, request, String.class);
		
		return response.getStatusCode();	
    }	
		
	@Override
	public HttpStatus add(BountyProgram bounty) {
		
		HashMap<String, String> requestPayload = new HashMap<String, String>();		
		
		//String bountyId = bountyProgramService.findBounty(bountyDTO.getBountyName(), bountyDTO.getBountyDesc(), bountyDTO.getProducerId(), bountyDTO.getProductId(), bountyDTO.getTestingCategory()).getId();
		requestPayload.put("bountyId", bounty.getId());
		requestPayload.put("bountyName", bounty.getBountyName());		
		requestPayload.put("assetId", bounty.getProducerName());
		requestPayload.put("assetVersion", "v1.0");
		requestPayload.put("startDate", bounty.getStartDate().toString());
		requestPayload.put("endDate", bounty.getEndDate().toString());
		requestPayload.put("reward", bounty.getReward().toString());
		requestPayload.put("claimFeatureType", "Functional");
		requestPayload.put("claimFeatureName", "Feature Name");
		requestPayload.put("bountyInstructionDocument", "bLwj0eNFuCrkjzvXnC2lSoxyS21RmO5ZhpXd");
		requestPayload.put("initiator", bounty.getProducerId());
		requestPayload.put("producer", BlockchainConfig.CLASS_PRODUCER + "#" + bounty.getProducerId());
		
		return callChaincodeAPI(BlockchainConfig.URI_BOUNTY, requestPayload);		
	}
}
