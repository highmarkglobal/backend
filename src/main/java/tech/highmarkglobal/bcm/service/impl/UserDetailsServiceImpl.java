package tech.highmarkglobal.bcm.service.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import tech.highmarkglobal.bcm.domain.UserDetails;
import tech.highmarkglobal.bcm.repo.UserDetailsRepository;
import tech.highmarkglobal.bcm.service.UserDetailsService;
import tech.highmarkglobal.bcm.web.dto.UserDetailsDTO;
import tech.highmarkglobal.bcm.web.transformer.DTOToDomainTransformer;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	
	private static final Logger logger=LoggerFactory.getLogger(BountyProgramServiceImpl.class);
	
	@Autowired
	UserDetailsRepository userDetailsRepository;

	@Override
	public UserDetailsDTO saveUserDetails(UserDetailsDTO userDetailsDTO) {
		// TODO Auto-generated method stub
		UserDetails userDetails = DTOToDomainTransformer.transform(userDetailsDTO);
		userDetailsRepository.save(userDetails);
		return DTOToDomainTransformer.transform(userDetails);
	}

/*	@Override
	public UserDetails addMoneyToWallet(String userId, Double amount) {
		// TODO Auto-generated method stub
		//UserDetails userDetails = new UserDetails();
		Collection<UserDetails> ud = new ArrayList<UserDetails>();
		System.out.println("UserId:"+userId);
		ud = userDetailsRepository.findByUserId(userId);
		if (ud == null) {
			System.out.println("ud null");
		}
		
		for (UserDetails userDetails : ud) {
			System.out.println("Inside for : "+userDetails.getWallet());
			Double walletAmt = userDetails.getWallet() + amount;
			userDetails.setWallet(walletAmt);
			System.out.println("After adding : "+userDetails.getWallet());
			userDetailsRepository.save(userDetails);
			return userDetails;
		}
		return null;
	}
*/
	@Override
	public UserDetails getUserDetails(String userId) {
		// TODO Auto-generated method stub
		ArrayList<UserDetails> ud = (ArrayList<UserDetails>) userDetailsRepository.findById(userId);
		UserDetails userDetails = ud.get(0);
		return userDetails;
	}
	
	//public UserDetailsDTO

}
