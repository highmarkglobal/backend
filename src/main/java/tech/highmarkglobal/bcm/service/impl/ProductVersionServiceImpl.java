package tech.highmarkglobal.bcm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.highmarkglobal.bcm.domain.Product;
import tech.highmarkglobal.bcm.domain.ProductVersion;
import tech.highmarkglobal.bcm.repo.ProductVersionRepository;
import tech.highmarkglobal.bcm.repo.ProductsRepository;
import tech.highmarkglobal.bcm.service.ProductVersionService;
import tech.highmarkglobal.bcm.web.dto.ProductVersionDTO;
@Service
public class ProductVersionServiceImpl implements ProductVersionService {
	
	@Autowired
	ProductVersionRepository productVersionRepository;
	
	@Autowired
	ProductsRepository productsRepository;

	@Override
	public ProductVersion saveProductsVersion(ProductVersionDTO productsversionDTO) {
		
		Product product=productsRepository.findById(productsversionDTO.getProductId());
		ProductVersion productVersion=new ProductVersion();
		productVersion.setName(productsversionDTO.getName());
		productVersion.setProduct(product);
		productVersion.setProductId(productsversionDTO.getProductId());
		return productVersionRepository.save(productVersion);

	}

}
