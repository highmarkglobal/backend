package tech.highmarkglobal.bcm.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import io.swagger.annotations.ApiModel;
import tech.highmarkglobal.bcm.domain.JoinBounty;
import tech.highmarkglobal.bcm.service.BlockchainContractBountyService;
import tech.highmarkglobal.bcm.service.JoinBountyService;
import tech.highmarkglobal.bcm.web.dto.JoinBountyDTO;
import tech.highmarkglobal.blockchain.config.BlockchainConfig;

@ApiModel
@Service
public class BlockchainContractBountyServiceImpl implements BlockchainContractBountyService {
	@Autowired
	JoinBountyService joinBountyService;
	
	private HttpStatus callChaincodeAPI(String uri, HashMap<String, String> requestPayload) {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("Content-Type",  "application/json");
		
		headers.setAll(map);
		
		HttpEntity<?> request = new HttpEntity<>(requestPayload, headers);
		ResponseEntity<?> response = new RestTemplate().postForEntity(uri, request, String.class);
		
		return response.getStatusCode();		
	}
	
	@Override
	public HttpStatus join(JoinBounty joinBounty) {
		HashMap<String, String> requestPayload = new HashMap<String, String>();		
		requestPayload.put("contractId", joinBounty.getId());
		requestPayload.put("contractDate", new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		requestPayload.put("bounty", BlockchainConfig.CLASS_BOUNTY + "#" + joinBounty.getBountyID());
		requestPayload.put("asset",BlockchainConfig.CLASS_ASSET + "#" + joinBounty.getAssetID());
		requestPayload.put("producer",BlockchainConfig.CLASS_PRODUCER + "#" + joinBounty.getProducerID());
		requestPayload.put("tester",BlockchainConfig.CLASS_TESTER + "#" + joinBounty.getTesterID());
		callChaincodeAPI(BlockchainConfig.URI_CONTRACT_BOUNTY, requestPayload);
		
		
		requestPayload.clear();		
		requestPayload.put("contractBounty", BlockchainConfig.CLASS_CONTRACT_BOUNTY + "#" + joinBounty.getId());
		
		return callChaincodeAPI(BlockchainConfig.URI_JOIN_BOUNTY, requestPayload);
	}
	
}

