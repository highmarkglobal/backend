package tech.highmarkglobal.bcm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.highmarkglobal.bcm.domain.SmartContract;
import tech.highmarkglobal.bcm.repo.SmartContractRepository;
import tech.highmarkglobal.bcm.service.SmartContractService;
import tech.highmarkglobal.bcm.web.dto.SmartContractDTO;
import tech.highmarkglobal.bcm.web.transformer.DTOToDomainTransformer;
@Service
public class SmartContractServiceImpl implements SmartContractService {
	
   @Autowired
   private SmartContractRepository smartContractRep;

	@Override
	public SmartContractDTO saveSmartContract(SmartContractDTO smartContractDTO) {
		
		 SmartContract smartContaract = DTOToDomainTransformer.transform(smartContractDTO);
		 SmartContract createdSmartContract = smartContractRep.save(smartContaract);
		 SmartContractDTO dto = DTOToDomainTransformer.transform(createdSmartContract);
		 return dto;
	}

}
