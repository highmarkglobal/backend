package tech.highmarkglobal.bcm.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.highmarkglobal.bcm.domain.AssetType;
import tech.highmarkglobal.bcm.domain.ProductDomain;
import tech.highmarkglobal.bcm.repo.AssetTypesRepository;
import tech.highmarkglobal.bcm.service.AssetTypeService;
import tech.highmarkglobal.bcm.web.dto.AssetTypeDTO;
import tech.highmarkglobal.bcm.web.dto.ProductDomainDTO;
import tech.highmarkglobal.bcm.web.transformer.DTOToDomainTransformer;
@Service
public class AssetTypeServiceImple implements AssetTypeService {
	
	@Autowired
	AssetTypesRepository assetTypesRepository;
	@Override
	public void saveAssetType(AssetTypeDTO assetTypeDTO) {
		AssetType asset_type=new AssetType();
		asset_type.setDescription(assetTypeDTO.getDescription());
		asset_type.setName(assetTypeDTO.getName());
		assetTypesRepository.save(asset_type);
	}
	@Override
	public List<AssetType> findAll() {
		return assetTypesRepository.findAll();
	}
	@Override
	public AssetTypeDTO getById(String id) {
		AssetTypeDTO assetDTO = new AssetTypeDTO();
		AssetType asset = assetTypesRepository.findById(id);
	    assetDTO = DTOToDomainTransformer.transform(asset);
		return assetDTO;
	}

}
