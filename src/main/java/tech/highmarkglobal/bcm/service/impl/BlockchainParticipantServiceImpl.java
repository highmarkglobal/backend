package tech.highmarkglobal.bcm.service.impl;

import java.util.HashMap;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import tech.highmarkglobal.bcm.domain.UserDetails;
import tech.highmarkglobal.bcm.service.BlockchainParticipantService;
import tech.highmarkglobal.bcm.web.dto.UserDetailsDTO;
import tech.highmarkglobal.blockchain.config.BlockchainConfig;
import io.swagger.annotations.ApiModel;


@ApiModel
@Service
public class BlockchainParticipantServiceImpl implements BlockchainParticipantService {
	
	private HttpStatus callChaincodeAPI(String uri, HashMap<String, String> requestPayload) {
		
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("Content-Type",  "application/json");
		
		headers.setAll(map);
		
		HttpEntity<?> request = new HttpEntity<>(requestPayload, headers);
		ResponseEntity<?> response = new RestTemplate().postForEntity(uri, request, String.class);
		
		return response.getStatusCode();		
	}
	
	public HttpStatus addProducer(HashMap<String, String> requestPayload) {
		return callChaincodeAPI(BlockchainConfig.URI_PRODUCER, requestPayload);		
	}
	
	public HttpStatus addConsumer(HashMap<String, String> requestPayload) {
		return callChaincodeAPI(BlockchainConfig.URI_CONSUMER, requestPayload);		
	}
	
	public HttpStatus addTester(HashMap<String, String> requestPayload) {
		return callChaincodeAPI(BlockchainConfig.URI_TESTER, requestPayload);		
	}
	
	public HttpStatus addValidator(HashMap<String, String> requestPayload) {
		return callChaincodeAPI(BlockchainConfig.URI_VALIDATOR, requestPayload);
		
	}
		
	@Override
	public HttpStatus add(UserDetails user) {
		HashMap<String, String> requestPayload = new HashMap<String, String>();		
		requestPayload.put("Id", user.getId());
		requestPayload.put("Name", user.getCorporationName());		

		String userType;
		if(user.getEntityType().contains(BlockchainConfig.PRODUCER))
			userType = new String(BlockchainConfig.PRODUCER);
		else if(user.getEntityType().contains(BlockchainConfig.CONSUMER))
			userType = new String(BlockchainConfig.CONSUMER);
		else if(user.getEntityType().contains(BlockchainConfig.TESTER))
			userType = new String(BlockchainConfig.TESTER);
		else // VALIDATOR
			userType = new String(BlockchainConfig.VALIDATOR);		
		
				
		if(userType.equals(BlockchainConfig.PRODUCER)) { 
			requestPayload.put(BlockchainConfig.CLASS, BlockchainConfig.CLASS_PRODUCER);
			return addProducer(requestPayload);			
		} else if(userType.equals(BlockchainConfig.CONSUMER)) {
			requestPayload.put(BlockchainConfig.CLASS, BlockchainConfig.CLASS_CONSUMER);
			return addConsumer(requestPayload);
		} else if(userType.equals(BlockchainConfig.TESTER)) {  
			requestPayload.put(BlockchainConfig.CLASS, BlockchainConfig.CLASS_TESTER);
			
			return addTester(requestPayload);
		}  else { // ( userType.equals(VALIDATOR)) 
			requestPayload.put(BlockchainConfig.CLASS, BlockchainConfig.CLASS_VALIDATOR);
			return addValidator(requestPayload);
		}		
		
	}


}
