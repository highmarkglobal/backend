package tech.highmarkglobal.bcm.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsCriteria;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;


import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;

import tech.highmarkglobal.bcm.domain.FileInfo;
import tech.highmarkglobal.bcm.service.FileInfoService;

@Service
public class FileInfoServiceImpl implements FileInfoService {

	private static final Logger logger = LoggerFactory.getLogger(FileInfoServiceImpl.class);
	
	@Autowired
	private GridFsTemplate gridFsTemplate;
	
	public FileInfo save(byte[] file, String name) {
		GridFSFile gridFile = gridFsTemplate.store(new  ByteArrayInputStream(file), name);		
		return create(gridFile);
	}
	
	
	private FileInfo create(GridFSFile gridFile) {
		FileInfo info = new FileInfo();
		info.setId(gridFile.getId().toString());
		info.setName(gridFile.getFilename());
		info.setSize(gridFile.getLength());
		info.setContentType(gridFile.getContentType());		
		//info.setCreatedDateTime(LocalDateTime.now());
		return info;
	}
	
	 public GridFSDBFile getById(String id) {
		  return this.gridFsTemplate.findOne(new Query(Criteria.where("_id").is(
		    id)));
		 }
	 
	 
	

}
