package tech.highmarkglobal.bcm.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import io.swagger.annotations.ApiModel;
import tech.highmarkglobal.bcm.domain.TestResult;
import tech.highmarkglobal.bcm.service.BlockchainTestResultService;
import tech.highmarkglobal.bcm.service.TestResultService;
import tech.highmarkglobal.blockchain.config.BlockchainConfig;

@ApiModel
@Service
public class BlockchainTestResultServiceImpl implements BlockchainTestResultService {
	
	private HttpStatus callChaincodeAPI(String uri, HashMap<String, String> requestPayload) {

		System.out.println("***** URI: " + uri);

		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("Content-Type", "application/json");

		headers.setAll(map);

		HttpEntity<?> request = new HttpEntity<>(requestPayload, headers);
		ResponseEntity<?> response = new RestTemplate().postForEntity(uri, request, String.class);

		return response.getStatusCode();
	}

	@Override
	public HttpStatus add(TestResult testResult) {
		HashMap<String, String> requestPayload = new HashMap<String, String>();
		requestPayload.put("testResultId", testResult.getId());
		requestPayload.put("bountyName", testResult.getBountyName());
		requestPayload.put("assetName", testResult.getAssetName());
		// requestPayload.put("featureType", testResultsDTO.getFeatures()());
		requestPayload.put("featureType", "functional"); // Need to change
		requestPayload.put("featureName", "e-prescription, computer aid diagnosis, medication tracking, computerized physician order entry"); // Need to change
		requestPayload.put("platformProvider", testResult.getPlatformProvider());
		requestPayload.put("platformOS", testResult.getPlatformOS());
		requestPayload.put("toolsUsed", testResult.getToolsUsed());
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS");
		requestPayload.put("testDate", df.format(new Date())); 	 //testResultsDTO.getTestDate().toString());		
		requestPayload.put("recommendation", testResult.getRecommendation());
		requestPayload.put("bugReportFile",  "Sample Test Report"); //testResultsDTO.getBugsReport().toString());
		requestPayload.put("url", "Sample URL" ); //testResultsDTO.getURL());
		requestPayload.put("testScriptFile", "Sample Test Script"); //testResultsDTO.getTestScript().toString());
		requestPayload.put("testDataFile", "Sample Test Data" ); // testResultsDTO.getTestData().toString());
		requestPayload.put("scriptLogFile", "Sample Script Log" ); //testResultsDTO.getScriptLog().toString());
		requestPayload.put("tester", BlockchainConfig.CLASS_TESTER +"#" + testResult.getTesterId()); //
		
		return callChaincodeAPI(BlockchainConfig.URI_TEST_RESULT, requestPayload);
	}
}
