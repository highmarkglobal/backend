package tech.highmarkglobal.bcm.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.highmarkglobal.bcm.domain.ProductDomain;
import tech.highmarkglobal.bcm.repo.ProductDomainRepository;
import tech.highmarkglobal.bcm.service.DomainService;
import tech.highmarkglobal.bcm.web.dto.ProductDomainDTO;
import tech.highmarkglobal.bcm.web.transformer.DTOToDomainTransformer;
@Service
public class DomainServiceImpl implements DomainService {

	@Autowired
	ProductDomainRepository productDomainRepo;
	
	@Override
	public void saveDomain(ProductDomainDTO productDomainDTO) {

		ProductDomain domain=DTOToDomainTransformer.transform(productDomainDTO);
		productDomainRepo.save(domain);
		
	}

	@Override
	public List<ProductDomain> findAll() {
		 return productDomainRepo.findAll();
	}

	@Override
	public void saveDomains(List<ProductDomainDTO> productDomainDTOs) {
		if(productDomainDTOs!=null && productDomainDTOs.size()!=0) {
			for (ProductDomainDTO productDomainDTO : productDomainDTOs) {
				ProductDomain domain=DTOToDomainTransformer.transform(productDomainDTO);
				productDomainRepo.save(domain);
			}
		
		}
	}
	
	@Override
	public ProductDomainDTO findByDomainId(String domainId) {
		ProductDomainDTO pdDTO = new ProductDomainDTO();
		ProductDomain pd = productDomainRepo.findById(domainId);
	    pdDTO = DTOToDomainTransformer.transform(pd);
		return pdDTO;
	
	}
	
	@Override
	public List<ProductDomainDTO> findByDomainName(String domainName) {
		List<ProductDomainDTO> dtos=new ArrayList<ProductDomainDTO>();
		List<ProductDomain> pd = productDomainRepo.findByDomainName(domainName);
		
		pd.forEach(bounty-> {
			ProductDomainDTO pdDTO = DTOToDomainTransformer.transform(bounty);
			dtos.add(pdDTO);
		});
	    
		return dtos;
	}


}
