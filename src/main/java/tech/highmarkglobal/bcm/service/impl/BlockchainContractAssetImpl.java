package tech.highmarkglobal.bcm.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import io.swagger.annotations.ApiModel;
import tech.highmarkglobal.bcm.service.PurchaseService;
import tech.highmarkglobal.bcm.service.SmartContractService;
import tech.highmarkglobal.bcm.domain.Purchase;
import tech.highmarkglobal.bcm.service.BlockchainContractAssetService;
import tech.highmarkglobal.blockchain.config.BlockchainConfig;;

@ApiModel
@Service
public class BlockchainContractAssetImpl implements BlockchainContractAssetService {
	@Autowired
	PurchaseService purchaseService;
	
	@Autowired
	SmartContractService smartContract;
	
	private HttpStatus callChaincodeAPI(String uri, HashMap<String, String> requestPayload) {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("Content-Type",  "application/json");
		
		headers.setAll(map);
		
		HttpEntity<?> request = new HttpEntity<>(requestPayload, headers);
		ResponseEntity<?> response = new RestTemplate().postForEntity(uri, request, String.class);
		
		return response.getStatusCode();		
	}
	
	@Override
	public HttpStatus makeContract(Purchase purchase) {
		
		HashMap<String, String> requestPayload = new HashMap<String, String>();		
		requestPayload.put("contractId", purchase.getId());
		requestPayload.put("contractDate", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		requestPayload.put("asset", BlockchainConfig.CLASS_ASSET + "#" + purchase.getAssetId());
		requestPayload.put("consumer",BlockchainConfig.CLASS_CONSUMER + "#" + purchase.getConsumerId());
		callChaincodeAPI(BlockchainConfig.URI_CONTRACT_ASSET, requestPayload);
		
		
		requestPayload.clear();		
		requestPayload.put("contractAsset", BlockchainConfig.CLASS_CONTRACT_ASSET + "#" + purchase.getId());
		
		return callChaincodeAPI(BlockchainConfig.URI_BUY_ASSET, requestPayload);
	}	
	
//	@Override 
//	public HttpStatus makeTranscation(Purchase purchase) {
//		
//	}
}
