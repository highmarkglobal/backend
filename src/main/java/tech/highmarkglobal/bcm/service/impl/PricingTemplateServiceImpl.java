package tech.highmarkglobal.bcm.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.highmarkglobal.bcm.domain.PricingTemplate;
import tech.highmarkglobal.bcm.repo.PricingTemplateRepository;
import tech.highmarkglobal.bcm.service.PricingTemplateService;
import tech.highmarkglobal.bcm.web.dto.PricingTemplateDTO;
import tech.highmarkglobal.bcm.web.transformer.DTOToDomainTransformer;

@Service
public class PricingTemplateServiceImpl implements PricingTemplateService {
	
	@Autowired
	private PricingTemplateRepository pricingTemplateRepo;

	@Override
	public void savePricingTemplates(List<PricingTemplateDTO> pricingTemplateDtos) {
		// TODO Auto-generated method stub
		
	
		pricingTemplateDtos.forEach(p -> {
			PricingTemplate pricingTemplate = DTOToDomainTransformer.transform(p);
			pricingTemplateRepo.save(pricingTemplate);
		});
		
		
	}
	
	@Override
	public List<PricingTemplate> getByPriceRange(Double start,Double end) {
		List<PricingTemplate> ptReturnList = new ArrayList<PricingTemplate>();
		List<PricingTemplate> ptList = pricingTemplateRepo.findAll();
		ptList.forEach(pricingTemp-> {
			if (pricingTemp.getPrice().compareTo(start)>=0 && pricingTemp.getPrice().compareTo(end) <= 0) {
				ptReturnList.add(pricingTemp);
			}
		});
		return ptReturnList;
	}

}
