package tech.highmarkglobal.bcm.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.highmarkglobal.bcm.domain.Transaction;
import tech.highmarkglobal.bcm.repo.TransactionRepository;
import tech.highmarkglobal.bcm.service.TransactionService;

@Service
public class TransactionServiceImpl implements TransactionService {

	
	@Autowired
	TransactionRepository txnRepository;
	
	@Override
	public Transaction saveTransaction(Transaction txn) {
		// TODO Auto-generated method stub
		Transaction trans = new Transaction();
		trans = txnRepository.save(txn);
		return trans;
	}
	
	@Override
	public Collection<Transaction> findTransactionByUserId(String userId) {
		System.out.println("Inside findTransactionByUserId");
		
		Collection<Transaction> txn = txnRepository.findByUserId(userId);
		System.out.println("Before return");
		return txn;
		
	}


}
