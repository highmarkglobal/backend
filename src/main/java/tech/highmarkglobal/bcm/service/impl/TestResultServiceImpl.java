package tech.highmarkglobal.bcm.service.impl;


import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.tomcat.jni.File;
//import org.hibernate.validator.internal.util.logging.Log_.logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;

import com.mongodb.gridfs.GridFSDBFile;

import tech.highmarkglobal.bcm.domain.Asset;
import tech.highmarkglobal.bcm.domain.FileInfo;
import tech.highmarkglobal.bcm.domain.JoinBounty;
import tech.highmarkglobal.bcm.domain.TestResult;
import tech.highmarkglobal.bcm.domain.User;
import tech.highmarkglobal.bcm.repo.BountyProgramRepository;
import tech.highmarkglobal.bcm.repo.JoinBountyRepository;
import tech.highmarkglobal.bcm.repo.TestResultRepository;
import tech.highmarkglobal.bcm.repo.UserRepository;
import tech.highmarkglobal.bcm.service.FileInfoService;
import tech.highmarkglobal.bcm.service.JoinBountyService;
import tech.highmarkglobal.bcm.service.TestResultService;

import tech.highmarkglobal.bcm.web.dto.TestResultDTO;
import tech.highmarkglobal.bcm.web.transformer.DTOToDomainTransformer;
@Service
public class TestResultServiceImpl implements TestResultService {
	
	@Autowired
	TestResultRepository testResultRepository;
	
	@Autowired
	private FileInfoService fileInfoService;
	
	@Autowired
	private GridFsTemplate gridFsTemplate;
	
	
	
	
	public static final String COLLECTION_NAME = "TestResult";

	
	@Override
	public TestResult saveTestResult(TestResultDTO testResultDTO,byte[] reportfile,byte[] testscriptfile,byte[] scriptlogfile) {
		TestResult testResult=new TestResult();
		//asset_type.setDescription(assetDTO.getDescription());
		testResult.setBountyName(testResultDTO.getBountyName());
		testResult.setAssetName(testResultDTO.getAssetName());
		/*testResult.setFeatureType(testResultDTO.getFeatureType());
		testResult.setFunctionalFeature(testResultDTO.getFunctionalFeature());*/
		testResult.setFeatures(testResultDTO.getFeatures());
		testResult.setPlatformProvider(testResultDTO.getPlatformProvider());
		testResult.setPlatformOS(testResultDTO.getPlatformOS());
		testResult.setTestType(testResultDTO.getTestType());
		testResult.setToolsUsed(testResultDTO.getToolsUsed());
		testResult.setTestDate(testResultDTO.getTestDate());
		testResult.setRecommendation(testResultDTO.getRecommendation());
		//testResult.setBugsReport(testResultDTO.getBugsReport());
		testResult.setURL(testResultDTO.getURL());
		//testResult.setTestScript(testResultDTO.getTestScript());
		//testResult.setTestData(testResultDTO.getTestData());
		//testResult.setScriptLog(testResultDTO.getScriptLog());
		testResult.setTestReportFilaname(testResultDTO.getTestReportFilaname());
		testResult.setTestDataFilename(testResultDTO.getTestDataFilename());
		testResult.setTestScriptFilename(testResultDTO.getTestScriptFilename());
		testResult.setScriptLogFilename(testResultDTO.getScriptLogFilename());
		
		
		//adding
		 FileInfo testReport=null;
		 testReport=	fileInfoService.save(reportfile,"TestReport");
		 testResult.setTestReport(testReport);
		
		
		 FileInfo testData=null;
		testData=	fileInfoService.save(testscriptfile,"TestData");
		testResult.setTestData(testData);
		
		 FileInfo scriptLog=null;
		scriptLog=	fileInfoService.save(scriptlogfile,"ScriptLog");
		testResult.setScriptLog(scriptLog);
		
		
        testResultRepository.save(testResult);

		return testResult;
	}

	@Override
	public List<TestResult> findAll() {
		return testResultRepository.findAll();
	}

	
	@Override
	public GridFSDBFile getTestReportById(String id) {
		GridFSDBFile file = gridFsTemplate.findOne(new Query(Criteria.where("_id").is(id)));
		return file;	
	}
	
}