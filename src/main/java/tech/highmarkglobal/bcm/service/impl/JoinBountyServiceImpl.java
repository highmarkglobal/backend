package tech.highmarkglobal.bcm.service.impl;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

//import org.hibernate.validator.internal.util.logging.Log_.logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import org.springframework.stereotype.Service;

import tech.highmarkglobal.bcm.domain.JoinBounty;
import tech.highmarkglobal.bcm.repo.JoinBountyRepository;
import tech.highmarkglobal.bcm.service.JoinBountyService;
import tech.highmarkglobal.bcm.web.dto.JoinBountyDTO;
import tech.highmarkglobal.bcm.web.transformer.DTOToDomainTransformer;
@Service
public class JoinBountyServiceImpl implements JoinBountyService {
	
	@Autowired
	JoinBountyRepository joinBountyRepository;
	
	@Autowired
	  private MongoTemplate mongoTemplate;
	
	

	
	@Override
	public JoinBounty saveJoinBounty(JoinBountyDTO joinBountyDTO) {
		JoinBounty joinBounty=new JoinBounty();
		//JoinBounty_type.setDescription(JoinBountyDTO.getDescription());
		joinBounty.setBountyID(joinBountyDTO.getBountyID());
		joinBounty.setBountyName(joinBountyDTO.getBountyName());
		joinBounty.setAssetID(joinBountyDTO.getAssetID());
		joinBounty.setAssetName(joinBountyDTO.getAssetName());
		joinBounty.setProducerID(joinBountyDTO.getProducerID());
		joinBounty.setProducerName(joinBountyDTO.getProducerName());
		joinBounty.setTesterID(joinBountyDTO.getTesterID());
		joinBounty.setTesterName(joinBountyDTO.getTesterName());
		joinBounty.setJoinDate(joinBountyDTO.getJoinDate());
		
		joinBountyRepository.save(joinBounty);
		
		return joinBounty;
	}
	@Override
	public List<JoinBounty> findAll() {
		return joinBountyRepository.findAll();
	}
	@Override
	public JoinBounty getByBountyID(String bountyID) {
		JoinBountyDTO joinBountyDTO = new JoinBountyDTO();
		JoinBounty joinBounty = joinBountyRepository.findByBountyID(bountyID);
	    joinBountyDTO = DTOToDomainTransformer.transform(joinBounty);
		return joinBounty;
	}
	
	public Collection<JoinBounty> getByTesterID(String testerID)
	{
		JoinBountyDTO joinBountyDTO = new JoinBountyDTO();
		Collection<JoinBounty> joinBounty = joinBountyRepository.findByTesterID(testerID);
	   // joinBountyDTO = DTOToDomainTransformer.transform(joinBounty);
		return joinBounty;
	}
	

}
