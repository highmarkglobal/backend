package tech.highmarkglobal.bcm.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.highmarkglobal.bcm.domain.TransactionType;
import tech.highmarkglobal.bcm.domain.AssetType;
import tech.highmarkglobal.bcm.domain.PricingTemplate;
import tech.highmarkglobal.bcm.domain.Product;
import tech.highmarkglobal.bcm.domain.ProductDomain;
import tech.highmarkglobal.bcm.domain.ProductSearch;
import tech.highmarkglobal.bcm.domain.ProductVersion;
import tech.highmarkglobal.bcm.domain.Transaction;
import tech.highmarkglobal.bcm.domain.TransactionType;
import tech.highmarkglobal.bcm.domain.UserDetails;
import tech.highmarkglobal.bcm.repo.AssetTypesRepository;
import tech.highmarkglobal.bcm.repo.ProductDomainRepository;
import tech.highmarkglobal.bcm.repo.ProductsRepository;
import tech.highmarkglobal.bcm.repo.TransactionRepository;
import tech.highmarkglobal.bcm.repo.UserDetailsRepository;
import tech.highmarkglobal.bcm.service.PricingTemplateService;
import tech.highmarkglobal.bcm.service.ProductService;
import tech.highmarkglobal.bcm.service.ProductVersionService;
import tech.highmarkglobal.bcm.service.UserDetailsService;
import tech.highmarkglobal.bcm.util.HighmarkUtil;
import tech.highmarkglobal.bcm.web.dto.PricingTemplateDTO;
import tech.highmarkglobal.bcm.web.dto.ProductDTO;
import tech.highmarkglobal.bcm.web.dto.ProductVersionDTO;
import tech.highmarkglobal.bcm.web.transformer.DTOToDomainTransformer;


@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductsRepository productRepo;
	
	@Autowired
	AssetTypesRepository assetTypeRepo;
	
	@Autowired
	ProductDomainRepository productDomainRepository;
	
	@Autowired
	TransactionRepository txnRepo;
	
	@Autowired
	private ProductVersionService productVersionService;
	
	@Autowired
	private PricingTemplateService pricingTemplateService;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	UserDetailsRepository userDetailsRepository;

	
	@Override
	public ProductDTO saveProduct(ProductDTO productDTO) {
		int count=0;
		Product product=new Product();
		/*product.setName(productDTO.getName());
		AssetType assetType = assetTypeRepo.findByName(productDTO.getAssetType());
		product.setAssetType(assetType);
		List<ProductDomain> product_domain = productDomainRepository.findByDomainName(productDTO.getDomainName());
		product.setProductDomain(product_domain);
		product.setServiceCharge(productDTO.getServiceCharge());
		product.setStage(productDTO.getStage());*/
		
		
		String userId = productDTO.getProducerId();
		System.out.println("User Id:"+userId);
		//Check if the user has enough amount in the wallet
		ArrayList<UserDetails> ud = (ArrayList<UserDetails>) userDetailsRepository.findById(userId);
		UserDetails userDetails = ud.get(0);
		/*if (productDTO.getServiceCharge().compareTo(userDetails.getWallet()) > 0) {
			return null;
		}
		
		//Deducting the amount from the wallet
		userDetailsService.addMoneyToWallet(userId, (productDTO.getServiceCharge() * -1));*/
		
		
		System.out.println("Before Transform");
		product = DTOToDomainTransformer.transform(productDTO);
		System.out.println("After Transform.. Before save");
		productRepo.save(product);
		System.out.println("After save");
		productDTO = DTOToDomainTransformer.transform(product);
		
		//Creating transaction entry
		Transaction txn = new Transaction();
		txn.setUserId(userId);
		txn.setCurrency(product.getServiceCharge());
		txn.setTokens(0);
		txn.setTransactionDate(HighmarkUtil.StrtoDate(HighmarkUtil.getCurrentDate()));
		txn.setTransactionType(TransactionType.ASSET.toString());
		txnRepo.save(txn);
		
		return productDTO;	
		/*List<Product> listOfProducts=productRepo.findProductsByName(product.getName());
		if(listOfProducts!=null)
			count=listOfProducts.size()+1;
		ProductVersionDTO productVersionDTO = new ProductVersionDTO();
		productVersionDTO.setProductId(product.getId());
		productVersionDTO.setProductVersion(count);
		ProductVersion productVersion = productVersionService.saveProductsVersion(productVersionDTO);
		
		pricingTemplateService.saveTemplates(productDTO.getPricingTemplateDtos(), productVersion.getId());*/
		
	}
	
	@Override
	public Collection<ProductDTO> findAll() {
		// return productRepo.findAll();
		 
		 Collection<ProductDTO> dtos=new ArrayList<ProductDTO>();
		 Collection<Product> list = productRepo.findAll();
		 list.forEach(product-> {
			 ProductDTO productDTO = DTOToDomainTransformer.transform(product);
			 dtos.add(productDTO);
		 });
		 return dtos;
	}
	
	public Collection<ProductDTO> findProductsByProducerId(String producerId) {
		// return productRepo.findAll();
		 
		 Collection<ProductDTO> dtos=new ArrayList<ProductDTO>();
		 Collection<Product> list = productRepo.findProductsByProducerId(producerId);
		 list.forEach(product-> {
			 ProductDTO productDTO = DTOToDomainTransformer.transform(product);
			 dtos.add(productDTO);
		 });
		 return dtos;
	}

	@Override
	public Product findById(String id) {
		
		return productRepo.findById(id);
	}

	@Override
	public Product updateProduct(ProductDTO productDTO) {
		Product productToUpdate = productRepo.findByName(productDTO.getName());
		productToUpdate.setStage(productDTO.getStage());
		productRepo.save(productToUpdate);
		return productRepo.findByName(productDTO.getName());
	}

	@Override
	public void deleteAsset(String assetname) {
		Product productToUpdate = productRepo.findByName(assetname);
		productRepo.delete(productToUpdate);
		
	}
	/**
	 * 
	 */
	/*@Override
	public List<PricingTemplateDTO> getPricingPolicies(String name , String id) {
		List<PricingTemplateDTO> list = pricingTemplateService.getPricingTemplates(name , id);
		return list;
	}*/

	/*@Override
	public Collection<Product> findProductsByPriceRange(Double start, Double end) {

		Collection<Product> productReturnList=new ArrayList<Product>();
		Collection<Product> productList = productRepo.findAll();
		productList.forEach(product-> {
			Double price = product.getPricingTemplate().getPrice();
			if (price.compareTo(start)>=0 && price.compareTo(end) <= 0) {
				productReturnList.add(product);
			}
		
		});
		return productReturnList;
	}*/

	@Override
	public Collection<Product> findProductsByProductDomain(String prodDomain) {
		Collection<Product> productReturnList=new ArrayList<Product>();
		Collection<Product> productList = productRepo.findAll();
		productList.forEach(product-> {
			List<ProductDomain> productDomainList = product.getProductDomain();
			for (ProductDomain pd : productDomainList) {
				if (pd.getDomainName().equals(prodDomain)) {
					productReturnList.add(product);
					break;
				}
			}
			
		});
		return productReturnList;
	}

	@Override
	public Collection<Product> searchAsset(ProductSearch ps) {
		int nameFlag = 0;
		int priceFlag = 0;
		int domainFlag = 0;
		int assetTypeFlag = 0;
		
		
		Collection<Product> nameFilterList=new ArrayList<Product>();
		Collection<Product> priceFilterList=new ArrayList<Product>();
		Collection<Product> assetTypeFilterList=new ArrayList<Product>();
		Collection<Product> domainFilterList=new ArrayList<Product>();
		
		Collection<Product> productList = productRepo.findAll();
		
		if (ps.getName() != null) {
			nameFlag = 1;
			productList.forEach(product-> {
				if(product.getName().contains(ps.getName())) {
					nameFilterList.add(product);
				}
			});
		}
		
		if (nameFlag == 1) {
			productList.retainAll(nameFilterList);
		}
		
		
		if (productList.size() > 0 && ps.getMinPrice() != null && ps.getMaxPrice() != null) {
			priceFlag = 1;
			productList.forEach(product-> {
				Collection<PricingTemplate> priceList = product.getPricingTemplate();
				for (PricingTemplate price : priceList) {
					if (price.getPrice().compareTo(ps.getMinPrice())>=0 && price.getPrice().compareTo(ps.getMaxPrice()) <= 0) {
						priceFilterList.add(product);
					}
				}
			
			});
		}
		
		if (priceFlag == 1) {
			productList.retainAll(priceFilterList);
		}
		
		if (productList.size() > 0 && ps.getProductDomain() != null) {
			domainFlag = 1;
			productList.forEach(product-> {
				List<ProductDomain> productDomainList = product.getProductDomain();
				for (ProductDomain pd : productDomainList) {
					if (ps.getProductDomain().contains(pd.getDomainName())) {
						domainFilterList.add(product);
						break;
					}
				}
				
			});
		}
		
		if (domainFlag == 1) {
			productList.retainAll(domainFilterList);
		}
		
		if (productList.size() > 0 && ps.getAssetType() != null) {
			assetTypeFlag = 1;
			productList.forEach(product-> {
				if(ps.getAssetType().contains(product.getAssetType().getName())) {
					assetTypeFilterList.add(product);
				}
			});
		}
		
		if (assetTypeFlag == 1) {
			productList.retainAll(assetTypeFilterList);
		}
		
		return productList;
	}
	
}
