package tech.highmarkglobal.bcm.service.impl;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
//import org.hibernate.validator.internal.util.logging.Log_.logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import tech.highmarkglobal.bcm.domain.Asset;
import tech.highmarkglobal.bcm.domain.AssetType;
import tech.highmarkglobal.bcm.domain.FeaturesDetails;
import tech.highmarkglobal.bcm.domain.Product;
import tech.highmarkglobal.bcm.domain.TestResult;
import tech.highmarkglobal.bcm.domain.User;
import tech.highmarkglobal.bcm.domain.Validator;
import tech.highmarkglobal.bcm.repo.AssetRepository;
import tech.highmarkglobal.bcm.repo.TestResultRepository;
import tech.highmarkglobal.bcm.repo.UserRepository;
import tech.highmarkglobal.bcm.repo.ValidatorRepository;
import tech.highmarkglobal.bcm.service.AssetService;
import tech.highmarkglobal.bcm.web.dto.AssetDTO;
import tech.highmarkglobal.bcm.web.dto.AssetTypeDTO;
import tech.highmarkglobal.bcm.web.dto.ProductDTO;
import tech.highmarkglobal.bcm.web.transformer.DTOToDomainTransformer;
@Service
public class AssetServiceImpl implements AssetService {
	
	@Autowired
	AssetRepository assetRepository;
	
	@Autowired
	  private MongoTemplate mongoTemplate;
	
	@Autowired
	TestResultRepository testResultsRepository;
	
	@Autowired
	ValidatorRepository validatorRepository;
	
	@Autowired
	UserRepository userRepository;
	
	public static final String COLLECTION_NAME = "Asset";

	
	@Override
	public Asset saveAsset(AssetDTO assetDTO) {
		Asset asset = new Asset();
		//asset_type.setDescription(assetDTO.getDescription());
		
		asset.setName(assetDTO.getName());
		asset.setAssetType(assetDTO.getAssetType());
        asset.setDomain(assetDTO.getDomain());
        asset.setInterfaceType(assetDTO.getInterfaceType());	
        asset.setDeployomentType(assetDTO.getDeployomentType());	
        asset.setOsType(assetDTO.getOsType());
        asset.setLicensingTerms(assetDTO.getLicensingTerms());	
        asset.setPrice(assetDTO.getPrice());	
        asset.setPricingUnit(assetDTO.getPricingUnit());	
        asset.setFeatures(assetDTO.getFeatures());
/*       asset_type.setFeatureType(assetDTO.getFeatureType());	
        asset_type.setFeatureTechnology(assetDTO.getFeatureTechnology());	
        asset_type.setFeatureName(assetDTO.getFeatureName());	
        asset_type.setFeatureDescription(assetDTO.getFeatureDescription());*/
        asset.setProducerName(assetDTO.getProducerName());
		asset.setServiceDuraiton(assetDTO.getServiceDuraiton());
		asset.setServiceBundledPrice(assetDTO.getServiceBundledPrice());
		asset.setPersistentPerCPU(assetDTO.getPersistentPerCPU());
		asset.setPersistentPerFeature(assetDTO.getPersistentPerFeature());
		asset.setPersistentPrice(assetDTO.getPersistentPrice());
		assetRepository.save(asset);
		
		return asset;
	}
	
	@Override
	public List<Asset> findAll() {
		return assetRepository.findAll();
	}
	@Override
	public Asset getById(String id) {
		AssetDTO assetDTO = new AssetDTO();
		Asset asset = assetRepository.findById(id);
	    assetDTO = DTOToDomainTransformer.transform(asset);
		return asset;
	}
	
	@Override
	public Collection<Asset> getByName(String name) {
		//AssetDTO assetDTO = new AssetDTO();
		Collection<Asset> assetList = assetRepository.findAll();
		Collection<Asset> nameFilterList = new ArrayList<Asset>();
		System.out.println("Asset list size:"+assetList.size());
		
		if(name!=null)
		{
		for (Asset a :assetList )
		{
			if(a.getName().contains(name.trim())) {
				
				nameFilterList.add(a);
			}
		}
		}
	
		System.out.println(nameFilterList);
		return nameFilterList;
	}


	@Override
	public Asset getByAsset(String assetName) {
		AssetDTO assetDTO = new AssetDTO();
		Asset asset = assetRepository.findByName(assetName);
	  //  assetDTO = DTOToDomainTransformer.transform(assetName);
		return asset;
	}
	
	@Override
	public  Collection<Asset> getByProducerId(String producerId) {
		AssetDTO assetDTO = new AssetDTO();
		Collection<Asset> asset = assetRepository.findByProducerId(producerId);
	   // assetDTO = DTOToDomainTransformer.transform(asset);
		return asset;
	}

	public FeaturesDetails getByAssetId(String assetId) throws ParseException {
		
		
		//TestResult testResult= testResultsRepository.findByTesterId(testerId);
		
		Asset asset = assetRepository.findById(assetId);
		
		TestResult testResult= testResultsRepository.findByAssetName(asset.getName());
		
		Validator validator=validatorRepository.findByAssetName(asset.getName());
		
		User user=userRepository.findById(testResult.getId());
		
		FeaturesDetails featuresDetails= new FeaturesDetails();
		
		featuresDetails.setAssetId(asset.getId());
		featuresDetails.setAssetName(testResult.getAssetName());
		featuresDetails.setBountyName(testResult.getBountyName());

		featuresDetails.setFeature_tester(asset.getFeatures());
		featuresDetails.setValidatorResult(validator.getValidationRecommendation());
		return featuresDetails;
	}

}
