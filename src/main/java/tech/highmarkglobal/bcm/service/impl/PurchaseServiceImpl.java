package tech.highmarkglobal.bcm.service.impl;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

//import org.hibernate.validator.internal.util.logging.Log_.logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import tech.highmarkglobal.bcm.domain.AssetType;
import tech.highmarkglobal.bcm.domain.BountyProgram;
import tech.highmarkglobal.bcm.domain.Purchase;
import tech.highmarkglobal.bcm.domain.User;
import tech.highmarkglobal.bcm.domain.Validator;
import tech.highmarkglobal.bcm.domain.Wallet;
import tech.highmarkglobal.bcm.repo.BountyProgramRepository;
import tech.highmarkglobal.bcm.repo.PurchaseRepository;
import tech.highmarkglobal.bcm.repo.TestResultRepository;
import tech.highmarkglobal.bcm.repo.UserRepository;
import tech.highmarkglobal.bcm.service.PurchaseService;
import tech.highmarkglobal.bcm.web.dto.PurchaseDTO;
import tech.highmarkglobal.bcm.web.transformer.DTOToDomainTransformer;
@Service
public class PurchaseServiceImpl implements  PurchaseService {
	
	@Autowired
	PurchaseRepository purchaseRepository;
	
	@Autowired
	BountyProgramRepository bountyProgramRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	TestResultRepository testResultRepository;
	
	@Override
	public Purchase savePurchase(PurchaseDTO purchaseDTO) {
       
		Purchase purchase = new Purchase();
		purchase.setProducerId(purchaseDTO.getProducerId());
		purchase.setProducerName(purchaseDTO.getProducerName());
		purchase.setConsumerId(purchaseDTO.getConsumerId());
		purchase.setConsumerName(purchaseDTO.getConsumerName());
		purchase.setAssetId(purchaseDTO.getAssetId());
		purchase.setAssetName(purchaseDTO.getAssetName());
		purchase.setPurchaseDate(purchaseDTO.getPurchaseDate());
		purchase.setPrice(purchaseDTO.getPrice());
		purchase.setPriceUnit(purchaseDTO.getPriceUnit());
		purchase.setLicensing(purchaseDTO.getLicensing());
		purchase.setPricingOption(purchaseDTO.getPriceUnit());
		purchase.setFeatures(purchaseDTO.getFeatures());
		
		purchaseRepository.save(purchase);
		return purchase;
	}
	@Override
	public List<Purchase> findAll() {
		return purchaseRepository.findAll();
	}
	@Override
	public List<Wallet> getById(String id)
	{
		List<Wallet> walletList=new ArrayList<Wallet>();;
		
		 
		
		User user=userRepository.findById(id);
		if(user.getUserType().contains("Producer"))
		{
			
			List<Purchase> purchaseList= purchaseRepository.findByproducerId(id);
			
			for(Purchase p : purchaseList) {
				Wallet wallet = new Wallet();
				wallet.setUserType(user.getUserType());
				wallet.setSymbol("Earn");
				wallet.setCounterParty(p.getConsumerName());
				wallet.setId(id);
				wallet.setTransactionType("BuyAsset");
				wallet.setTransactionIdentifer(p.getAssetName());
				wallet.setTransactionDate(p.getPurchaseDate());
				wallet.setAmount(p.getPrice());
				walletList.add(wallet);
			}
				
			
		}
			
		
		else if(user.getUserType().contains("Consumer"))
		{
			List<Purchase> purchaseConsumerList=purchaseRepository.findByconsumerId(id);
			
			for(Purchase p : purchaseConsumerList) {
				Wallet wallet=new Wallet();
				System.out.println(user.getUserType());
				wallet.setUserType(user.getUserType());
				wallet.setSymbol("Pay");
				wallet.setCounterParty(p.getProducerName());
				wallet.setId(id);
				wallet.setTransactionType("BuyAsset");
				wallet.setTransactionIdentifer(p.getAssetName());
				wallet.setTransactionDate(p.getPurchaseDate());
				wallet.setAmount(p.getPrice());
				walletList.add(wallet);
			}
		}
		
		//wallet1.add(wallet);
		return walletList;
	}
	
	
	public Collection<Purchase> findByConsumerId(String consumerId){
		
		
		return  purchaseRepository.findByConsumerId(consumerId);
		
	}
	
}

