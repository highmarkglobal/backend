package tech.highmarkglobal.bcm.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.highmarkglobal.bcm.domain.Asset;
import tech.highmarkglobal.bcm.domain.BountyProgram;
import tech.highmarkglobal.bcm.domain.BountySearch;
import tech.highmarkglobal.bcm.domain.PricingTemplate;
import tech.highmarkglobal.bcm.domain.Transaction;
import tech.highmarkglobal.bcm.domain.TransactionType;
import tech.highmarkglobal.bcm.domain.UserDetails;
import tech.highmarkglobal.bcm.repo.BountyProgramRepository;

import tech.highmarkglobal.bcm.repo.UserDetailsRepository;
import tech.highmarkglobal.bcm.service.UserDetailsService;
import tech.highmarkglobal.bcm.util.HighmarkUtil;
import tech.highmarkglobal.bcm.web.dto.BountyProgramDTO;
import tech.highmarkglobal.bcm.web.transformer.DTOToDomainTransformer;
@Service
public class BountyProgramServiceImpl implements tech.highmarkglobal.bcm.service.BountyProgramService {
	
	private static final Logger logger=LoggerFactory.getLogger(BountyProgramServiceImpl.class);
	
	@Autowired
	BountyProgramRepository bountyProgramRepository;
	
	@Autowired
	UserDetailsRepository userDetailsRepository;

	
	@Autowired
	private UserDetailsService userDetailsService;

	@Override
	public BountyProgram saveBountyProgram(BountyProgramDTO bountyProgramDTO) {
		
		System.out.println("Inside saveBountyProgram()");
		String userId = bountyProgramDTO.getProducerId();
		System.out.println("User Id:"+userId);
		
		//ArrayList<UserDetails> ud = (ArrayList<UserDetails>) userDetailsRepository.findById(userId);
		//UserDetails userDetails = ud.get(0);
/*		if (bountyProgramDTO.getServiceChargs().compareTo(userDetails.getWallet()) > 0) {
			return null;
		}*/
		
		//Deducting the amount from the wallet
	//	userDetailsService.addMoneyToWallet(userId, (bountyProgramDTO.getServiceChargs() * -1));
				
		BountyProgram bountyProgram = DTOToDomainTransformer.transform(bountyProgramDTO);
		BountyProgram bounty = bountyProgramRepository.save(bountyProgram);
		logger.info(" bounty :{}",bounty);
		System.out.println("Bounty start date: "+bounty.getStartDate());
		System.out.println("Bounty end date: "+bounty.getEndDate());
		
		return bountyProgram;
		
		/*
		bountyProgramDTO=DTOToDomainTransformer.transform(bounty);
		
		System.out.println("Bounty start date: "+bountyProgramDTO.getStartDate());
		System.out.println("Bounty end date: "+bountyProgramDTO.getEndDate());
		
		//Creating transaction entry
		Transaction txn = new Transaction();
		txn.setUserId(userId);
		txn.setCurrency(bounty.getServiceChargs());
		txn.setTokens(bounty.getTokensOffered());
		txn.setTransactionDate(HighmarkUtil.StrtoDate(HighmarkUtil.getCurrentDate()));
		txn.setTransactionType(TransactionType.ASSET.toString());
		txnRepo.save(txn);

		return bountyProgramDTO;
		*/		
	}
	
	@Override
	public BountyProgramDTO findBounty(String name) {
		
		BountyProgram bounty = bountyProgramRepository.findByBountyName(name);
		BountyProgramDTO bountyProgramDTO = DTOToDomainTransformer.transform(bounty);
		return bountyProgramDTO;
	}
	/**
	 * 
	 */
	@Override
	public Collection<BountyProgramDTO> findBountys() {
		Collection<BountyProgramDTO> dtos=new ArrayList<BountyProgramDTO>();
		Collection<BountyProgram> list = bountyProgramRepository.findAll();
		list.forEach(bounty-> {
			BountyProgramDTO bountyProgramDTO = DTOToDomainTransformer.transform(bounty);
			dtos.add(bountyProgramDTO);
		});
		return dtos;
	}
	
	@Override
	public Collection<BountyProgramDTO> findBountiesByProductId(String productId) {
		Collection<BountyProgramDTO> dtos=new ArrayList<BountyProgramDTO>();
		Collection<BountyProgram> list = bountyProgramRepository.findByProductId(productId);
		
		list.forEach(bounty-> {
			BountyProgramDTO bountyProgramDTO = DTOToDomainTransformer.transform(bounty);
			dtos.add(bountyProgramDTO);
		});
		return dtos;
		
	}
	
	@Override
	public Collection<BountyProgramDTO> findBountiesByProducerId(String producerId) {
		Collection<BountyProgramDTO> dtos=new ArrayList<BountyProgramDTO>();
		Collection<BountyProgram> list = bountyProgramRepository.findByProducerId(producerId);
		
		list.forEach(bounty-> {
			BountyProgramDTO bountyProgramDTO = DTOToDomainTransformer.transform(bounty);
			dtos.add(bountyProgramDTO);
		});
		return dtos;
		
	}

	@Override
	public Collection<BountyProgram> searchBounties(BountySearch bountySearch) {
		System.out.println("Inside searchBounties");

		int nameFlag = 0;
		int dateFlag = 0;
		int bountyDomainFlag = 0;
		int tokensFlag = 0;
		int testingCategoryFlag = 0;
		
		
		//Collection<BountyProgram> bounties = new ArrayList<BountyProgram>();
		Collection<BountyProgram> nameFilterList = new ArrayList<BountyProgram>();
		Collection<BountyProgram> dateFilterList = new ArrayList<BountyProgram>();
		Collection<BountyProgram> bountyDomainFilterList = new ArrayList<BountyProgram>();
		Collection<BountyProgram> tokensFilterList = new ArrayList<BountyProgram>();
		Collection<BountyProgram> testCategoryFilterList = new ArrayList<BountyProgram>();
		
		
		Collection<BountyProgram> bountyList = bountyProgramRepository.findAll();
		
		System.out.println("Bounty list size:"+bountyList.size());
		
		if (bountySearch.getName() != null) {
			nameFlag = 1;
			bountyList.forEach(bounty-> {
				if(bounty.getBountyName().contains(bountySearch.getName())) {
					nameFilterList.add(bounty);
				}
			});
		}
		
		if (nameFlag == 1) {
			bountyList.retainAll(nameFilterList);
		}
		System.out.println("Bounty list size:"+bountyList.size());
		
		if (bountyList.size() > 0 && bountySearch.getBountyDomain() != null) {
			bountyDomainFlag = 1;
			for (BountyProgram bounty:bountyList){
				List<String> domainList = bounty.getTags();
				for (String tag : domainList) {
					if (bountySearch.getBountyDomain().contains(tag)) {
						bountyDomainFilterList.add(bounty);
						break;
					}
				}
			}
		}
		
		if (bountyDomainFlag == 1) {
			bountyList.retainAll(bountyDomainFilterList);
		}
		System.out.println("Bounty list size:"+bountyList.size());
		
		if (bountyList.size() > 0 && bountySearch.getStartDate() != null && bountySearch.getEndDate() != null) {
			dateFlag = 1;
			for (BountyProgram bounty:bountyList){
				/*System.out.println("Bounty start date: "+bounty.getStartDt());
				System.out.println("Bounty end date: "+bounty.getEndDate());
				System.out.println("BountySearch start date: "+bountySearch.getStartDate());
				System.out.println("BountySearch end date: "+bountySearch.getEndDate());*/
				if ((bounty.getStartDate().compareTo(bountySearch.getStartDate()) <= 0) &&
					(bounty.getEndDate().compareTo(bountySearch.getEndDate()) >= 0)) {
					dateFilterList.add(bounty);
				}
			}	
		}
		
		if (dateFlag == 1) {
			bountyList.retainAll(dateFilterList);
		}
		
		if (bountyList.size() > 0 && bountySearch.getMinToken() != null && bountySearch.getMaxToken() != null) {
			tokensFlag = 1;
			bountyList.forEach(bounty-> {
				if ((bounty.getTokensOffered().intValue() >= bountySearch.getMinToken().intValue()) && 
						(bounty.getTokensOffered().intValue() <= bountySearch.getMaxToken().intValue())) {
					tokensFilterList.add(bounty);
				}
			});
		}
		
		if (tokensFlag == 1) {
			bountyList.retainAll(tokensFilterList);
		}
		
		if(bountyList.size() > 0 && bountySearch.getTestingCategory() != null) {
			testingCategoryFlag = 1;
			for (BountyProgram bp : bountyList) {
				if(bountySearch.getTestingCategory().contains(bp.getTestingCategory())) {
					testCategoryFilterList.add(bp);
				}
			}
		}
		
		if (testingCategoryFlag == 1) {
			bountyList.retainAll(testCategoryFilterList);
		}
		
		System.out.println("Bounty list size:"+bountyList.size());
		return bountyList;
	}
	
	public BountyProgram getById(String id) {
		BountyProgramDTO bountyProgramDTO=new BountyProgramDTO();
		BountyProgram bountyProgram = bountyProgramRepository.findById(id);
		bountyProgramDTO = DTOToDomainTransformer.transform(bountyProgram);
		return bountyProgram;
		
	}
	
	@Override	
	public BountyProgram findBounty(String bountyName, String desc, String producerId, String assetId, String testingCategory) {
		return bountyProgramRepository.findByNameDescProducerProduct(bountyName, desc, producerId, assetId, testingCategory);
	}

}
