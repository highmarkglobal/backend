package tech.highmarkglobal.bcm.service.impl;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import io.swagger.annotations.ApiModel;
import tech.highmarkglobal.bcm.domain.Asset;
import tech.highmarkglobal.bcm.service.AssetService;
import tech.highmarkglobal.bcm.service.BlockchainAssetService;
import tech.highmarkglobal.blockchain.config.BlockchainConfig;;

@ApiModel
@Service
public class BlockchainAssetServiceImpl implements BlockchainAssetService {
	@Autowired
	AssetService assetService;

	private HttpStatus callChaincodeAPI(String uri, HashMap<String, String> requestPayload) {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("Content-Type", "application/json");

		headers.setAll(map);

		HttpEntity<?> request = new HttpEntity<>(requestPayload, headers);
		ResponseEntity<?> response = new RestTemplate().postForEntity(uri, request, String.class);

			return response.getStatusCode();
	}

	@Override
	public HttpStatus add(Asset asset) {
		
		HashMap<String, String> requestPayload = new HashMap<String, String>();
		requestPayload.put("assetId", asset.getId());
		requestPayload.put("assetName", asset.getName());
		requestPayload.put("assetVersion", "1.0"); // need to add version data to		
		requestPayload.put("license", asset.getLicensingTerms());
		requestPayload.put("licensingTerms", "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwYXVsLmh"); // need to add hash function
		requestPayload.put("price", "1000"); //asset.getPrice());
		requestPayload.put("pricingUnit", "USD"); //asset.getPricingUnit());
		
		String featureName = "";
		String featureType = "";
		String featureDescription = "";
		ArrayList<JSONObject> features = (ArrayList<JSONObject>) asset.getFeatures().get("list");		
		for(int i = 0; i < features.size(); i++) {
			HashMap<String, String> feature = (HashMap<String, String>) features.get(i);
			featureName += feature.get("featureName") + ", ";
			featureType += feature.get("featureType") + ", ";	
			featureDescription += feature.get("featureDescription");
		}			
		requestPayload.put("featureType", featureType);
		requestPayload.put("featureName", featureName);		
		requestPayload.put("featureDescription", featureDescription);
		
		requestPayload.put("producer", BlockchainConfig.CLASS_PRODUCER + "#" +assetService.getByAsset(asset.getName()).getProducerId());

//		requestPayload.put("assetId", assetService.getByName(assetDTO.getName()).getId());
//		requestPayload.put("assetName", assetDTO.getName());
//		requestPayload.put("assetVersion", "1.0"); // need to add version data to Asset DTO
//		requestPayload.put("license", "MIT");
//		requestPayload.put("licensingTerms", "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwYXVsLmh"); // need to add hash function
//		requestPayload.put("price", "1000");
//		requestPayload.put("pricingUnit", "USD");
//		requestPayload.put("featureType", "Functional");
//		requestPayload.put("featureName", "e-prescription");
//		requestPayload.put("featureDescription",
//				"It provide physicains with emailing a prescription to the patient and the pharmacy");
//		requestPayload.put("producer",
//				BlockchainConfig.CLASS_PRODUCER + "#" + assetService.getByName(assetDTO.getName()).getProducerId());

		return callChaincodeAPI(BlockchainConfig.URI_ASSET, requestPayload);
	}
}
