package tech.highmarkglobal.bcm.service;

import java.util.Collection;
import java.util.List;

import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Component;

import tech.highmarkglobal.bcm.domain.Validator;
import tech.highmarkglobal.bcm.domain.Wallet;
import tech.highmarkglobal.bcm.web.dto.ValidatorDTO;
@Component
public interface ValidatorService {
	
	//Asset getById(String id);
	
	List<Wallet> getById(String id)throws java.text.ParseException;
	
	Validator savevalidatorResults(ValidatorDTO validatorDTO) throws java.text.ParseException, ParseException;
	Validator savevalidatorResults(ValidatorDTO validatorDTO, byte[] validationreport,byte[] valbugsreport,byte[] valtestscript,byte[] valscriptlog) throws ParseException, java.text.ParseException;
	
//	List<Wallet> getById(String id);

}
