package tech.highmarkglobal.bcm.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Component;

import tech.highmarkglobal.bcm.domain.Asset;
import tech.highmarkglobal.bcm.domain.FeaturesDetails;
import tech.highmarkglobal.bcm.web.dto.AssetDTO;
import tech.highmarkglobal.bcm.web.dto.ProductDTO;

@Component
public interface AssetService {
	
 Asset saveAsset(AssetDTO assetDTO);
 
 /*List<Asset> findAll();*/
 
 Asset getById(String id);
 
 Collection<Asset> getByName(String name);
 
 Asset getByAsset(String asset);
 
 Collection<Asset> getByProducerId(String producerId);
 
 List<Asset> findAll();
 
FeaturesDetails getByAssetId(String assetId) throws ParseException;



 
 
 
 /*List<AssetType> getAssetTypeBySelectionCriteria(List<SelectionCriteria> criteriaList) ;*/
 
}
