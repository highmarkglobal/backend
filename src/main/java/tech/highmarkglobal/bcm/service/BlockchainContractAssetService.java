package tech.highmarkglobal.bcm.service;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import tech.highmarkglobal.bcm.domain.Purchase;
import tech.highmarkglobal.bcm.web.dto.PurchaseDTO;

@Component
public interface BlockchainContractAssetService {
	HttpStatus makeContract(Purchase purchase);
}