package tech.highmarkglobal.bcm.service;

import java.util.List;
import java.util.Optional;

import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;

import tech.highmarkglobal.bcm.domain.FileInfo;

public interface FileInfoService {
	public FileInfo save(byte[] file, String name);
	

	
	public GridFSDBFile getById(String id);

	
}