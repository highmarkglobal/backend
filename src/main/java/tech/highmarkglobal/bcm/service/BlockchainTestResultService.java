package tech.highmarkglobal.bcm.service;


import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import tech.highmarkglobal.bcm.domain.TestResult;

@Component
public interface BlockchainTestResultService {

	HttpStatus add(TestResult testResult);
	
}
