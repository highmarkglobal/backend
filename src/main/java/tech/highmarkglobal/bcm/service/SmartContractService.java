package tech.highmarkglobal.bcm.service;

import tech.highmarkglobal.bcm.web.dto.SmartContractDTO;

public interface SmartContractService {
	
	
	public SmartContractDTO saveSmartContract(SmartContractDTO smartContract);

}
