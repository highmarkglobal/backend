package tech.highmarkglobal.bcm.service;

import java.util.Optional;

import tech.highmarkglobal.bcm.domain.User;
import tech.highmarkglobal.bcm.domain.UserDetails;
import tech.highmarkglobal.bcm.web.dto.UserDTO;
import tech.highmarkglobal.bcm.web.dto.UserDetailsDTO;

public interface UserService {

	boolean isEmailExits(String email);

	UserDetails save(UserDetailsDTO signUpDTO);

	 User getUserById(String id);
	
	User getUserByEmail(String email);
	
 UserDetails getUserDetails(String userId);

}
