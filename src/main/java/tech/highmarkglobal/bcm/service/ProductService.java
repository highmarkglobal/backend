package tech.highmarkglobal.bcm.service;


import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Component;

import tech.highmarkglobal.bcm.domain.Product;
import tech.highmarkglobal.bcm.domain.ProductSearch;
import tech.highmarkglobal.bcm.web.dto.PricingTemplateDTO;
import tech.highmarkglobal.bcm.web.dto.ProductDTO;

/**
 * 
 * @author ramesh
 *
 */
@Component
public interface ProductService {
	
	
	public ProductDTO saveProduct(ProductDTO productDTO);
	
	Collection<ProductDTO> findAll();

	public Product findById(String id);

	public Product updateProduct(ProductDTO productDTO);

	public void deleteAsset(String assetname);

	//public List<PricingTemplateDTO> getPricingPolicies(String name ,String id);
	
	public Collection<ProductDTO> findProductsByProducerId(String producerId);

	//public Collection<Product> findProductsByPriceRange(Double start, Double end);
	
	public Collection<Product> findProductsByProductDomain(String prodDomain);
	
	public Collection<Product> searchAsset(ProductSearch ps);
}
