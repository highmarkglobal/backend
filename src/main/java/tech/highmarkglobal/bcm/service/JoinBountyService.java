package tech.highmarkglobal.bcm.service;

import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Component;

import tech.highmarkglobal.bcm.domain.JoinBounty;
import tech.highmarkglobal.bcm.web.dto.JoinBountyDTO;


@Component
public interface JoinBountyService {
	
 JoinBounty saveJoinBounty(JoinBountyDTO joinBountyDTO);
 
 JoinBounty getByBountyID(String bountyID);
 
 List<JoinBounty> findAll();
 
 Collection<JoinBounty> getByTesterID(String testerID);
 
 
 
 
 
 
}