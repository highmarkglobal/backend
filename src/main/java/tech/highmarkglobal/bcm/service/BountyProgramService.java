package tech.highmarkglobal.bcm.service;

import java.util.Collection;
import java.util.List;

import tech.highmarkglobal.bcm.domain.BountyProgram;
import tech.highmarkglobal.bcm.domain.BountySearch;
import tech.highmarkglobal.bcm.web.dto.BountyProgramDTO;
import tech.highmarkglobal.bcm.web.rest.controller.BountyProgramController;

public interface BountyProgramService {
	
	public BountyProgram saveBountyProgram(BountyProgramDTO bountyProgramDTO);

	public BountyProgramDTO findBounty(String name);

	public Collection<BountyProgramDTO> findBountys();
	
	public Collection<BountyProgramDTO> findBountiesByProductId(String productId);
	
	public Collection<BountyProgramDTO> findBountiesByProducerId(String producerId);
	
	public Collection<BountyProgram> searchBounties(BountySearch bountySearch);
	
	public BountyProgram getById(String id);
	
	public BountyProgram findBounty(String bountyName, String desc, String producerId, String assetId, String testingCategory);
}
