package tech.highmarkglobal.bcm.service;

import java.util.List;

import org.springframework.stereotype.Component;

import tech.highmarkglobal.bcm.domain.ProductDomain;
import tech.highmarkglobal.bcm.web.dto.ProductDomainDTO;

@Component
public interface DomainService {
	
	
	public void saveDomain(ProductDomainDTO productDomainDTO);
	
	public void saveDomains(List<ProductDomainDTO> productDomainDTOs);

	public List<ProductDomain> findAll();
	
	public ProductDomainDTO findByDomainId(String domainId);
	
	public List<ProductDomainDTO> findByDomainName(String domainName);
	

}
