package tech.highmarkglobal.bcm.service;

import java.util.List;

import org.springframework.stereotype.Component;

import tech.highmarkglobal.bcm.domain.AssetType;
import tech.highmarkglobal.bcm.web.dto.AssetTypeDTO;

@Component
public interface AssetTypeService {
	
 void saveAssetType(AssetTypeDTO assetTypeDTO);
 
 List<AssetType> findAll();
 
 AssetTypeDTO getById(String id);
}
