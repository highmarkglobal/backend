package tech.highmarkglobal.bcm.service;

import org.springframework.stereotype.Component;

import tech.highmarkglobal.bcm.domain.UserDetails;
import tech.highmarkglobal.bcm.web.dto.UserDetailsDTO;

public interface UserDetailsService {
	
	public UserDetailsDTO saveUserDetails(UserDetailsDTO UserDetailsDTO);
	
	//public UserDetails addMoneyToWallet(String userId, Double amount);
	
	public UserDetails getUserDetails(String userId);

}
