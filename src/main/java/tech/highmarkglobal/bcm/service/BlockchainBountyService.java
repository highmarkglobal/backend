package tech.highmarkglobal.bcm.service;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import tech.highmarkglobal.bcm.domain.BountyProgram;
import tech.highmarkglobal.bcm.web.dto.BountyProgramDTO;

@Component
public interface BlockchainBountyService {

	HttpStatus add(BountyProgram bountyProgram);	
}
