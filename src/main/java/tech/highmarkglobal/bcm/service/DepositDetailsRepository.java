package tech.highmarkglobal.bcm.service;

import org.springframework.data.mongodb.repository.MongoRepository;

import tech.highmarkglobal.bcm.domain.DepositDetails;

public interface DepositDetailsRepository extends MongoRepository<DepositDetails,String> {

}
