package tech.highmarkglobal.bcm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import tech.highmarkglobal.bcm.domain.Asset;
import tech.highmarkglobal.bcm.web.dto.AssetDTO;

@Component
public interface BlockchainAssetService {
				
	HttpStatus add(Asset asset);
	//HttpStatus buy(AssetDTO assetDTO);
}
