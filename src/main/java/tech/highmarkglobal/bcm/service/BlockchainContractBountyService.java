package tech.highmarkglobal.bcm.service;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import tech.highmarkglobal.bcm.domain.JoinBounty;

@Component
public interface BlockchainContractBountyService {
	HttpStatus join(JoinBounty joinBounty);
}
