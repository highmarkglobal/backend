package tech.highmarkglobal.bcm.service;

import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Component;

import tech.highmarkglobal.bcm.domain.AssetType;

import tech.highmarkglobal.bcm.domain.Purchase;
import tech.highmarkglobal.bcm.domain.Wallet;
import tech.highmarkglobal.bcm.web.dto.PurchaseDTO;

@Component
public interface PurchaseService {
	
 Purchase savePurchase(PurchaseDTO purchaseDTO);
 
 List<Purchase> findAll();
 
 /*List<AssetType> getAssetTypeBySelectionCriteria(List<SelectionCriteria> criteriaList) ;*/
 
 List<Wallet> getById(String id);
 
 Collection<Purchase> findByConsumerId(String consumerId);
 
}

