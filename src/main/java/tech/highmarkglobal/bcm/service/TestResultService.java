package tech.highmarkglobal.bcm.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.mongodb.gridfs.GridFSDBFile;

import tech.highmarkglobal.bcm.domain.TestResult;
import tech.highmarkglobal.bcm.web.dto.TestResultDTO;

@Component
public interface TestResultService {
	
	 TestResult saveTestResult(TestResultDTO testResultDTO,byte[] reportfile,byte[] testdatafile,byte[] scriptlogfile);
	 
	 
	 List<TestResult> findAll();


	//GridFSDBFile getTestReport(String testerId) throws Exception;
	
	//Optional<TestResult> findByIdForTestReport(String id);


	GridFSDBFile getTestReportById(String id); 


}
