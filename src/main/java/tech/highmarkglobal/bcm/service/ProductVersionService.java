package tech.highmarkglobal.bcm.service;

import org.springframework.stereotype.Component;

import tech.highmarkglobal.bcm.domain.ProductVersion;
import tech.highmarkglobal.bcm.web.dto.ProductVersionDTO;

@Component
public interface ProductVersionService {

	ProductVersion saveProductsVersion(ProductVersionDTO productsversion);

}
