package tech.highmarkglobal.bcm.service;

import java.util.Collection;

import tech.highmarkglobal.bcm.domain.Transaction;


public interface TransactionService {

	public Transaction saveTransaction(Transaction txn);
	
	public Collection<Transaction> findTransactionByUserId(String userId);

}
