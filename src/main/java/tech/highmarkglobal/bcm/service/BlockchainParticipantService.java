package tech.highmarkglobal.bcm.service;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import tech.highmarkglobal.bcm.domain.UserDetails;

@Component
public interface BlockchainParticipantService {
	HttpStatus add(UserDetails userDetails);	
}
