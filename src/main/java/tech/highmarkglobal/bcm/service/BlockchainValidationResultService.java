package tech.highmarkglobal.bcm.service;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import tech.highmarkglobal.bcm.domain.Validator;

@Component
public interface BlockchainValidationResultService {
	
	HttpStatus add(Validator validator);

}
