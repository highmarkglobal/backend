package tech.highmarkglobal.bcm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * The main spring boot application which will start up a web container and wire
 * up all the required beans.
 * 
 * @author Ramesh
 *
 */
@SpringBootApplication
@EnableMongoAuditing
public class HighmarkGloblaApplication {

	public static void main(String[] args) {
		SpringApplication.run(HighmarkGloblaApplication.class, args);
	}
	
	
	 @Bean
	    public WebMvcConfigurer corsConfigurer() {
	        return new WebMvcConfigurerAdapter() {
	            @Override
	            public void addCorsMappings(CorsRegistry registry) {
	                registry.addMapping("/**");
	            }
	        };
	    }
}
