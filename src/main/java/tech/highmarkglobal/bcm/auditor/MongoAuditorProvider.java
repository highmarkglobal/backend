package tech.highmarkglobal.bcm.auditor;
/**
 * Implementation of AuditorAware interface 
 * This class provides implementation for getCurrentAuditor which will be used to populate
 * fields annotated with @createdby @lastmodifiedby in Highmark domain classes
 * @author - Ramesh 
 */
import org.springframework.data.domain.AuditorAware;


public class MongoAuditorProvider<T> implements AuditorAware<String> {
 
	/**
	 * provide implementation for getCurrentAuditor to populate fields annotated with 
	 * createdBy and lastUpdatedBy
	 * Retrieve the username from Spring security context if user account is authenticated
	 * Returns system-generated as default value
	 */
	@Override
	public String getCurrentAuditor() {
	/*	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.isAuthenticated()){
			User usr = (User) auth.getPrincipal();
			return usr.getUsername();
		}		*/
		return "system-generated";
	}

}
