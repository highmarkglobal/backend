package tech.highmarkglobal.bcm.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import tech.highmarkglobal.bcm.domain.AssetType;

public interface AssetTypesRepository extends MongoRepository<AssetType, String> {

	AssetType findByName(String assetType);
	
	AssetType findById(String id);

}
