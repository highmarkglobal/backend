package tech.highmarkglobal.bcm.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import tech.highmarkglobal.bcm.domain.User;
import tech.highmarkglobal.bcm.domain.UserDetails;

/**
 * @author arun
 */
@Repository
public interface UserRepository extends MongoRepository<User, Long> {
	
	User findById(String Id);
	
    Optional<User> findByEmail(String email);

    List<User> findByIdIn(List<Long> userIds);

    Boolean existsByEmail(String email);
    
    //User findByUserId(String userId);
}
