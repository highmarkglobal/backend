package tech.highmarkglobal.bcm.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import tech.highmarkglobal.bcm.domain.ProductDomain;


public interface ProductDomainRepository extends MongoRepository<ProductDomain, String> {

	public List<ProductDomain> findByDomainName(String name);
	
	ProductDomain findById(String id);

}
