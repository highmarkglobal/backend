package tech.highmarkglobal.bcm.repo;

import java.util.Collection;

import org.springframework.data.mongodb.repository.MongoRepository;

import tech.highmarkglobal.bcm.domain.JoinBounty;

public interface JoinBountyRepository extends MongoRepository<JoinBounty , String>{
	
	JoinBounty findByBountyID(String bountyID);
	
	//JoinBounty findByTesterID(String testerID);

	Collection<JoinBounty> findByTesterID(String testerID);
	
	JoinBounty findByTesterName(String testerName);

}
