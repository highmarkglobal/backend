package tech.highmarkglobal.bcm.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import tech.highmarkglobal.bcm.domain.AddressDetails;

public interface AddressDetailsRepository extends MongoRepository<AddressDetails, String> {

}
