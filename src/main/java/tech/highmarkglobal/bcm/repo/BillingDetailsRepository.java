package tech.highmarkglobal.bcm.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import tech.highmarkglobal.bcm.domain.BillingDetails;

public interface BillingDetailsRepository extends MongoRepository<BillingDetails, String> {

}
