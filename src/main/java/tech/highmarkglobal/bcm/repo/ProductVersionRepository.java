package tech.highmarkglobal.bcm.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import tech.highmarkglobal.bcm.domain.ProductVersion;

public interface ProductVersionRepository extends MongoRepository<ProductVersion, String> {

	ProductVersion findByProductId(String productId);

	ProductVersion findByProductVersion(float productVersionId);

}
