package tech.highmarkglobal.bcm.repo;

import java.util.Collection;
import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.data.mongodb.repository.MongoRepository;

import tech.highmarkglobal.bcm.domain.Asset;
import tech.highmarkglobal.bcm.domain.Product;

public interface AssetRepository extends MongoRepository<Asset, String> {

	Asset findByName(String name);
	
	Asset findById(String id);
	
	Asset findByAssetType(String asset);
	
	Collection<Asset> findByProducerId(String producerId);
	
	Asset findByfeatures(JSONObject features);

}
