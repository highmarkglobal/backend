package tech.highmarkglobal.bcm.repo;

import java.util.Collection;

import org.springframework.data.mongodb.repository.MongoRepository;

import tech.highmarkglobal.bcm.domain.AssetType;
import tech.highmarkglobal.bcm.domain.Transaction;

public interface TransactionRepository extends MongoRepository<Transaction, String> {

	Collection<Transaction> findByUserId(String userId);
}
