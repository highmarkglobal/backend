package tech.highmarkglobal.bcm.repo;

import java.util.Collection;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import tech.highmarkglobal.bcm.domain.BountyProgram;
import tech.highmarkglobal.bcm.web.dto.BountyProgramDTO;

public interface BountyProgramRepository extends MongoRepository<BountyProgram, String> {

	BountyProgram findByBountyName(String name);
    Collection<BountyProgram> findByProductId(String productId);
    Collection<BountyProgram> findByProducerId(String producerId);
    BountyProgram findById(String id);
    
    List<BountyProgram> findAll();
    
    @Query(value = "{ 'bountyName': ?0, 'bountyDesc': ?1, 'producerId': ?2, 'assetId': ?3, 'testingCategory': ?4}" )
    BountyProgram findByNameDescProducerProduct(String bountyName, String desc, String producerId, String assetId, String testingCategory);
}
