package tech.highmarkglobal.bcm.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import tech.highmarkglobal.bcm.domain.SmartContract;

public interface SmartContractRepository extends MongoRepository<SmartContract, String> {
	List<SmartContract> findByName(String name);
	
	SmartContract findById(String id);
}
