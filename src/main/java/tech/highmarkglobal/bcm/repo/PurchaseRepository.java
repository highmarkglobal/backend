package tech.highmarkglobal.bcm.repo;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import tech.highmarkglobal.bcm.domain.Purchase;

public interface PurchaseRepository extends MongoRepository<Purchase, String> {
 
	@Query(value = "{ 'producerId': ?0, 'consumerId': ?1, 'assetId': ?2, 'purchaseDate': ?3}" )
	Purchase findByProducerConsumerAssetDate(String producer, String consumerId, String assetId, Date purchaseDate);
	
	List<Purchase> findByproducerId(String producerId);

	Collection<Purchase> findByConsumerId(String consumerId);

	List<Purchase> findByconsumerId(String consumerid);
	
	

}
