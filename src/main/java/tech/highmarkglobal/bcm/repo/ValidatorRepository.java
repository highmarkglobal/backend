package tech.highmarkglobal.bcm.repo;

import java.util.Collection;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;


import tech.highmarkglobal.bcm.domain.Validator;

public interface ValidatorRepository extends MongoRepository<Validator,String>{
	
	
	Validator findByRewards(String rewards);

	List<Validator> findByTesterID(String testerID);

	List<Validator> findByProducerId(String producerId);
	
	Validator findByAssetName(String assetname);
}
