package tech.highmarkglobal.bcm.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import tech.highmarkglobal.bcm.domain.Product;

public interface ProductsRepository extends MongoRepository<Product, String> {

	Product findByName(String name);

	Product findById(String productId);
	
	@Query(value = "{ 'name' : 0 }")
    List<Product> findProductsByName(String name);
	
	List<Product> findProductsByProducerId(String producerId);


}
