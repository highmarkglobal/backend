package tech.highmarkglobal.bcm.repo;

import java.util.Collection;

import org.springframework.data.mongodb.repository.MongoRepository;

import tech.highmarkglobal.bcm.domain.UserDetails;

public interface UserDetailsRepository extends MongoRepository<UserDetails, String> {

	 Collection<UserDetails> findById(String userId);
	 
	 UserDetails findByContactEmail(String emailId);
}
