package tech.highmarkglobal.bcm.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import tech.highmarkglobal.bcm.domain.PricingTemplate;

public interface PricingTemplateRepository extends MongoRepository<PricingTemplate, String> {

}
