package tech.highmarkglobal.bcm.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import tech.highmarkglobal.bcm.domain.TestResult;

public interface TestResultRepository extends MongoRepository<TestResult, String> {

	TestResult findByBountyName(String bountyname);

	TestResult findByAssetName(String assetnamwe);
}
