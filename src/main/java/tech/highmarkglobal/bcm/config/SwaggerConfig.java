package tech.highmarkglobal.bcm.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean

    public Docket userApi() {

        return new Docket(DocumentationType.SWAGGER_2).securitySchemes(Arrays.asList(apiKey()))
                .select()
                .apis(RequestHandlerSelectors.basePackage("tech.highmarkglobal.bcm"))
                //.paths(regex("/user.*,/keycloak.*"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(metaData());
    }
    
    private ApiKey apiKey() {
        return new ApiKey("Authorization", "Bearer jwtToken", "header");
     }
    
    @SuppressWarnings("unused")
	private SecurityContext securityContext() {
		return SecurityContext.builder()
				.securityReferences(defaultAuth())
				.forPaths(PathSelectors.regex("/api/v1.*"))
				.build();
}
    
    
    private List<SecurityReference> defaultAuth() {
    	AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
    	AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
    	authorizationScopes[0] = authorizationScope;
    	return (List<SecurityReference>) Arrays.asList(new SecurityReference("apiKey", authorizationScopes));
    }
    
    
  /*  private Predicate<String> categoryPaths() {
        return or(regex("/category.*"), regex("/category"), regex("/categories"));
}*/

    private ApiInfo metaData() {

        ApiInfo apiInfo = new ApiInfo(

                "Highmark Global REST API",

                "Highmark Global REST API for Online Store",

                "1.0",

                "Terms of service",

                new Contact("Highmark Global team", "https://www.above-inc.com/", "admin@"),

               "Apache License Version 2.0",

                "https://www.apache.org/licenses/LICENSE-2.0");

        return apiInfo;

    }
    
    

}