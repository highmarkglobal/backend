package tech.highmarkglobal.bcm.config;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import tech.highmarkglobal.bcm.domain.User;
import tech.highmarkglobal.bcm.security.UserPrincipal;

@Component
public class MongoAuditorProvider<T> implements AuditorAware<String> {
	 
	   /**
	    * provide implementation for getCurrentAuditor to populate fields annotated with 
	    * createdBy and lastUpdatedBy
	    * Retrieve the username from Spring security context if user account is authenticated
	    * Returns system-generated as default value
	    */
	   @Override
	   public String getCurrentAuditor() {
	      Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	      if(auth.isAuthenticated()){
	    	 UserPrincipal usr = (UserPrincipal) auth.getPrincipal();
	         return usr.getId();
	      }     
	      return "system-generated";
	   }

	}