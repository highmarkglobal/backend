package tech.highmarkglobal.bcm.exception;

/**
 * This class define the common exception class that will be used within the
 * system.
 * 
 * @author Ramesh
 *
 */
public class HighmarkGlobalException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4269316494280751012L;

	public HighmarkGlobalException(final String message) {
		super(message);
	}

	public HighmarkGlobalException(final String message, final Throwable throwable) {
		super(message, throwable);
	}
}
