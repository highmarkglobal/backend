package tech.highmarkglobal.bcm.domain;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;

public class FeaturesDetails {
	
	private String assetId;
	
	private String assetName;
	
	private String bountyName;
	
	//List<String> features = new ArrayList<String>();
	
	private JSONObject Feature_tester;
	



	
	private String validatorResult;

	public String getAssetId() {
		return assetId;
	}

	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}

	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	public String getBountyName() {
		return bountyName;
	}

	public void setBountyName(String bountyName) {
		this.bountyName = bountyName;
	}





	public JSONObject getFeature_tester() {
		return Feature_tester;
	}

	public void setFeature_tester(JSONObject feature_tester) {
		Feature_tester = feature_tester;
	}

	public String getValidatorResult() {
		return validatorResult;
	}

	public void setValidatorResult(String validatorResult) {
		this.validatorResult = validatorResult;
	}

	
	
}
