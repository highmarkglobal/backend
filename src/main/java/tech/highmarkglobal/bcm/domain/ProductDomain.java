package tech.highmarkglobal.bcm.domain;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import io.swagger.annotations.ApiModel;

@ApiModel
@Document(collection = "productdomains")
public class ProductDomain {

	
	@Id
	private String id;
	
	private String domainName;
	
	private String parentId;
	
	//private DomainAttributes domainAttr;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/*public DomainAttributes getDomainAttr() {
		return domainAttr;
	}

	public void setDomainAttr(DomainAttributes domainAttr) {
		this.domainAttr = domainAttr;
	}*/
	

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	@Override
	public String toString() {
		return "ProductDomain [id=" + id + ", domainName=" + domainName + "]";
	}
	
	
}
