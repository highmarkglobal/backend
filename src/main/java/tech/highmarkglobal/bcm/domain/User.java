package tech.highmarkglobal.bcm.domain;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Size;

import org.bson.types.ObjectId;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author arun
 */


@Document(collection = "user")
public class User {
    
	@Id
	private String id;

/*    @Size(max = 40)
    private String name;*/

    @NotBlank
    @Size(max = 40)
    @Email
    private String email;

    @NotBlank
    @Size(max = 100)
    private String password;
    
    private String userType;
    
    private String userDetailsId;
   
    private Set<String> roles = new HashSet<>();

    private String name;
    
    private String corporationName;
    

    
    
/*
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }*/

    public String getCorporationName() {
		return corporationName;
	}

	public void setCorporationName(String corporationName) {
		this.corporationName = corporationName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getUserDetailsId() {
		return userDetailsId;
	}

	public void setUserDetailsId(String userDetailsId) {
		this.userDetailsId = userDetailsId;
	}




}