package tech.highmarkglobal.bcm.domain;

public class DepositDetails {
	
	String bankLocation;
	
	String accHolderName;
	
	String bankAccountNumber;
	
	String routingNumber;
	
	public DepositDetails() {
		// TODO Auto-generated constructor stub
	}
	
	public DepositDetails(String loc, String holderName, String accNum, String routingNum) {
		
		this.bankLocation = loc;
		this.accHolderName = holderName;
		this.bankAccountNumber = accNum;
		this.routingNumber = routingNum;
	}

	public String getBankLocation() {
		return bankLocation;
	}

	public void setBankLocation(String bankLocation) {
		this.bankLocation = bankLocation;
	}

	public String getAccHolderName() {
		return accHolderName;
	}

	public void setAccHolderName(String accHolderName) {
		this.accHolderName = accHolderName;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getRoutingNumber() {
		return routingNumber;
	}

	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}
	
}
