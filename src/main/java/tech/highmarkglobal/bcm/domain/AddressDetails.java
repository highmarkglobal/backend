package tech.highmarkglobal.bcm.domain;

public class AddressDetails {
	
	String address;
	
	String website;
	
	String phoneNumber;
	
	public AddressDetails() {
		
	}
	
	public AddressDetails(String addr, String web, String ph) {
		this.address = addr;
		this.website = web;
		this.phoneNumber = ph;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	

}
