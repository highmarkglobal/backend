package tech.highmarkglobal.bcm.domain;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class BillingDetails {
	
	String cardNumber;
	
	String cardHolderName;
	@DateTimeFormat(iso = ISO.DATE)
	Date validity;
	
	public BillingDetails() {
		
	}
	
	public BillingDetails(String cardNum, String cardHoldName) {
		this.cardNumber = cardNum;
		this.cardHolderName = cardHoldName;
		this.setValidity(new Date());
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public Date getValidity() {
		return validity;
	}

	public void setValidity(Date validity) {
		this.validity = validity;
	}
	
	
}
