package tech.highmarkglobal.bcm.domain;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "userdetails")
public class UserDetails {
	
	@Id
	private String id;
	
	private String entityType;

	private String corporationName;

	private String contactFirstName;

	private String contactLastName;
	
	@NotBlank
	@Size(max = 40)
	@Email
	private String contactEmail;
	
	@NotBlank
	@Size(max = 100)
	private String password;

	private String contactPhoneNumber;

	private String address1;

	private String addres2;

	private String country;

	private String city;

	private String province;

	private String state;

	private String postalCode;

	private String corporationNumber;

	private String registrationNumber;

	private String taxNumber;

	private String yearlyTurnover;

	private String numberOfEmployees;
	
	private String userType;
	

	public UserDetails() {

	    }
	 

	public UserDetails(String entityType, String corporationName, String contactFirstName, String contactLastName,
			String contactEmail, String password, String contactPhoneNumber, String address1, String addres2,
			String country, String city, String province, String state, String postalCode, String corporationNumber,
			String registrationNumber, String taxNumber, String yearlyTurnover, String numberOfEmployees,
			String userType) {
		
		this.entityType = entityType;
		this.corporationName = corporationName;
		this.contactFirstName = contactFirstName;
		this.contactLastName = contactLastName;
		this.contactEmail = contactEmail;
		this.password = password;
		this.contactPhoneNumber = contactPhoneNumber;
		this.address1 = address1;
		this.addres2 = addres2;
		this.country = country;
		this.city = city;
		this.province = province;
		this.state = state;
		this.postalCode = postalCode;
		this.corporationNumber = corporationNumber;
		this.registrationNumber = registrationNumber;
		this.taxNumber = taxNumber;
		this.yearlyTurnover = yearlyTurnover;
		this.numberOfEmployees = numberOfEmployees;
		this.userType = userType;
	}








	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public String getCorporationName() {
		return corporationName;
	}

	public void setCorporationName(String corporationName) {
		this.corporationName = corporationName;
	}

	public String getContactFirstName() {
		return contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getContactPhoneNumber() {
		return contactPhoneNumber;
	}

	public void setContactPhoneNumber(String contactPhoneNumber) {
		this.contactPhoneNumber = contactPhoneNumber;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddres2() {
		return addres2;
	}

	public void setAddres2(String addres2) {
		this.addres2 = addres2;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCorporationNumber() {
		return corporationNumber;
	}

	public void setCorporationNumber(String corporationNumber) {
		this.corporationNumber = corporationNumber;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getTaxNumber() {
		return taxNumber;
	}

	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}

	public String getYearlyTurnover() {
		return yearlyTurnover;
	}

	public void setYearlyTurnover(String yearlyTurnover) {
		this.yearlyTurnover = yearlyTurnover;
	}

	public String getNumberOfEmployees() {
		return numberOfEmployees;
	}

	public void setNumberOfEmployees(String numberOfEmployees) {
		this.numberOfEmployees = numberOfEmployees;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}



  
    
}
