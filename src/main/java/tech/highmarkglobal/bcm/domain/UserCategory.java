package tech.highmarkglobal.bcm.domain;

public enum UserCategory {
	INDIVIDUAL, BUSINESS, INSTITUTION
}
