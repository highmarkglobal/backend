package tech.highmarkglobal.bcm.domain;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import org.json.simple.JSONObject;

import io.swagger.annotations.ApiModel;

@ApiModel
@Document(collection = "purchase")
public class Purchase {
	
	@Id
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	private String producerId;
	
	private String producerName;
	
	private String consumerId;
	
	private String consumerName;
	
	private String assetId;
	
	private String assetName;
	
	@DateTimeFormat(iso = ISO.DATE)
	private Date purchaseDate;
	
	private Integer price;
	
	private String priceUnit;
	
	private String licensing;
	
	private String pricingOption;
	
	private JSONObject features;
	
	
	
	
	
	
	
	public String getPricingOption() {
		return pricingOption;
	}

	public void setPricingOption(String pricingOption) {
		this.pricingOption = pricingOption;
	}

	public JSONObject getFeatures() {
		return features;
	}

	public void setFeatures(JSONObject features) {
		this.features = features;
	}

	public String getProducerId() {
		return producerId;
	}

	public void setProducerId(String producerId) {
		this.producerId = producerId;
	}

	public String getProducerName() {
		return producerName;
	}

	public void setProducerName(String producerName) {
		this.producerName = producerName;
	}

	public String getConsumerId() {
		return consumerId;
	}

	public void setConsumerId(String consumerId) {
		this.consumerId = consumerId;
	}

	public String getConsumerName() {
		return consumerName;
	}

	public void setConsumerName(String consumerName) {
		this.consumerName = consumerName;
	}

	public String getAssetId() {
		return assetId;
	}

	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}

	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public String getPriceUnit() {
		return priceUnit;
	}

	public void setPriceUnit(String priceUnit) {
		this.priceUnit = priceUnit;
	}

	public String getLicensing() {
		return licensing;
	}

	public void setLicensing(String licensing) {
		this.licensing = licensing;
	}

	@Override
	public String toString() {
		return "Purchase [ producerId=" + producerId + ", producerName=" + producerName + ",consumerId=" + consumerId + ",consumerName=" + consumerName + ",assetId=" + assetId + ",assetName=" + assetName + ",purchaseDate=" + purchaseDate + ",price=" + price + ", priceUnit=" + priceUnit + ",licensing=" + licensing + "]";
	}
}
