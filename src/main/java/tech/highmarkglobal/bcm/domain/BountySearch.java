package tech.highmarkglobal.bcm.domain;

import java.util.Date;
import java.util.List;

public class BountySearch {
	
	String name;
	Date startDate;
	Date endDate;
	List<String> bountyDomain;
	//List<String> productDomain;
	List<String> assetType;
	Integer minToken;
	Integer maxToken;
	List<String> testingCategory;
	Integer currentApplicants;
		
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public List<String> getBountyDomain() {
		return bountyDomain;
	}
	public void setBountyDomain(List<String> bountyDomain) {
		this.bountyDomain = bountyDomain;
	}
	/*public List<String> getProductDomain() {
		return productDomain;
	}
	public void setProductDomain(List<String> productDomain) {
		this.productDomain = productDomain;
	}*/
	public List<String> getAssetType() {
		return assetType;
	}
	public void setAssetType(List<String> assetType) {
		this.assetType = assetType;
	}
	public Integer getMinToken() {
		return minToken;
	}
	public void setMinToken(Integer minToken) {
		this.minToken = minToken;
	}
	public Integer getMaxToken() {
		return maxToken;
	}
	public void setMaxToken(Integer maxToken) {
		this.maxToken = maxToken;
	}
	public List<String> getTestingCategory() {
		return testingCategory;
	}
	public void setTestingCategory(List<String> testingCategory) {
		this.testingCategory = testingCategory;
	}
	public Integer getCurrentApplicants() {
		return currentApplicants;
	}
	public void setCurrentApplicants(Integer currentApplicants) {
		this.currentApplicants = currentApplicants;
	}	
}
