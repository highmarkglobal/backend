package tech.highmarkglobal.bcm.domain;


import java.util.List;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author ramesh
 *
 */
@Document(collection = "products")
public class Product {
	
	@Id
	private String id;
	
	
	String name;


	String stage;
	
	@DBRef
	private AssetType assetType;
	
	private Double serviceCharge;
	
	@DBRef
	private List<ProductDomain> productDomain;
	
	@CreatedBy
	private String producerId;
	
	private List<PricingTemplate> pricingTemplate;
	

   
	public Product() {
		super();
		}



	/**
	 * 
	 * @param name
	 * @param stage
	 */

	
	public Product(String name, String stage) {
		super();
		this.name = name;
		this.stage = stage;
	}

	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}



	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", stage=" + stage + ", assetType=" + assetType
				+ ", serviceCharge=" + serviceCharge + ", productDomain=" + productDomain + "]";
	}



	public Double getServiceCharge() {
		return serviceCharge;
	}



	public void setServiceCharge(Double serviceCharge) {
		this.serviceCharge = serviceCharge;
	}



	public List<ProductDomain> getProductDomain() {
		return productDomain;
	}



	public void setProductDomain(List<ProductDomain> productDomain) {
		this.productDomain = productDomain;
	}



	public AssetType getAssetType() {
		return assetType;
	}



	public void setAssetType(AssetType assetType) {
		this.assetType = assetType;
	}



	public String getProducerId() {
		return producerId;
	}



	public void setProducerId(String producerId) {
		this.producerId = producerId;
	}



	public List<PricingTemplate> getPricingTemplate() {
		return pricingTemplate;
	}



	public void setPricingTemplate(List<PricingTemplate> pricingTemplate) {
		this.pricingTemplate = pricingTemplate;
	}
	
	
}

