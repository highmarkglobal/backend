package tech.highmarkglobal.bcm.domain;
import org.json.simple.JSONObject;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import io.swagger.annotations.ApiModel;
@ApiModel
@Document(collection = "smartcontracts")
public class SmartContract {
	
	@Id
    private String id;
	
	private String name;
	
	private String secondParty;
	
	private JSONObject jsonObject;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecondParty() {
		return secondParty;
	}

	public void setSecondParty(String secondParty) {
		this.secondParty = secondParty;
	}

	public JSONObject getJsonObject() {
		return jsonObject;
	}

	public void setJsonObject(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}

	@Override
	public String toString() {
		return "SmartContract [id=" + id + ", name=" + name + ", secondParty=" + secondParty + ", jsonObject="
				+ jsonObject + "]";
	}
	
	
}
