package tech.highmarkglobal.bcm.domain;

import java.util.List;

public class ProductSearch {
	String name;
	List<String> assetType;
	List<String> productDomain;
	Double minPrice;
	Double maxPrice;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getAssetType() {
		return assetType;
	}
	public void setAssetType(List<String> assetType) {
		this.assetType = assetType;
	}
	public List<String> getProductDomain() {
		return productDomain;
	}
	public void setProductDomain(List<String> productDomain) {
		this.productDomain = productDomain;
	}
	public Double getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(Double minPrice) {
		this.minPrice = minPrice;
	}
	public Double getMaxPrice() {
		return maxPrice;
	}
	public void setMaxPrice(Double maxPrice) {
		this.maxPrice = maxPrice;
	}
	
	
}
