package tech.highmarkglobal.bcm.domain;



import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "productsVersion")
public class ProductVersion {
	
	@Id
	private String id;
	
	String productId;
	
	int productVersion;
	
	String name;
	
	@DBRef
	private Product product;
	 
	public ProductVersion() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	

	public int getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(int productVersion) {
		this.productVersion = productVersion;
	}

	@Override
	public String toString() {
		return "ProductVersion [id=" + id + ", productId=" + productId + ", productVersion=" + productVersion
				+ ", name=" + name + ", product=" + product + "]";
	}
	
	

}
