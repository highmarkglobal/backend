package tech.highmarkglobal.bcm.domain;

import java.util.Date;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class Wallet {
	
	
	private String id;
	
	private String transactionIdentifer; //transactionname
	
	@DateTimeFormat(iso = ISO.DATE)
	private Date transactionDate;
	
	private String counterParty;
	
	private Integer amount;
	
	private String symbol;
	
	private String transactionType;
	
	private String userType;
	
	


	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType2) {
		this.userType = userType2;
		
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTransactionIdentifer() {
		return transactionIdentifer;
	}

	public void setTransactionIdentifer(String transactionIdentifer) {
		this.transactionIdentifer = transactionIdentifer;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getCounterParty() {
		return counterParty;
	}

	public void setCounterParty(String counterParty) {
		this.counterParty = counterParty;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}


	
	

}
