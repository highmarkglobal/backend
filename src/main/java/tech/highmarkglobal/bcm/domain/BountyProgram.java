package tech.highmarkglobal.bcm.domain;

import java.util.Date;
import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import io.swagger.annotations.ApiModel;

@ApiModel
@Document(collection = "bountyprograms")
public class BountyProgram {
	
	@Id
	private String id;
	
	private String bountyName;
	
	//new
	private String assetID;
	
	
	private Integer reward;
	
	private String noOfParticipants;
	
	private String claimConditons;
	
	/*private String claimFeatureType;
	
	private String claimFeatureName;*/
	
	private JSONObject features;
	
	//old 
	private String bountyDesc;
	@DateTimeFormat(iso = ISO.DATE)
	private Date startDate;
	@DateTimeFormat(iso = ISO.DATE)
	private Date endDate;
	
	private Integer tokensOffered;
	
	private List<String> tags;
	
	private Double serviceChargs;
	
	private String offeredType;
	
	private String action;
	
	private String testingCategory;
	
	private List<String> milestones;
	
	private Integer searched;
	
	private Integer responses;
	
	private Integer contracts;
	
	@CreatedBy
	private String producerId;
	
	private String producerName;
	
	
	private String productId;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBountyName() {
		return bountyName;
	}

	public void setBountyName(String bountyName) {
		this.bountyName = bountyName;
	}
	
	//new
	public String getAssetID() {
		return assetID;
	}

	public void setAssetID(String assetID) {
		this.assetID = assetID;
	}

	public Integer getReward() {
		return reward;
	}

	public void setReward(Integer reward) {
		this.reward = reward;
	}

	public String getNoOfParticipants() {
		return noOfParticipants;
	}

	public void setNoOfParticipants(String noOfParticipants) {
		this.noOfParticipants = noOfParticipants;
	}

	public String getClaimConditons() {
		return claimConditons;
	}

	public void setClaimConditons(String claimConditons) {
		this.claimConditons = claimConditons;
	}

/*	public String getClaimFeatureType() {
		return claimFeatureType;
	}

	public void setClaimFeatureType(String claimFeatureType) {
		this.claimFeatureType = claimFeatureType;
	}

	public String getClaimFeatureName() {
		return claimFeatureName;
	}

	public void setClaimFeatureName(String claimFeatureName) {
		this.claimFeatureName = claimFeatureName;
	}*/
	
	//old
	public String getBountyDesc() {
		return bountyDesc;
	}

	public void setBountyDesc(String bountyDesc) {
		this.bountyDesc = bountyDesc;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getTokensOffered() {
		return tokensOffered;
	}

	public void setTokensOffered(Integer tokensOffered) {
		this.tokensOffered = tokensOffered;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public Double getServiceChargs() {
		return serviceChargs;
	}

	public void setServiceChargs(Double serviceChargs) {
		this.serviceChargs = serviceChargs;
	}

	public String getOfferedType() {
		return offeredType;
	}

	public void setOfferedType(String offeredType) {
		this.offeredType = offeredType;
	}

	public String getProducerId() {
		return producerId;
	}

	public void setProducerId(String producerId) {
		this.producerId = producerId;
	}
	
	public String getProducerName() {
		return producerName;
	}

	public void setProducerName(String producerName) {
		this.producerName = producerName;
	}

	
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public List<String> getMilestones() {
		return milestones;
	}

	public void setMilestones(List<String> milestones) {
		this.milestones = milestones;
	}

	public Integer getSearched() {
		return searched;
	}

	public void setSearched(Integer searched) {
		this.searched = searched;
	}

	public Integer getResponses() {
		return responses;
	}

	public void setResponses(Integer responses) {
		this.responses = responses;
	}

	public Integer getContracts() {
		return contracts;
	}

	public void setContracts(Integer contracts) {
		this.contracts = contracts;
	}

	public String getTestingCategory() {
		return testingCategory;
	}

	public void setTestingCategory(String testingCategory) {
		this.testingCategory = testingCategory;
	}
	
	

	public JSONObject getFeatures() {
		return features;
	}

	public void setFeatures(JSONObject features) {
		this.features = features;
	}

	@Override
	public String toString() {
		return "BountyProgram [id=" + id + ", bountyName=" + bountyName + ", bountyDesc=" + bountyDesc + ", startDate="
				+ startDate + ", endDate=" + endDate + ", tokensOffered=" + tokensOffered + ", tags=" + tags + ", testingCategory=" + testingCategory
				+ ", producerName="+ producerName +",serviceChargs=" + serviceChargs + ", assetID=" + assetID + ",reward=" + reward + ",noOfParticipants=" + noOfParticipants + 
				",features=" + features + "]";
	}
	
}
