package tech.highmarkglobal.bcm.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * @author ramesh
 *
 */
public class HighmarkUtil {
	
	private static final Logger logger=LoggerFactory.getLogger(HighmarkUtil.class);
	static DateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
	
	
	public static Date StrtoDate(String strDate)
	{
    	Date date = null;
		
		try {
			date = formatter.parse(strDate);
		} catch (ParseException e) {
			
			logger.error(" exception :{}",e);
		}
		logger.info("date :{}",date );
		return date;
	}
	
	public static String DateToStr(Date date )
	{
		String strDate = formatter.format(date);
		logger.info("strDate :{}",strDate );
		return strDate;  
	}
	
	public static String getCurrentDate() {
		Date dt = new Date();
		String formattedDt = formatter.format(dt);
		return formattedDt;
	}
	

}
