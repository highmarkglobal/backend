package tech.highmarkglobal.bcm.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorUtil {
	public static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(5);
}
