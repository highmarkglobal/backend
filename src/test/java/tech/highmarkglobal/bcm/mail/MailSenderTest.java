package tech.highmarkglobal.bcm.mail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import tech.highmarkglobal.bcm.mail.impl.MailSender;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(profiles={"test"})
public class MailSenderTest {
	
	@Autowired
    @Qualifier("simpleMailSender")
	public MailSender mailSender;

	@Test
	public void sendOnboardingMailTest() {
		String from = "highmarkglobal@gmail.com";
		String to = "nv.arunkumar@gmail.com";
		String subject = "JavaMailSender";
		String body = "Just-Testing!";
		
		mailSender.sendMail(from, to, subject, body);
	}

}
